#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "poly.h"

#include <x86intrin.h>

p_polyf_t creer_polynome (int degre)
{
  p_polyf_t p ;

  unsigned long long start, end ;

  start = _rdtsc () ;
  
  p = (p_polyf_t) malloc (sizeof (polyf_t)) ;
  p->degre = degre ;

  p->coeff = (float *) malloc ((degre+1) * sizeof (float))  ;

  init_polynome(p,0);

  end = _rdtsc () ;

  printf ("creation polynome: %Ld cycles\n", end-start) ;

  return p ;
}
void init_polynome (p_polyf_t p, float x)
{
  register unsigned int i ;

  for (i = 0 ; i <= p->degre; ++i)
    p->coeff [i] = x ;

  return ;
}
p_polyf_t lire_polynome_float (char *nom_fichier)
{
  FILE *f ;
  p_polyf_t p ;
  int degre ;
  int i  ;

  unsigned long long start, end ;

  start = _rdtsc () ;
  
  f = fopen (nom_fichier, "r") ;
  if (f == NULL)
    {
      fprintf (f, "erreur ouverture %s \n", nom_fichier) ;
      exit (-1) ;
    }
  
  fscanf (f, "%i", &degre) ;
  
  
  p = creer_polynome (degre) ;
  
  for (i = 0 ; i <= degre; i++)
    { 
     fscanf (f, "%f", &p->coeff[i]) ;
    }

  fclose (f) ;

  end = _rdtsc () ;

  printf ("lire polynome:  %Ld cycles\n", end-start) ;
  
  return p ;
}

void ecrire_polynome_float (p_polyf_t p)
{
  int i ;

  printf ("%f + %f x ", p->coeff [0], p->coeff [1]) ;
  
  for (i = 2 ; i <= p->degre; i++)
    {
      printf ("+ %f X^%d ", p->coeff [i], i) ;
    }
  
  printf ("\n") ;

  return ;
}

int egalite_polynome (p_polyf_t p1, p_polyf_t p2)
{
  if(p1 == NULL || p2 == NULL)
    return 0 ;
  if(p1->degre != p2->degre)
    return 0;
  int i = 0;
  while(i <= p1->degre && p1->coeff[i] == p2->coeff[i])
    i++;
  return i > p1->degre;
}

p_polyf_t addition_polynome (p_polyf_t p1, p_polyf_t p2)
{
  if(p1 == NULL || p2 == NULL)
    return NULL;
  int degre_max = p1->degre > p2->degre ? p1->degre : p2->degre;
  p_polyf_t p3 = creer_polynome(degre_max);
  for(int i = 0; i <= p3->degre; i++)
  {
    float s = 0;
    if(i <= p1->degre)
      s += p1->coeff[i];
    if(i <= p2->degre)
      s += p2->coeff[i];
    p3->coeff[i] = s;
  }
  return p3;
}

p_polyf_t multiplication_polynome_scalaire (p_polyf_t p, float alpha)
{
  if(p == NULL)
    return NULL;
  p_polyf_t p1 = creer_polynome(p->degre);
  for(int i = 0; i <= p->degre; i++)
    p1->coeff[i] = p->coeff[i] * alpha;
  return p1;
}

float eval_polynome (p_polyf_t p, float x)
{
  float rslt = 0;
  for(int i = 0; i <= p->degre; i++)
    rslt += p->coeff[i] * pow((double)x,(double)i);
  return rslt;
}

p_polyf_t multiplication_polynomes (p_polyf_t p1, p_polyf_t p2)
{
  if(p1 == NULL || p2 == NULL)
    return NULL;
  int degre_max = p1->degre + p2->degre;
  p_polyf_t p3 = creer_polynome(degre_max);
  for(int i = 0; i <= p1->degre; i++)
  {
    for(int j = 0; j <= p2->degre; j++)
      p3->coeff[i+j] += p1->coeff[i] * p2->coeff[j]; 
  } 
  return p3;
}

p_polyf_t puissance_polynome (p_polyf_t p, int n)
{
  if(p == NULL)
    return NULL;
  if(n == 0)
  {
    p_polyf_t p1 = creer_polynome(0);
    p1->coeff[0] = 1;
    return p1;
  }
  return multiplication_polynomes(p,puissance_polynome(p,n-1));
}

p_polyf_t composition_polynome (p_polyf_t p, p_polyf_t q)
{
  if(p == NULL || q == NULL)
    return NULL;
  p_polyf_t r = creer_polynome(p->degre * q->degre);
  for(int i = 0; i <= p->degre; i++){
    if(p->coeff[i] != 0)
    {
      p_polyf_t t1,t2;
      t1 = puissance_polynome(q,i);
      t2 = multiplication_polynome_scalaire(t1,p->coeff[i]);
      r = addition_polynome(r,t2);
      detruire_polynome(t1);
      detruire_polynome(t2);      
    }
  }
  return r;
}
void detruire_polynome (p_polyf_t p){
  free(p->coeff);
  free(p);
}
