#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "poly_creux.h"

#include <x86intrin.h>

p_poly_cf_t creer_polynome(int taille_tableau)
{
  p_poly_cf_t p;

  unsigned long long start, end;

  start = _rdtsc();

  p = (p_poly_cf_t)malloc(sizeof(poly_cf_t));
  p->taille_tableau = taille_tableau + 1;

  p->coeff = (float *)malloc((taille_tableau + 1) * sizeof(float));
  p->degre = (int *)malloc((taille_tableau + 1) * sizeof(int));

  init_polynome(p, 0);

  end = _rdtsc();

  printf("creation polynome: %Ld cycles\n", end - start);

  return p;
}
void init_polynome(p_poly_cf_t p, float x)
{
  register unsigned int i;

  for (i = 0; i < p->taille_tableau; ++i)
  {
    p->coeff[i] = x;
    p->degre[i] = i;
  }

  return;
}
p_poly_cf_t lire_polynome_float(char *nom_fichier)
{
  FILE *f;
  p_poly_cf_t p;
  int taille_tableau = 0;
  int i;

  unsigned long long start, end;

  start = _rdtsc();

  f = fopen(nom_fichier, "r");
  if (f == NULL)
  {
    fprintf(f, "erreur ouverture %s \n", nom_fichier);
    exit(-1);
  }

  fscanf(f, "%i", &taille_tableau);

  p = creer_polynome(taille_tableau);

  for (i = 0; i < taille_tableau; i++)
  {
    fscanf(f, "%i %f", &p->degre[i], &p->coeff[i]);
  }

  fclose(f);

  end = _rdtsc();

  printf("lire polynome:  %Ld cycles\n", end - start);

  return p;
}

void ecrire_polynome_float(p_poly_cf_t p)
{
  int i = 0;
  if (p->degre[i] == 0)
  {
    printf("%f ", p->coeff[i]);
    i++;
  }

  if (p->degre[i] == 1)
  {
    if (i > 0)
      printf("+ ");
    printf("%f X ", p->coeff[i]);
    i++;
  }

  for (; i < p->taille_tableau - 1; i++)
  {
    if (i > 0)
      printf("+ ");
    printf("%f X^%d ", p->coeff[i], p->degre[i]);
  }

  printf("\n");

  return;
}

int egalite_polynome(p_poly_cf_t p1, p_poly_cf_t p2)
{
  if (p1 == NULL || p2 == NULL)
    return 0;
  if (p1->degre[p1->taille_tableau] != p2->degre[p2->taille_tableau])
    return 0;
  int i = 0;
  while (i <= min(p1->taille_tableau, p2->taille_tableau) && p1->coeff[i] == p2->coeff[i] && p1->degre[i] == p2->degre[i])
    i++;
  return i > min(p1->taille_tableau, p2->taille_tableau);
}

p_poly_cf_t addition_polynome(p_poly_cf_t p1, p_poly_cf_t p2)
{
  if (p1 == NULL || p2 == NULL)
    return NULL;
  p_poly_cf_t p3 = creer_polynome(0);
  int i_p1 = 0, i_p2 = 0, i_p3 = 0;
  while ((i_p1 < p1->taille_tableau - 1 || i_p2 < p2->taille_tableau - 1))
  {

    if (i_p1 < p1->taille_tableau - 1 && i_p2 < p2->taille_tableau - 1 && p1->degre[i_p1] == p2->degre[i_p2])
    {
      p3->degre[i_p3] = p1->degre[i_p1];
      p3->coeff[i_p3] = p1->coeff[i_p1] + p2->coeff[i_p2];
      i_p1++;
      i_p2++;
    }
    else if (i_p1 < p1->taille_tableau - 1 && p1->degre[i_p1] < p2->degre[i_p2])
    {
      p3->degre[i_p3] = p1->degre[i_p1];
      p3->coeff[i_p3] = p1->coeff[i_p1];
      i_p1++;
    }
    else
    {
      p3->degre[i_p3] = p2->degre[i_p2];
      p3->coeff[i_p3] = p2->coeff[i_p2];
      i_p2++;
    }
    i_p3++;
    p3->coeff = realloc(p3->coeff, sizeof(float) * i_p3 + 1);
    p3->degre = realloc(p3->degre, sizeof(int) * i_p3 + 1);
    p3->taille_tableau = i_p3 + 1;
  }
  return p3;
}

p_poly_cf_t multiplication_polynome_scalaire(p_poly_cf_t p, float alpha)
{
  if (p == NULL)
    return NULL;
  if (alpha == 0)
    return creer_polynome(0);
  p_poly_cf_t p1 = creer_polynome(p->taille_tableau - 1);
  for (int i = 0; i < p1->taille_tableau - 1; i++)
  {
    p1->coeff[i] = p->coeff[i] * alpha;
    p1->degre[i] = p->degre[i];
  }
  return p1;
}

float eval_polynome(p_poly_cf_t p, float x)
{
  float rslt = 0;
  for (int i = 0; i < p->taille_tableau; i++)
    rslt += p->coeff[i] * pow((double)x, (double)p->degre[i]);
  return rslt;
}

p_poly_cf_t multiplication_polynomes(p_poly_cf_t p1, p_poly_cf_t p2)
{
  if (p1 == NULL || p2 == NULL)
    return NULL;
  int taille_max = p1->taille_tableau + p2->taille_tableau + 1;
  double p3[taille_max];
  int n_null = 0;

  for (int i = 0; i < p1->taille_tableau; i++)
  {
    for (int j = 0; j < p2->taille_tableau; j++)
    {
      p3[p1->degre[i] + p2->degre[j]] += p1->coeff[i] * p2->coeff[j];
    }
  }
  for (int i = 0; i < taille_max; i++)
  {
    if (p3[i] > pow(10, -20)) n_null++;
  }
  
  p_poly_cf_t rslt = creer_polynome(n_null);
  int i = 0, j = 0;
  while (i < taille_max)
  {
    if (p3[i] > pow(10, -20))
    {
      rslt->coeff[j] = p3[i];
      rslt->degre[j] = i;
      j++;
    }
    i++;
  }
  return rslt;
}

p_poly_cf_t puissance_polynome(p_poly_cf_t p, int n)
{
  if (p == NULL)
    return NULL;
  if (n == 0)
  {
    p_poly_cf_t p1 = creer_polynome(0);
    p1->coeff[0] = 1;
    return p1;
  }else if(n == 1)
  {
    return multiplication_polynome_scalaire(p,n);
  }
  return multiplication_polynomes(p, puissance_polynome(p,n-1));
}

p_poly_cf_t composition_polynome(p_poly_cf_t p, p_poly_cf_t q)
{
  if (p == NULL || q == NULL)
    return NULL;
  p_poly_cf_t r = creer_polynome(p->taille_tableau * q->taille_tableau);
  for (int i = 0; i <= p->degre; i++)
  {
    if (p->coeff[i] != 0)
    {
      p_poly_cf_t t1, t2;
      t1 = puissance_polynome(q, i);
      t2 = multiplication_polynome_scalaire(t1, p->coeff[i]);
      r = addition_polynome(r, t2);
      detruire_polynome(t1);
      detruire_polynome(t2);
    }
  }
  return r;
}
void detruire_polynome(p_poly_cf_t p)
{
  free(p->coeff);
  free(p->degre);
  free(p);
}
