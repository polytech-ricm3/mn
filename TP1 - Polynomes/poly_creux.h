/*
  poly_cf_t   : structure polynome
  p_poly_cf_t : pointeur sur un polynome
*/

#define max(a,b) ((a)>(b)?(a):(b))
#define min(a,b) ((a)<(b)?(a):(b))

typedef struct {
  //int degremax ;
  int taille_tableau;
  float *coeff;
  int *degre;
} poly_cf_t, *p_poly_cf_t;

p_poly_cf_t creer_polynome (int taille_tableau) ;

void init_polynome (p_poly_cf_t p, float x) ;

void detruire_polynome (p_poly_cf_t p) ;

p_poly_cf_t lire_polynome_float (char *nom_fichier) ;

void ecrire_polynome_float (p_poly_cf_t p) ;

int egalite_polynome (p_poly_cf_t p1, p_poly_cf_t p2) ;

p_poly_cf_t addition_polynome (p_poly_cf_t p1, p_poly_cf_t p2) ;

p_poly_cf_t multiplication_polynome_scalaire (p_poly_cf_t p, float alpha) ;

float eval_polynome (p_poly_cf_t p, float x) ;

p_poly_cf_t multiplication_polynomes (p_poly_cf_t p1, p_poly_cf_t p2) ;

p_poly_cf_t puissance_polynome (p_poly_cf_t p, int n) ;

p_poly_cf_t composition_polynome (p_poly_cf_t p, p_poly_cf_t q) ;
