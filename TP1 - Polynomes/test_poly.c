#include <stdio.h>
#include <stdlib.h>

#include "poly_creux.h"


int main (int argc, char **argv)
{
  p_poly_cf_t p1, p2, res,x;

  x = creer_polynome(0);
  x->coeff[0] = 1;
  x->degre[0] = 1;
  
  if (argc != 3)
    {
      fprintf (stderr, "deux paramètres (polynomes,fichiers) sont à passer \n") ;
      exit (-1) ;
    }
      
  p1 = lire_polynome_float (argv [1]) ;
  p2 = lire_polynome_float (argv [2]) ;

  ecrire_polynome_float (p1) ;
  ecrire_polynome_float (p2) ;


  ecrire_polynome_float(x);

  /********* Egalité de Polynomes ***************/
  int egal;
  egal = egalite_polynome(p2,p2);
  printf("\n\nEgalité entre P2 et P2 (doit retourner vrai) : %d\n", egal);
  egal = egalite_polynome(p1,p2);
  printf("Egalité entre P1 et P2 (doit retourner faux) : %d\n", egal);

  printf("\n*********************************\n");

  printf("P1 + P2: ");
  res = addition_polynome(p1,p2);
  ecrire_polynome_float(res);
  detruire_polynome(res);
  printf("\n");

  printf("\n*********************************\n");

  for(int i = -2; i<3; i++)
  {
    printf("P1 * %i: ",i);
    res = multiplication_polynome_scalaire(p1,i);
    ecrire_polynome_float(res);
    detruire_polynome(res);
    printf("\n");
  }

  printf("\n*********************************\n");

  printf("P1 * P2: ");
  res = multiplication_polynomes(p1,p2);
  ecrire_polynome_float(res);
  detruire_polynome(res);
  printf("\n");
  printf("P1 * x: ");
  res = multiplication_polynomes(p1,x);
  ecrire_polynome_float(res);
  detruire_polynome(res);
  printf("\n");

  printf("\n*********************************\n");

  for(int i = 0; i<4; i++)
  {
    printf("P1 ^ %i: ",i);
    res = puissance_polynome(p1,i);
    ecrire_polynome_float(res);
    printf("\n");
    detruire_polynome(res);
  }

  printf("\n*********************************\n");

 /* printf("P1 o P1: ");
  res = composition_polynome(p1,p1);
  ecrire_polynome_float(res);
  detruire_polynome(res);
  printf("\n");*/

  detruire_polynome(p1);
  detruire_polynome(p2);
  detruire_polynome(x);
}
