#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "test.h"


void help()
{
    printf("------------------------------------------------------------------------\n");
    printf("                           AIDE\n");
    printf("------------------------------------------------------------------------\n");
    printf("<nom du test (ex: dot, asum, amax, ...)>  : Execute le test\n");
    printf("h                                         : Affiche cette aide\n");
    printf("at                                        : Affiche les tests disponible\n");
    printf("q                                         : Quitter\n");
    printf("------------------------------------------------------------------------\n");
}

void show_available_tests()
{
    int i = 0;
    char *test_available[] = {"amin","amax","asum","axpy","copy","dot","swap","nrm2","gemv","gemm",'\0'};
    printf("\n");
    printf("TESTS DISPONIBLES : \n\n");
    while(test_available[i])
    {
        printf("  - %s  ",test_available[i]);
        if(i > 0 && (i + 1) % 3 == 0)
            printf("\n");
        i++;
    }
    printf("\n");
}

int main(int argc, char **argv)
{
    srand(time(NULL));

    char *action = malloc(sizeof(char) * 50);

    if(argc == 2) action = argv[1];
    else help();

    while(1)
    {
        if(argc < 2)
        {
            printf("\n");
            printf(">> ");
            scanf("%s",action);
            fflush(stdin);
        }
        if(strcmp(action,"q") == 0) exit(0);
        else if(strcmp(action,"h") == 0)
            help();
        else if(strcmp(action,"at") == 0)
            show_available_tests();
        else if(strcmp(action, "amin") == 0)
            test_amin();
        else if(strcmp(action, "amax") == 0)
            test_amax();
        else if(strcmp(action, "asum") == 0)
            test_asum();
        else if(strcmp(action, "axpy") == 0)
            test_axpy();
        else if(strcmp(action, "copy") == 0)
            test_copy();
        else if(strcmp(action, "dot")  == 0)
            test_dot();
        else if(strcmp(action, "swap") == 0)
            test_swap();
        else if(strcmp(action, "nrm2") == 0)
            test_nrm2();
        else if(strcmp(action, "gemv") == 0)
            test_gemv();
        else if(strcmp(action, "gemm") == 0)
            test_gemm();
        else
            printf("Erreur: commande inconnue! \n");
        if (argc >= 2) break;
    }
    action = NULL;
    free(action);
    return 0;
}
