#ifndef _TEST_H_
#define _TEST_H_

int test_amin();
int test_amax();
int test_asum();
int test_axpy();
int test_copy();
int test_dot ();
int test_swap();
int test_nrm2();
int test_gemv();
int test_gemm();

#endif //_TEST_H_
