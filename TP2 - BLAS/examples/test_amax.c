#include <stdio.h>
#include <x86intrin.h>
#include <assert.h>
#include <math.h>

#include "test.h"
#include "mnblas.h"
#include "flop.h"
#include "vector.h"

#define NB_FOIS 5

int test_isamax(vfloat      v, CBLAS_INDEX  res);
int test_idamax(vdouble     v, CBLAS_INDEX  res);
int test_icamax(vcomplex_f  v, CBLAS_INDEX  res);
int test_izamax(vcomplex_d  v, CBLAS_INDEX  res);

int test_amax()
{
    printf("\n");
    printf("mnblas_isamax\n");
    printf("--------------------------------------------\n");
    unsigned long long start, end;
    vfloat v1;
    CBLAS_INDEX res;
    int i = 0;
    for(; i < NB_FOIS; i++)
    {
        vector_init(v1,(float)i + 1);
        start = _rdtsc();
            res = mnblas_isamax(VECSIZE,v1,1);
        end   = _rdtsc();
        assert(test_isamax(v1,res));
        printf("[%i] Test de %s : OK\n",i,"mnblas_isamax");
        calcul_mem("mnblas_isamax",(VECSIZE / 2) * sizeof(float), end - start);
        printf("--------------------------------------------\n");
    }

    printf("\n");
    printf("mnblas_idamax\n");
    printf("--------------------------------------------\n");
    vdouble vd1;
    i = 0;
    for(; i < NB_FOIS; i++)
    {
        vector_init_double(vd1,(double)i + 1);
        start = _rdtsc();
            res = mnblas_idamax(VECSIZE,vd1,1);
        end   = _rdtsc();
        assert(test_idamax(vd1,res));
        printf("[%i] Test de %s : OK\n",i,"mnblas_idamax");
        calcul_mem("mnblas_idamax",(VECSIZE / 2) * sizeof(double), end - start);
        printf("--------------------------------------------\n");
    }

    printf("\n");
    printf("mnblas_icamax\n");
    printf("--------------------------------------------\n");
    vcomplex_f vcf1;
    i = 0;
    for(; i < NB_FOIS; i++)
    {
        vector_init_complex_f(vcf1,(float)i + 1,(float)i + 1);
        start = _rdtsc();
            res = mnblas_icamax(VECSIZE,vcf1,1);
        end   = _rdtsc();
        assert(test_icamax(vcf1,res));
        printf("[%i] Test de %s : OK\n",i,"mnblas_icamax");
        calcul_flop("mnblas_icamax",VECSIZE, end - start);
        printf("--------------------------------------------\n");
    }

    printf("\n");
    printf("mnblas_izamax\n");
    printf("--------------------------------------------\n");
    vcomplex_d vcd1;
    i = 0;
    for(; i < NB_FOIS; i++)
    {
        vector_init_complex_d(vcd1,(double)i + 1,(double)i + 1);
        start = _rdtsc();
            res = mnblas_izamax(VECSIZE,vcd1,1);
        end   = _rdtsc();
        assert(test_izamax(vcd1,res));
        printf("[%i] Test de %s : OK\n",i,"mnblas_izamax");
        calcul_flop("mnblas_izamax",VECSIZE, end - start);
        printf("--------------------------------------------\n");
    }
    return 0;
}

int test_isamax(vfloat v, CBLAS_INDEX res)
{
    CBLAS_INDEX expected = 0;
    float tmp = fabs(v[0]);
    for(int i = 0; i < VECSIZE; i++)
    {
        if(fabs(v[i]) >= tmp)
        {
            expected = i;
            tmp      = fabs(v[i]); 
        }
    }
    assert(res == expected);
    return 1;
}
int test_idamax(vdouble v, CBLAS_INDEX res)
{
    CBLAS_INDEX expected = 0;
    double tmp = (double)fabs(v[0]);
    for(int i = 0; i < VECSIZE; i++)
    {
        if((double)fabs(v[i]) >= tmp)
        {
            expected = i;
            tmp      = (double)fabs(v[i]); 
        }
    }
    assert(res == expected);
    return 1;
}
int test_icamax(vcomplex_f v, CBLAS_INDEX res)
{
    CBLAS_INDEX expected = 0;
    float tmp = fabs(v[0].real) + fabs(v[0].imaginary);
    for(int i = 0; i < VECSIZE; i++)
    {
        if(fabs(v[i].real) + fabs(v[i].imaginary) >= tmp)
        {
            expected = i;
            tmp      = fabs(v[i].real) + fabs(v[i].imaginary); 
        }
    }
    assert(res == expected);
    return 1;
}

int test_izamax(vcomplex_d v, CBLAS_INDEX res)
{
    CBLAS_INDEX expected = 0;
    double tmp = (double)fabs(v[0].real) + (double)fabs(v[0].imaginary);
    for(int i = 0; i < VECSIZE; i++)
    {
        if((double)fabs(v[i].real) + (double)fabs(v[i].imaginary) >= tmp)
        {
            expected = i;
            tmp      = (double)fabs(v[i].real) + (double)fabs(v[i].imaginary); 
        }
    }
    assert(res == expected);
    return 1;
}