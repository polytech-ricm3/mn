#include <stdio.h>
#include <x86intrin.h>
#include <assert.h>
#include <math.h>

#include "test.h"
#include "mnblas.h"
#include "flop.h"
#include "vector.h"

#define NB_FOIS 5

int test_isamin(vfloat      v, CBLAS_INDEX  res);
int test_idamin(vdouble     v, CBLAS_INDEX  res);
int test_icamin(vcomplex_f  v, CBLAS_INDEX  res);
int test_izamin(vcomplex_d  v, CBLAS_INDEX  res);

int test_amin()
{
    printf("\n");
    printf("mnblas_isamin\n");
    printf("--------------------------------------------\n");
    unsigned long long start, end;
    vfloat v1;
    CBLAS_INDEX res;
    int i = 0;
    for(; i < NB_FOIS; i++)
    {
        vector_init(v1,(float)i + 1);
        start = _rdtsc();
            res = mnblas_isamin(VECSIZE,v1,1);
        end   = _rdtsc();
        assert(test_isamin(v1,res));
        printf("[%i] Test de %s : OK\n",i,"mnblas_isamin");
        calcul_mem("mnblas_isamin",(VECSIZE / 2) * sizeof(float), end - start);
        printf("--------------------------------------------\n");
    }

    printf("\n");
    printf("mnblas_idamin\n");
    printf("--------------------------------------------\n");
    vdouble vd1;
    i = 0;
    for(; i < NB_FOIS; i++)
    {
        vector_init_double(vd1,(double)i + 1);
        start = _rdtsc();
            res = mnblas_idamin(VECSIZE,vd1,1);
        end   = _rdtsc();
        assert(test_idamin(vd1,res));
        printf("[%i] Test de %s : OK\n",i,"mnblas_idamin");
        calcul_mem("mnblas_idamin",(VECSIZE / 2) * sizeof(double), end - start);
        printf("--------------------------------------------\n");
    }

    printf("\n");
    printf("mnblas_icamin\n");
    printf("--------------------------------------------\n");
    vcomplex_f vcf1;
    i = 0;
    for(; i < NB_FOIS; i++)
    {
        vector_init_complex_f(vcf1,(float)i + 1,(float)i + 1);
        start = _rdtsc();
            res = mnblas_icamin(VECSIZE,vcf1,1);
        end   = _rdtsc();
        assert(test_icamin(vcf1,res));
        printf("[%i] Test de %s : OK\n",i,"mnblas_icamin");
        calcul_flop("mnblas_icamin",VECSIZE, end - start);
        printf("--------------------------------------------\n");
    }

    printf("\n");
    printf("mnblas_izamin\n");
    printf("--------------------------------------------\n");
    vcomplex_d vcd1;
    i = 0;
    for(; i < NB_FOIS; i++)
    {
        vector_init_complex_d(vcd1,(double)i + 1,(double)i + 1);
        start = _rdtsc();
            res = mnblas_izamin(VECSIZE,vcd1,1);
        end   = _rdtsc();
        assert(test_izamin(vcd1,res));
        printf("[%i] Test de %s : OK\n",i,"mnblas_izamin");
        calcul_flop("mnblas_izamin",VECSIZE, end - start);
        printf("--------------------------------------------\n");
    }
    return 0;
}

int test_isamin(vfloat v, CBLAS_INDEX res)
{
    CBLAS_INDEX expected = 0;
    float tmp = fabs(v[0]);
    for(int i = 0; i < VECSIZE; i++)
    {
        if(fabs(v[i]) <= tmp)
        {
            expected = i;
            tmp      = fabs(v[i]);
        }
    }
    assert(res == expected);
    return 1;
}
int test_idamin(vdouble v, CBLAS_INDEX res)
{
    CBLAS_INDEX expected = 0;
    double tmp = (double)fabs(v[0]);
    for(int i = 0; i < VECSIZE; i++)
    {
        if((double)fabs(v[i]) <= tmp)
        {
            expected = i;
            tmp      = (double)fabs(v[i]);
        }
    }
    assert(res == expected);
    return 1;
}
int test_icamin(vcomplex_f v, CBLAS_INDEX res)
{
    CBLAS_INDEX expected = 0;
    float tmp = fabs(v[0].real) + fabs(v[0].imaginary);
    for(int i = 0; i < VECSIZE; i++)
    {
        if(fabs(v[i].real) + fabs(v[i].imaginary) <= tmp)
        {
            expected = i;
            tmp      = fabs(v[i].real) + fabs(v[i].imaginary);
        }
    }
    assert(res == expected);
    return 1;
}

int test_izamin(vcomplex_d v, CBLAS_INDEX res)
{
    CBLAS_INDEX expected = 0;
    double tmp = (double)fabs(v[0].real) + (double)fabs(v[0].imaginary);
    for(int i = 0; i < VECSIZE; i++)
    {
        if((double)fabs(v[i].real) + (double)fabs(v[i].imaginary) <= tmp)
        {
            expected = i;
            tmp      = (double)fabs(v[i].real) + (double)fabs(v[i].imaginary);
        }
    }
    assert(res == expected);
    return 1;
}
