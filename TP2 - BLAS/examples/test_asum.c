#include <stdio.h>
#include <x86intrin.h>
#include <assert.h>
#include <math.h>

#include "test.h"
#include "mnblas.h"
#include "flop.h"
#include "vector.h"

#define NB_FOIS 5

int test_sasum(vfloat      v, float  res);
int test_dasum(vdouble     v, double res);
int test_scasum(vcomplex_f v, float  res);
int test_dzasum(vcomplex_d v, double res);

int test_asum()
{
    printf("\n");
    printf("mnblas_sasum\n");
    printf("--------------------------------------------\n");
    unsigned long long start, end;
    vfloat v1;
    float res = 0;
    int i = 0;
    for(; i < NB_FOIS; i++)
    {
        vector_init(v1,(float)i + 1);
        start = _rdtsc();
            res = mnblas_sasum(VECSIZE,v1,1);
        end   = _rdtsc();
        assert(test_sasum(v1,res));
        printf("[%i] Test de %s : OK\n",i,"mnblas_sasum");
        calcul_flop("mnblas_sasum",VECSIZE, end - start);
        printf("--------------------------------------------\n");
    }
    printf("\n");
    printf("mnblas_dasum\n");
    printf("--------------------------------------------\n");
    vdouble vd1;
    double resd = 0;
    i = 0;
    for(; i < NB_FOIS; i++)
    {
        vector_init_double(vd1,(double)i + 1);
        start = _rdtsc();
            resd = mnblas_dasum(VECSIZE,vd1,1);
        end   = _rdtsc();
        assert(test_dasum(vd1,resd));
        printf("[%i] Test de %s : OK\n",i,"mnblas_dasum");
        calcul_flop("mnblas_dasum",VECSIZE, end - start);
        printf("--------------------------------------------\n");
    }
    printf("\n");
    printf("mnblas_scasum\n");
    printf("--------------------------------------------\n");
    vcomplex_f vcf1;
    float rescf = 0;
    i = 0;
    for(; i < NB_FOIS; i++)
    {
        vector_init_complex_f(vcf1,(float)i + 1,(float)i + 1);
        start = _rdtsc();
            rescf = mnblas_scasum(VECSIZE,vcf1,1);
        end   = _rdtsc();
        assert(test_scasum(vcf1,rescf));
        printf("[%i] Test de %s : OK\n",i,"mnblas_scasum");
        calcul_flop("mnblas_scasum",VECSIZE, end - start);
        printf("--------------------------------------------\n");
    }
    printf("\n");
    printf("mnblas_dzasum\n");
    printf("--------------------------------------------\n");
    vcomplex_d vcd1;
    double rescd = 0;
    i = 0;
    for(; i < NB_FOIS; i++)
    {
        vector_init_complex_d(vcd1,(double)i + 1,(double)i + 1);
        start = _rdtsc();
            rescd = mnblas_dzasum(VECSIZE,vcd1,1);
        end   = _rdtsc();
        assert(test_dzasum(vcd1,rescd));
        printf("[%i] Test de %s : OK\n",i,"mnblas_dzasum");
        calcul_flop("mnblas_dzasum",VECSIZE, end - start);
        printf("--------------------------------------------\n");
    }
    return 0;
}

int test_sasum(vfloat v , float res)
{
    float expected = 0;
    for(int i = 0; i < VECSIZE; i++)
        expected += fabs(v[i]);
    assert(res == expected);
    return 1;
}

int test_dasum(vdouble v, double res)
{
    double expected = 0;
    for(int i = 0; i < VECSIZE; i++)
        expected += (double)fabs(v[i]);
    assert(res == expected);
    return 1;
}
int test_scasum(vcomplex_f v, float  res)
{
    float expected = 0;
    for(int i = 0; i < VECSIZE; i++)
        expected += (double)fabs(v[i].real) + (double)fabs(v[i].imaginary);
    assert(res == expected);
    return 1;
}
int test_dzasum(vcomplex_d v, double res)
{
    double expected = 0;
    for(int i = 0; i < VECSIZE; i++)
        expected += (double)fabs(v[i].real) + (double)fabs(v[i].imaginary);
    assert(res == expected);
    return 1;
}