#include <stdio.h>
#include <x86intrin.h>
#include <assert.h>

#include "test.h"
#include "mnblas.h"
#include "flop.h"
#include "vector.h"

#define NB_FOIS 5

int test_saxpy(vfloat v , vfloat expected);
int test_daxpy(vdouble v, vdouble expected);
int test_caxpy(vcomplex_f v, vcomplex_f expected);
int test_zaxpy(vcomplex_d v, vcomplex_d expected);

int test_axpy()
{
    printf("\n");
    printf("mnblas_saxpy\n");
    printf("--------------------------------------------\n");
    unsigned long long start, end;
    vfloat v1, v2, expected;
    int i = 0;
    for(; i < NB_FOIS; i++)
    {
        vector_init(v1,(float)i + 1);
        vector_init(v2,(float)i);
        start = _rdtsc();
            mnblas_saxpy(VECSIZE,(float) i,v1,1,v2,1);
        end   = _rdtsc();
        for(int j = 0; j < VECSIZE; j++)
            expected[j] = v1[j] * i;
        assert(test_saxpy(v2,expected));
        printf("[%i] Test de %s : OK\n",i,"mnblas_saxpy");
        calcul_flop("mnblas_saxpy",VECSIZE, end - start);
        printf("--------------------------------------------\n");
    }
    printf("\n");
    printf("mnblas_daxpy\n");
    printf("--------------------------------------------\n");
    i = 0;
    vdouble vd1, vd2, expected_d;
    for(; i < NB_FOIS; i++)
    {
        vector_init_double(vd1,(double)i + 1);
        vector_init_double(vd2,(double)i);
        start = _rdtsc();
            mnblas_daxpy(VECSIZE,(double) i,vd1,1,vd2,1);
        end   = _rdtsc();
        for(int j = 0; j < VECSIZE; j++)
            expected_d[j] = vd1[j] * i;
        assert(test_daxpy(vd2,expected_d));
        printf("[%i] Test de %s : OK\n",i,"mnblas_daxpy");
        calcul_flop("mnblas_daxpy",VECSIZE, end - start);
        printf("--------------------------------------------\n");
    }
    printf("\n");
    printf("mnblas_caxpy\n");
    printf("--------------------------------------------\n");
    i = 0;
    vcomplex_f vcf1, vcf2, expected_cf;
    for(; i < NB_FOIS; i++)
    {
        complexe_float_t *alpha = malloc(sizeof(complexe_float_t));
        alpha->real      = i + 1;
        alpha->imaginary = i;
        vector_init_complex_f(vcf1,(float)i + 1,(float)i + 1);
        vector_init_complex_f(vcf2,(float)i,(float)i);
        start = _rdtsc();
            mnblas_caxpy(VECSIZE,alpha,vcf1,1,vcf2,1);
        end   = _rdtsc();
        for(int j = 0; j < VECSIZE; j++)
        {
            complexe_float_t tmp;
            tmp.real      = (vcf1->real * alpha->real) - 
                            (vcf1->imaginary * alpha->imaginary);
            tmp.imaginary = (vcf1->real * alpha->imaginary) + 
                            (vcf1->imaginary * alpha->real);
            expected_cf[j] = tmp;
        }
        assert(test_caxpy(vcf2,expected_cf));
        printf("[%i] Test de %s : OK\n",i,"mnblas_caxpy");
        calcul_flop("mnblas_caxpy",4 * VECSIZE, end - start);
        free(alpha);
        printf("--------------------------------------------\n");
    }
    printf("\n");
    printf("mnblas_zaxpy\n");
    printf("--------------------------------------------\n");
    i = 0;
    vcomplex_d vcd1, vcd2, expected_cd;
    for(; i < NB_FOIS; i++)
    {
        complexe_double_t *alpha = malloc(sizeof(complexe_double_t));
        alpha->real      = i + 1;
        alpha->imaginary = i;
        vector_init_complex_d(vcd1,(double)i + 1,(double)i + 1);
        vector_init_complex_d(vcd2,(double)i,(double)i);
        start = _rdtsc();
            mnblas_zaxpy(VECSIZE,alpha,vcd1,1,vcd2,1);
        end   = _rdtsc();
        for(int j = 0; j < VECSIZE; j++)
        {
            complexe_double_t tmp;
            tmp.real      = (vcd1->real * alpha->real) - 
                            (vcd1->imaginary * alpha->imaginary);
            tmp.imaginary = (vcd1->real * alpha->imaginary) + 
                            (vcd1->imaginary * alpha->real);
            expected_cd[j] = tmp;
        }
        assert(test_zaxpy(vcd2,expected_cd));
        printf("[%i] Test de %s : OK\n",i,"mnblas_zaxpy");
        calcul_flop("mnblas_zaxpy", 4 * VECSIZE, end - start);
        free(alpha);
        printf("--------------------------------------------\n");
    }
    return 0;
}

int test_saxpy(vfloat v, vfloat expected)
{
    for(int i = 0; i < VECSIZE; i++)
        assert(v[i] == expected[i]);
    return 1;
}
int test_daxpy(vdouble v, vdouble expected)
{
    for(int i = 0; i < VECSIZE; i++)
        assert(v[i] == expected[i]);
    return 1;
}
int test_caxpy(vcomplex_f v, vcomplex_f expected)
{
    for(int i = 0; i < VECSIZE; i++)
    {
        assert(v[i].real      == expected[i].real);
        assert(v[i].imaginary == expected[i].imaginary);
    }
    return 1;
}
int test_zaxpy(vcomplex_d v, vcomplex_d expected)
{
    for(int i = 0; i < VECSIZE; i++)
    {
        assert(v[i].real      == expected[i].real);
        assert(v[i].imaginary == expected[i].imaginary);
    }
    return 1;
}
