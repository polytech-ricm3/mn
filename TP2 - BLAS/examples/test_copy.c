#include <stdio.h>
#include <x86intrin.h>
#include <assert.h>

#include "test.h"
#include "mnblas.h"
#include "flop.h"
#include "vector.h"

#define NB_FOIS    5

int test_vector_copy(vfloat v1, vfloat v2)
{
    for(int i = 0; i < VECSIZE; i++)
    {
        assert(v1[i] == v2[i]);
    }
    return 1;
}

int test_vector_double(vdouble v1, vdouble v2)
{
    for(int i = 0; i < VECSIZE; i++)
    {
        assert(v1[i] == v2[i]);
    }
    return 1;
}

int test_vector_cf(vcomplex_f v1, vcomplex_f v2)
{
    for(int i = 0; i < VECSIZE; i++)
    {
        assert(v1[i].real      == v2[i].real);
        assert(v1[i].imaginary == v2[i].imaginary);
    }
    return 1;
}

int test_vector_cd(vcomplex_d v1, vcomplex_d v2)
{
    for(int i = 0; i < VECSIZE; i++)
    {
        assert(v1[i].real      == v2[i].real);
        assert(v1[i].imaginary == v2[i].imaginary);
    }
    return 1;
}

int test_copy()
{
    unsigned long long start, end;
    vfloat v1, v2;
    int i = 0;
    for(; i < NB_FOIS; i++)
    {
        vector_init(v1,(float)i + 1);
        vector_init(v2,(float)i);
        start = _rdtsc();
            mncblas_scopy(VECSIZE,v1,1,v2,1);
        end   = _rdtsc();
        assert(test_vector_copy(v1,v2) == 1);
        printf("[%i] Test de %s : OK\n",i,"mncblas_scopy");
        calcul_mem("mncblas_scopy",(VECSIZE / 2) * sizeof(float), end - start);
        printf("\n");
    }
    printf("--------------------------------------------\n\n");
    
    i = 0;
    vdouble vd1, vd2;
    for(; i < NB_FOIS; i++)
    {
        vector_init_double(vd1,(double)i + 1);
        vector_init_double(vd2,(double)i);
        start = _rdtsc();
            mncblas_dcopy(VECSIZE,vd1,1,vd2,1);
        end   = _rdtsc();
        assert(test_vector_double(vd1,vd2) == 1);
        printf("[%i] Test de %s : OK\n",i,"mncblas_dcopy");
        calcul_mem("mncblas_dcopy",(VECSIZE / 2) * sizeof(double), end - start);
        printf("\n");
    }
    printf("--------------------------------------------\n\n");

    i = 0;
    vcomplex_f vcf1, vcf2;
    for(; i < NB_FOIS; i++)
    {
        vector_init_complex_f(vcf1,(float)i + 1,(float)i + 1);
        vector_init_complex_f(vcf2,(float)i,(float)i);
        start = _rdtsc();
            mncblas_ccopy(VECSIZE,vcf1,1,vcf2,1);
        end   = _rdtsc();
        assert(test_vector_cf(vcf1,vcf2) == 1);
        printf("[%i] Test de %s : OK\n",i,"mncblas_ccopy");
        calcul_mem("mncblas_ccopy",(VECSIZE / 2) * sizeof(complexe_float_t), end - start);
        printf("\n");
    }
    printf("--------------------------------------------\n\n");

    i = 0;
    vcomplex_d vcd1, vcd2;
    for(; i < NB_FOIS; i++)
    {
        vector_init_complex_d(vcd1,(double)i + 1,(double)i + 1);
        vector_init_complex_d(vcd2,(double)i,(double)i);
        start = _rdtsc();
            mncblas_zcopy(VECSIZE,vcd1,1,vcd2,1);
        end   = _rdtsc();
        assert(test_vector_cd(vcd1,vcd2) == 1);
        printf("[%i] Test de %s : OK\n",i,"mncblas_zcopy");
        calcul_mem("mncblas_zcopy",(VECSIZE / 2) * sizeof(complexe_double_t), end - start);
        printf("\n");
    }
    printf("\n");
    return 0;
}