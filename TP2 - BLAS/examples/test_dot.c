#include <stdio.h>
#include <x86intrin.h>
#include <assert.h>
#include <math.h>
#include <float.h>

#include "test.h"
#include "mnblas.h"
#include "utils/flop.h"
#include "utils/vector.h"
#include "complexe.h"

#define NB_FOIS    5

int test_sdot();
int test_ddot();
int test_cdotu_sub();
int test_cdotc_sub();
int test_zdotu_sub();
int test_zdotc_sub();


int test_dot()
{
  assert(test_sdot());
  assert(test_ddot());
  assert(test_cdotu_sub());
  assert(test_cdotc_sub());
  assert(test_zdotu_sub());
  assert(test_zdotc_sub());
  return 1;
}

int test_sdot()
{
  printf("\n");
  printf("mncblas_sdot\n");
  printf("--------------------------------------------\n");
  unsigned long long start, end;
  vfloat v1, v2;


  for(int i = 0; i < NB_FOIS; i++)
  {
    float expected = 0.0;
    float res = 0.0;
    vector_init(v1,(float)i);
    vector_init(v2,1.0);
    start = _rdtsc();
      res = mncblas_sdot(VECSIZE,v1,1,v2,1);
    end   = _rdtsc();

    for(int j = 0; j < VECSIZE; j++)
    {
      expected +=v1[j] * v2[j];
    }

    assert(expected == res);

    printf("[%i] Test de %s : OK\n",i,"mncblas_sdot");
    calcul_flop("mncblas_sdot",2 * VECSIZE, end - start);
    printf("--------------------------------------------\n");
  }
  return 1;

}

int test_ddot()
{
  printf("\n");
  printf("mncblas_ddot\n");
  printf("--------------------------------------------\n");
  unsigned long long start, end;
  vdouble v1, v2;


  for(int i = 0; i < NB_FOIS; i++)
  {
    double expected = 0.0;
    double res = 0.0;
    vector_init_double(v1,(double)i);
    vector_init_double(v2,1.0);
    start = _rdtsc();
    res = mncblas_ddot(VECSIZE,v1,1,v2,1);
    end   = _rdtsc();

    for(int j = 0; j < VECSIZE; j++)
    expected = expected + v1[j] * 1.0;
    assert(res == expected);

    printf("[%i] Test de %s : OK\n",i,"mncblas_ddot");
    calcul_flop("mncblas_ddot",2 * VECSIZE, end - start);
    printf("--------------------------------------------\n");
  }
  return 1;

}

int test_cdotu_sub()
{
  printf("\n");
  printf("mncblas_cdotu_sub\n");
  printf("--------------------------------------------\n");
  unsigned long long start, end;
  vcomplex_f v1, v2;

  for(int i = 0; i < NB_FOIS; i++)
  {
    complexe_float_t expected = {0,0};
    complexe_float_t tmp = {0,0};
    complexe_float_t res = {0,0};

    vector_init_complex_f(v1,(float)i,(float) i + 1.0);
    vector_init_complex_f(v2,1.0,1.0);
    start = _rdtsc();
    mncblas_cdotu_sub(VECSIZE,v1,1,v2,1, &res);
    end   = _rdtsc();

    for(int j = 0; j < VECSIZE; j++)
    {
      tmp =  mult_complexe_float (v1[j], v2[j]);
      expected = add_complexe_float(expected, tmp);
    }
    assert((res.real == expected.real) && (res.imaginary == expected.imaginary));

    printf("[%i] Test de %s : OK\n",i,"mncblas_cdotu_sub");
    calcul_flop("mncblas_cdotu_sub", 6 * VECSIZE, end - start);
    printf("--------------------------------------------\n");
  }
  return 1;
}

int test_cdotc_sub()
{
  printf("\n");
  printf("mncblas_cdotc_sub\n");
  printf("--------------------------------------------\n");
  unsigned long long start, end;
  vcomplex_f v, v1, v2;


  for(int i = 0; i < NB_FOIS; i++)
  {
    complexe_float_t expected = {0,0};
    complexe_float_t tmp = {0,0};
    complexe_float_t res = {0,0};

    vector_init_complex_f(v1,(float)i,(float) i + 1.0);
    vector_init_complex_f(v2,1.0,1.0);
    vector_init_complex_f(v,0.0,0.0);
    start = _rdtsc();
    mncblas_cdotc_sub(VECSIZE,v1,1,v2,1, &res);
    end   = _rdtsc();

    for(int j = 0; j < VECSIZE; j++)
    {
      v[j] = conjugue_float(v1[j]);
      tmp =  mult_complexe_float (v[j], v2[j]);
      expected = add_complexe_float(expected, tmp);
    }
    assert((res.real == expected.real) && (res.imaginary == expected.imaginary));

    printf("[%i] Test de %s : OK\n",i,"mncblas_cdotc_sub");
    calcul_flop("mncblas_cdotc_sub", 6 * VECSIZE, end - start);
    printf("--------------------------------------------\n");
  }
  return 1;
}

int test_zdotu_sub()
{
  printf("\n");
  printf("mncblas_zdotu_sub\n");
  printf("--------------------------------------------\n");
  unsigned long long start, end;
  vcomplex_d v1, v2;


  for(int i = 0; i < NB_FOIS; i++)
  {
    complexe_double_t expected = {0,0};
    complexe_double_t res= {0,0};
    complexe_double_t tmp = {0,0};

    vector_init_complex_d(v1,(double)i, (double)i+1.0);
    vector_init_complex_d(v2,1.0,1.0);
    start = _rdtsc();
    mncblas_zdotu_sub(VECSIZE,v1,1,v2,1, &res);
    end   = _rdtsc();

    for(int j = 0; j < VECSIZE; j++)
    {
      tmp =  mult_complexe_double (v1[j], v2[j]);
      expected = add_complexe_double(expected, tmp);
    }
    assert((res.real == expected.real) && (res.imaginary == expected.imaginary));

    printf("[%i] Test de %s : OK\n",i,"mncblas_zdotu_sub");
    calcul_flop("mncblas_zdotu_sub",6 * VECSIZE, end - start);
    printf("--------------------------------------------\n");
  }
  return 1;
}

int test_zdotc_sub()
{
  printf("\n");
  printf("mncblas_zdotc_sub\n");
  printf("--------------------------------------------\n");
  unsigned long long start, end;
  vcomplex_d v, v1, v2;


  for(int i = 0; i < NB_FOIS; i++)
  {
    complexe_double_t expected = {0,0};
    complexe_double_t res= {0,0};
    complexe_double_t tmp = {0,0};

    vector_init_complex_d(v1,(double)i, (double)i+1.0);
    vector_init_complex_d(v2,1.0,1.0);
    vector_init_complex_d(v,0.0,0.0);

    start = _rdtsc();
    mncblas_zdotc_sub(VECSIZE,v1,1,v2,1, &res);
    end   = _rdtsc();

    for(int j = 0; j < VECSIZE; j++)
    {
      v[j] = conjugue_double(v1[j]);
      tmp =  mult_complexe_double (v[j], v2[j]);
      expected = add_complexe_double(expected, tmp);
    }
    assert((res.real == expected.real) && (res.imaginary == expected.imaginary));

    printf("[%i] Test de %s : OK\n",i,"mncblas_zdotc_sub");
    calcul_flop("mncblas_zdotc_sub", 6 * VECSIZE, end - start);
    printf("--------------------------------------------\n");
  }
  return 1;
}
