#include <stdio.h>
#include <x86intrin.h>
#include <assert.h>
#include <math.h>
#include <float.h>

#include "test.h"
#include "mnblas.h"
#include "utils/flop.h"
#include "utils/matrix.h"
#include "utils/vector.h"

#define NB_FOIS 5
#define MATSIZE 256
#define FLT_ROUNDS 0


int test_sgemm();
int test_dgemm();
int test_cgemm();
int test_zgemm();

int test_gemm()
{
    assert(test_sgemm());
    assert(test_dgemm());
    assert(test_cgemm());
    assert(test_zgemm());
    return 1;
}

int test_sgemm()
{
    printf("\n");
    printf("mnblas_sgemv\n");
    printf("--------------------------------------------\n");
    unsigned long long start, end;
    float v1[MATSIZE * MATSIZE], v2[MATSIZE * MATSIZE], v2s[MATSIZE * MATSIZE], A[MATSIZE * MATSIZE];
    float alpha = 1, beta = 2;

    for(int i = 0; i < MATSIZE * MATSIZE; i++)
    {
        v1[i] = 1; v2[i] = 2;
        A[i]  = 3;
    }

    mncblas_scopy(MATSIZE * MATSIZE,v2,1,v2s,1);
    for(int i = 0; i < NB_FOIS; i++)
    {
        start = _rdtsc();
            mncblas_sgemm(101, 111,111, MATSIZE, MATSIZE,MATSIZE, alpha, A, MATSIZE, v1, MATSIZE, beta, v2, MATSIZE);
        end = _rdtsc();
        calcul_flop("mnblas_sgemm",2*MATSIZE * MATSIZE *MATSIZE, end - start);
        
        start = _rdtsc();
            mncblas_sgemm(101, 111,111, MATSIZE, MATSIZE,MATSIZE, alpha, A, MATSIZE, v1, MATSIZE, beta, v2s, MATSIZE);
        end = _rdtsc();
        calcul_flop("mnblas_sgemm",2*MATSIZE * MATSIZE *MATSIZE, end - start);

        assert(vector_equal_size(v2,v2s,MATSIZE*MATSIZE));
        printf("[%i] Test de %s : OK\n",i,"mnblas_sgemm");
    }
    return 1;
}

int test_dgemm()
{
    printf("\n");
    printf("mnblas_dgemv\n");
    printf("--------------------------------------------\n");
    unsigned long long start, end;
    double v1[MATSIZE * MATSIZE], v2[MATSIZE * MATSIZE], v2s[MATSIZE * MATSIZE], A[MATSIZE * MATSIZE];
    double alpha = 1, beta = 2;

    for(int i = 0; i < MATSIZE * MATSIZE; i++)
    {
        v1[i] = 1; v2[i] = 2;
        A[i]  = 3;
    }

    mncblas_dcopy(MATSIZE * MATSIZE,v2,1,v2s,1);
    for(int i = 0; i < NB_FOIS; i++)
    {
        start = _rdtsc();
            mncblas_dgemm(101, 111,111, MATSIZE, MATSIZE,MATSIZE, alpha, A, MATSIZE, v1, MATSIZE, beta, v2, MATSIZE);
        end = _rdtsc();
        calcul_flop("mnblas_dgemm",2*MATSIZE * MATSIZE *MATSIZE, end - start);
        
        start = _rdtsc();
            mncblas_dgemm(101, 111,111, MATSIZE, MATSIZE,MATSIZE, alpha, A, MATSIZE, v1, MATSIZE, beta, v2s, MATSIZE);
        end = _rdtsc();
        calcul_flop("mnblas_dgemm",2*MATSIZE * MATSIZE *MATSIZE, end - start);

        assert(vector_equal_d_size(v2,v2s,MATSIZE*MATSIZE));
        printf("[%i] Test de %s : OK\n",i,"mnblas_dgemm");
    }
    return 1;
}

int test_cgemm()
{
    printf("\n");
    printf("mncblas_cgemm\n");
    printf("--------------------------------------------\n");

    unsigned long long start, end;
    int i;
    complexe_float_t cf1 = {1,1}, cf2 = {2,2};
    complexe_float_t alpha, beta;
    complexe_float_t v1[MATSIZE * MATSIZE], v2[MATSIZE * MATSIZE], v2s[MATSIZE * MATSIZE], A[MATSIZE * MATSIZE];

    alpha = cf2; beta = cf1;

    for(i = 0; i < MATSIZE * MATSIZE; i++)
    {
        v1[i] = cf1; v2[i] = cf2;
        A[i] = cf1;
    }

    mncblas_ccopy(MATSIZE*MATSIZE, v2, 1, v2s, 1);
    for (i = 0; i < NB_FOIS; i++) {

    /*                                Test_cgemv                                              */
        start = _rdtsc();
        mncblas_cgemm(101, 111,111, MATSIZE, MATSIZE,MATSIZE,&alpha, A, MATSIZE, v1, MATSIZE, &beta, v2, MATSIZE);
        end = _rdtsc();
        calcul_flop("mncblas_cgemm",(MATSIZE * MATSIZE) * (2*MATSIZE+4), end - start);

        start = _rdtsc();
        mncblas_cgemm(101, 111, 111,MATSIZE, MATSIZE,MATSIZE,&alpha, A, MATSIZE, v1, MATSIZE, &beta, v2s, MATSIZE);
        end = _rdtsc();
        calcul_flop("mncblas_cgemm",(MATSIZE * MATSIZE) * (2*MATSIZE+4), end - start);

        assert(vector_equal_complex_f_size(v2,v2s,MATSIZE*MATSIZE));
        printf("[%i] Test de %s : OK\n",i,"mncblas_cgemm");
    }
    return 1;
}

int test_zgemm()
{
    printf("\n");
    printf("mncblas_Zgemm\n");
    printf("--------------------------------------------\n");

    unsigned long long start, end;
    int i;
    complexe_double_t cf1 = {1,1}, cf2 = {2,2};
    complexe_double_t alpha, beta;
    complexe_double_t v1[MATSIZE * MATSIZE], v2[MATSIZE * MATSIZE], v2s[MATSIZE * MATSIZE], A[MATSIZE * MATSIZE];

    alpha = cf2; beta = cf1;

    for(i = 0; i < MATSIZE * MATSIZE; i++)
    {
        v1[i] = cf1; v2[i] = cf2;
        A[i] = cf1;
    }

    mncblas_zcopy(MATSIZE*MATSIZE, v2, 1, v2s, 1);
    for (i = 0; i < NB_FOIS; i++) {

    /*                                Test_cgemv                                              */
        start = _rdtsc();
        mncblas_zgemm(101, 111,111, MATSIZE, MATSIZE,MATSIZE,&alpha, A, MATSIZE, v1, MATSIZE, &beta, v2, MATSIZE);
        end = _rdtsc();
        calcul_flop("mncblas_Zgemm",(MATSIZE * MATSIZE) * (2*MATSIZE+4), end - start);

        start = _rdtsc();
        mncblas_zgemm(101, 111,111, MATSIZE, MATSIZE,MATSIZE,&alpha, A, MATSIZE, v1, MATSIZE, &beta, v2s, MATSIZE);
        end = _rdtsc();
        calcul_flop("mncblas_Zgemm",(MATSIZE * MATSIZE) * (2*MATSIZE+4), end - start);

        assert(vector_equal_complex_d_size(v2,v2s,MATSIZE*MATSIZE));
        printf("[%i] Test de %s : OK\n",i,"mncblas_Zgemm");
    }
    return 1;
}