#include <stdio.h>
#include <x86intrin.h>
#include <assert.h>
#include <math.h>
#include <float.h>

#include "test.h"
#include "mnblas.h"
#include "utils/flop.h"
#include "utils/matrix.h"
#include "utils/vector.h"

#define NB_FOIS 5
#define MATSIZE 256
#define FLT_ROUNDS 0

//Verifie l'égalité de deux vecteurs simple

int test_sgemv();
int test_dgemv();
int test_cgemv();
int test_zgemv();

int test_gemv()
{
  assert(test_sgemv());
  assert(test_dgemv());
  assert(test_cgemv());
  assert(test_zgemv());
  return 1;
}

int test_sgemv()
{
  printf("\n");
  printf("mnblas_sgemv\n");
  printf("--------------------------------------------\n");
  unsigned long long start, end;
  vfloat v1, v2, v2s;
  float alpha, beta;
  mfloat* A;

  int i;

  vector_random(v1);
  vector_random(v2);

  alpha = 10.0;
  beta = 1.2;

  mncblas_scopy(VECSIZE, v2, 1, v2s, 1);

  A = matrix_random(VECSIZE, VECSIZE);

  for (i = 0; i < NB_FOIS; i++) {

    /*                                Test_sgemv                                              */

    start = _rdtsc();
    mncblas_sgemv(101, 111, VECSIZE, VECSIZE, alpha, A->donnees, VECSIZE, v1, 1, beta, v2, 1);
    end = _rdtsc();
    calcul_flop("mnblas_sgemv",2 * (VECSIZE * VECSIZE) + 3 * VECSIZE, end - start);

    start = _rdtsc();
    mncblas_sgemv(101, 111, VECSIZE, VECSIZE, alpha, A->donnees, VECSIZE, v1, 1, beta, v2s, 1);
    end = _rdtsc();
    calcul_flop("mnblas_sgemv",2 * (VECSIZE * VECSIZE) + 3 * VECSIZE, end - start);

    assert(vector_equal(v2,v2s));
    printf("[%i] Test de %s : OK\n",i,"mnblas_sgemv");

  }

  free(A->donnees);
  free(A);

  return 1;

}

int test_dgemv()
{
  printf("\n");
  printf("mnblas_dgemv\n");
  printf("--------------------------------------------\n");
  unsigned long long start, end;
  vdouble v1, v2, v2s;
  double alpha, beta;
  mdouble* B;

  int i;

  vector_random_double(v1);
  vector_random_double(v2);

  alpha = 10.0;
  beta = 1.2;

  mncblas_dcopy(VECSIZE, v2, 1, v2s, 1);

  B = matrix_random_double(VECSIZE, VECSIZE);

  for (i = 0; i < NB_FOIS; i++) {
    /*                                Test_dgemv                                              */

    start = _rdtsc();
    mncblas_dgemv(101, 111, VECSIZE, VECSIZE, alpha, B->donnees, VECSIZE, v1, 1, beta, v2, 1);
    end = _rdtsc();
    calcul_flop("mnblas_dgemv",2 * (VECSIZE * VECSIZE) + 3 * VECSIZE, end - start);

    start = _rdtsc();
    mncblas_dgemv(101, 111, VECSIZE, VECSIZE, alpha, B->donnees, VECSIZE, v1, 1, beta, v2s, 1);
    end = _rdtsc();
    calcul_flop("mnblas_dgemv",2 * (VECSIZE * VECSIZE) + 3 * VECSIZE, end - start);

    assert(vector_equal_double(v2,v2s));
    printf("[%i] Test de %s : OK\n",i,"mnblas_dgemv");



  }
  free(B->donnees);
  free(B);
  return 1;

}


int test_cgemv()
{
  printf("\n");
  printf("mnblas_cgemv\n");
  printf("--------------------------------------------\n");
  
  unsigned long long start, end;
  int i;
  complexe_float_t cf1 = {1,1}, cf2 = {2,2};
  complexe_float_t alpha, beta;
  complexe_float_t v1[MATSIZE], v2[MATSIZE], v2s[MATSIZE];
  complexe_float_t B[MATSIZE * MATSIZE];

  alpha = cf2; beta = cf1;

  for(i = 0; i < MATSIZE; i++)
  {
    v1[i] = cf1; v2[i] = cf2;
  }

  for(i = 0; i < MATSIZE * MATSIZE; i++)
  {
     B[i] = cf1;
  }

  mncblas_ccopy(MATSIZE, v2, 1, v2s, 1);
  for (i = 0; i < NB_FOIS; i++) {

    /*                                Test_cgemv                                              */
    start = _rdtsc();
    mncblas_cgemv(101, 111, MATSIZE, MATSIZE, &alpha, B, MATSIZE, v1, 1, &beta, v2, 1);
    end = _rdtsc();
    calcul_flop("mnblas_cgemv",2 * (MATSIZE * MATSIZE) + 2 * MATSIZE, end - start);

    start = _rdtsc();
    mncblas_cgemv(101, 111, MATSIZE, MATSIZE, &alpha, B, MATSIZE, v1, 1, &beta, v2s, 1);
    end = _rdtsc();
    calcul_flop("mnblas_cgemv",2 * (MATSIZE * MATSIZE) + 2 * MATSIZE, end - start);

    assert(vector_equal_complex_f_size(v2,v2s,MATSIZE));
    printf("[%i] Test de %s : OK\n",i,"mnblas_cgemv");
  }
  return 1;
}

int test_zgemv()
{
  printf("\n\n");
  printf("mnblas_zgemv\n");
  printf("--------------------------------------------\n");
  
  unsigned long long start, end;
  int i;
  complexe_double_t cf1 = {1,1}, cf2 = {2,2};
  complexe_double_t alpha, beta;
  complexe_double_t v1[MATSIZE], v2[MATSIZE], v2s[MATSIZE];
  complexe_double_t B[MATSIZE * MATSIZE];

  alpha = cf2; beta = cf1;

  for(i = 0; i < MATSIZE; i++)
  {
    v1[i] = cf1; v2[i] = cf2;
  }

  for(i = 0; i < MATSIZE * MATSIZE; i++)
  {
     B[i] = cf1;
  }

  mncblas_zcopy(MATSIZE, v2, 1, v2s, 1);
  for (i = 0; i < NB_FOIS; i++) {

    /*                                Test_zgemv                                              */
    start = _rdtsc();
    mncblas_zgemv(101, 111, MATSIZE, MATSIZE, &alpha, B, MATSIZE, v1, 1, &beta, v2, 1);
    end = _rdtsc();
    calcul_flop("mnblas_zgemv",2 * (MATSIZE * MATSIZE) + 2 * MATSIZE, end - start);

    start = _rdtsc();
    mncblas_zgemv(101, 111, MATSIZE, MATSIZE, &alpha, B, MATSIZE, v1, 1, &beta, v2s, 1);
    end = _rdtsc();
    calcul_flop("mnblas_zgemv",2 * (MATSIZE * MATSIZE) + 2 * MATSIZE, end - start);

    assert(vector_equal_complex_d_size(v2,v2s,MATSIZE));
    printf("[%i] Test de %s : OK\n",i,"mnblas_cgemv");
  }
  return 1;
}