#include <stdio.h>
#include <assert.h>
#include <math.h>
#include <x86intrin.h>

#include "test.h"
#include "mnblas.h"
#include "flop.h"
#include "vector.h"

#define NB_FOIS 5

int test_snrm2(vfloat v, float  res);
int test_dnrm2(vdouble v, double res);
int test_scnrm2(vcomplex_f v, float  res);
int test_dznrm2(vcomplex_d v, double res);

int test_nrm2()
{
    printf("\n");
    printf("mnblas_snrm2\n");
    printf("--------------------------------------------\n");
    unsigned long long start, end;
    vfloat v1;
    float res = 0;
    int i = 0;
    for(; i < NB_FOIS; i++)
    {
        vector_init(v1,(float)i + 1);
        start = _rdtsc();
            res = mnblas_snrm2(VECSIZE,v1,1);
        end   = _rdtsc();
        assert(test_snrm2(v1,res));
        printf("[%i] Test de %s : OK\n",i,"mnblas_snrm2");
        calcul_flop("mnblas_snrm2",VECSIZE, end - start);
        printf("--------------------------------------------\n");
    }
    printf("\n");
    printf("mnblas_dnrm2\n");
    printf("--------------------------------------------\n");
    vdouble vd1;
    double resd = 0;
    i = 0;
    for(; i < NB_FOIS; i++)
    {
        vector_init_double(vd1,(double)i + 1);
        start = _rdtsc();
            resd = mnblas_dnrm2(VECSIZE,vd1,1);
        end   = _rdtsc();
        assert(test_dnrm2(vd1,resd));
        printf("[%i] Test de %s : OK\n",i,"mnblas_dnrm2");
        calcul_flop("mnblas_dnrm2",VECSIZE, end - start);
        printf("--------------------------------------------\n");
    }
    printf("\n");
    printf("mnblas_scnrm2\n");
    printf("--------------------------------------------\n");
    vcomplex_f vcf1;
    float rescf = 0;
    i = 0;
    for(; i < NB_FOIS; i++)
    {
        vector_init_complex_f(vcf1,(float)i + 1,(float)i + 1);
        start = _rdtsc();
            rescf = mnblas_scnrm2(VECSIZE,vcf1,1);
        end   = _rdtsc();
        assert(test_scnrm2(vcf1,rescf));
        printf("[%i] Test de %s : OK\n",i,"mnblas_scnrm2");
        calcul_flop("mnblas_scnrm2",VECSIZE, end - start);
        printf("--------------------------------------------\n");
    }
    printf("\n");
    printf("mnblas_dznrm2\n");
    printf("--------------------------------------------\n");
    vcomplex_d vcd1;
    double rescd = 0;
    i = 0;
    for(; i < NB_FOIS; i++)
    {
        vector_init_complex_d(vcd1,(double)i + 1,(double)i + 1);
        start = _rdtsc();
            rescd = mnblas_dznrm2(VECSIZE,vcd1,1);
        end   = _rdtsc();
        assert(test_dznrm2(vcd1,rescd));
        printf("[%i] Test de %s : OK\n",i,"mnblas_dznrm2");
        calcul_flop("mnblas_dznrm2",VECSIZE, end - start);
        printf("--------------------------------------------\n");
    }
    return 0;
}

int test_snrm2(vfloat v , float res)
{
    float expected = 0;
    float tmp = 0;
    for(int i = 0; i < VECSIZE; i++)
        tmp += powf(v[i], 2);
    expected = sqrtf(tmp);
    assert(res == expected);
    return 1;
}

int test_dnrm2(vdouble v, double res)
{
    double expected = 0;
    double tmp = 0;
    for(int i = 0; i < VECSIZE; i++)
        tmp += pow(v[i], 2);
    expected = sqrt(tmp);
    assert(res == expected);
    return 1;
}
int test_scnrm2(vcomplex_f v, float  res)
{
    float expected = 0;
    float tmp = 0;
    for(int i = 0; i < VECSIZE; i++)
        tmp += powf((v[i].real), 2) + powf((v[i].imaginary), 2);
    expected = sqrtf(tmp);
    assert(res == expected);
    return 1;
}
int test_dznrm2(vcomplex_d v, double res)
{
    double expected = 0;
    double tmp = 0;
    for(int i = 0; i < VECSIZE; i++)
        tmp += pow((v[i].real), 2) + pow((v[i].imaginary), 2);
    expected = sqrt(tmp);
    assert(res == expected);
    return 1;
}
