#include <stdio.h>
#include <x86intrin.h>
#include <assert.h>

#include "mnblas.h"
#include "complex.h"
#include "flop.h"
#include "vector.h"
#include "test.h"

#define NB_FOIS    5
vfloat v1, v2, v3, v4;

void test_vector(vfloat v1, vfloat v2, char *func)
{
    for(int i = 0; i < VECSIZE; i++)
    {
        assert(v1[i] == v2[i]);
    }
    printf("Test de %s : OK",func);
    printf("\n");
}

int test_swap()
{
    unsigned long long start, end;
    int i = 0;
    for(; i < NB_FOIS; i++)
    {
        vector_init(v1,1.0); vector_init(v3,1.0);
        vector_init(v2,2.0); vector_init(v4,2.0);
        start = _rdtsc();
            mncblas_sswap(VECSIZE,v1,1,v2,1);
        end   = _rdtsc();
        printf("V1: "); vector_print(v1);
        printf("V2: "); vector_print(v2);
        test_vector(v1,v4,"mncblas_sswap");
        test_vector(v2,v3,"mncblas_sswap"); 
        calcul_flop("mncblas_sswap",VECSIZE, end - start);
    }
    return 0;
}