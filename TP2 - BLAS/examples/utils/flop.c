#include <stdio.h>
#include <x86intrin.h>

static const float duree_cycle = (float) 1 / (float) 2.6 ;
// duree du cycle en nano seconde 10^-9

void calcul_flop (char *message, int nb_operations_flottantes, unsigned long long int cycles)
{
  printf ("%s %d operations %5.3f GFLOP/s\n", message, nb_operations_flottantes, ((float) nb_operations_flottantes) / (((float) cycles) * duree_cycle)) ;
  return ;
}
void calcul_mem  (char *message, int nb_access_mem, unsigned long long int time)
{
  printf("%s %d acces mem %5.3f o/s\n",message,nb_access_mem,(float)(nb_access_mem/(float)(time * duree_cycle)));
}
