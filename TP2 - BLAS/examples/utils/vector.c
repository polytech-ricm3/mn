#include "vector.h"
#include <assert.h>
#include <math.h>
#include <time.h>


#define RANDOM_VALUE 50
#define EPSILON 0.000000001

void vector_init (vfloat V, float x)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++)
    V [i] = x ;

  return ;
}

void vector_random(vfloat V)
{

    register unsigned int i;
    float a;
    float signe = rand() % 2;
    for (i = 0; i < VECSIZE; i++) {
        a = rand() % RANDOM_VALUE;
        if (signe)
            a = -a;
        V[i] = a;
    }
}

void vector_random_double(vdouble v)
{

  register unsigned int i;
  double a;
  double signe = rand() % 2;
  for (i = 0; i < VECSIZE; i++) {
      a = rand() % RANDOM_VALUE;
      if (signe)
          a = -a;
      v[i] = a;
  }
}

void vector_random_complex_f(vcomplex_f v)
{

  register unsigned int i = 0;
  complexe_float_t c;

  for(; i < VECSIZE; i++)
  {
    c.real      =   rand() % RANDOM_VALUE;
    c.imaginary =   rand() % RANDOM_VALUE;
    v[i] = c;
  }
}

void vector_random_complex_d(vcomplex_d v)
{

  register unsigned int i = 0;
  complexe_double_t c;

  for(; i < VECSIZE; i++)
  {
    c.real      = rand() % RANDOM_VALUE;
    c.imaginary = rand() % RANDOM_VALUE;
    v[i] = c;
  }
}

int vector_equal(vfloat v1, vfloat v2)
{
    register unsigned int i;
    for (i = 0; i < VECSIZE && fabs(v1[i] - v2[i]) < EPSILON; i++);
    return (i == VECSIZE);
}

int vector_equal_double(vdouble v1, vdouble v2)
{
    register unsigned int i;
    for (i = 0; i < VECSIZE && fabs(v1[i] - v2[i]) < EPSILON; i++);
    return (i == VECSIZE);
}

int  vector_equal_complex_f(vcomplex_f v1,vcomplex_f v2)
{
    register unsigned int i;
    for(i = 0; i < VECSIZE; i++)
    {
        assert(fabs(v1[i].real - v2[i].real) < EPSILON);
        assert(fabs(v1[i].imaginary - v2[i].imaginary) < EPSILON);
    }
    return (i == VECSIZE);
}

int  vector_equal_size(float *v1, float *v2,int size)
{
  register unsigned int i;
  for (i = 0; i < size && fabs(v1[i] -v2[i]) < EPSILON; i++);
  return (i == size);
}

int  vector_equal_d_size(double *v1, double *v2,int size)
{
  register unsigned int i;
  for (i = 0; i < size && fabs(v1[i] - v2[i]) < EPSILON; i++);
  return (i == size);
}

int  vector_equal_complex_f_size(complexe_float_t *v1,complexe_float_t *v2, const int taille)
{
  register unsigned int i;
    for(i = 0; i < taille; i++)
    {
        assert(fabs(v1[i].real - v2[i].real) < EPSILON);
        assert(fabs(v1[i].imaginary - v2[i].imaginary) < EPSILON);
    }
    return (i == taille);
}

int  vector_equal_complex_d_size(complexe_double_t *v1,complexe_double_t *v2, const int taille)
{
  register unsigned int i;
    for(i = 0; i < taille; i++)
    {
        assert(fabs(v1[i].real - v2[i].real) < EPSILON);
        assert(fabs(v1[i].imaginary - v2[i].imaginary) < EPSILON);
    }
    return (i == taille);
}

int  vector_equal_complex_d(vcomplex_d v1,vcomplex_d v2)
{
    register unsigned int i;
    for(i = 0; i < VECSIZE; i++)
    {
        assert(fabs(v1[i].real - v2[i].real) < EPSILON);
        assert(fabs(v1[i].imaginary - v2[i].imaginary) < EPSILON);
    }
    return (i == VECSIZE);
}

void vector_init_double(vdouble V, double x)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++)
    V [i] = x ;

  return ;
}

void vector_init_complex_f(vcomplex_f v,float real, float img){
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++)
  {
    complexe_float_t tmp;
    tmp.real      = real;
    tmp.imaginary = img;
    v[i] = tmp;
  }

  return ;
}

void vector_init_complex_d(vcomplex_d v,double real, double img)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++)
  {
    complexe_double_t tmp;
    tmp.real      = real;
    tmp.imaginary = img;
    v[i] = tmp;
  }

  return ;
}

void vector_print (vfloat V)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++)
    printf ("%f ", V[i]) ;
  printf ("\n") ;

  return ;
}
