#include <stdio.h>
#include <stdlib.h>

#include "complexe.h"

#define VECSIZE    1024

typedef float             vfloat     [VECSIZE];
typedef double            vdouble    [VECSIZE];
typedef complexe_float_t  vcomplex_f [VECSIZE];
typedef complexe_double_t vcomplex_d [VECSIZE];

void vector_init (vfloat V, float x);
void vector_init_double(vdouble V, double x);
void vector_init_complex_f(vcomplex_f v,float real, float img);
void vector_init_complex_d(vcomplex_d v,double real, double img);


void vector_random(vfloat V);
void vector_random_double(vdouble v);
void vector_random_complex_f(vcomplex_f v);
void vector_random_complex_d(vcomplex_d v);


int  vector_equal(vfloat v1, vfloat v2);
int  vector_equal_double(vdouble v1, vdouble v2);
int  vector_equal_complex_f(vcomplex_f v1,vcomplex_f v2);
int  vector_equal_complex_d(vcomplex_d v1,vcomplex_d v2);

int  vector_equal_size(float *v1, float *v2,int size);
int  vector_equal_d_size(double *v1, double *v2,int size);
int  vector_equal_complex_f_size(complexe_float_t *v1,complexe_float_t *v2, const int taille);
int  vector_equal_complex_d_size(complexe_double_t *v1,complexe_double_t *v2, const int taille);


void vector_print (vfloat V);
