#ifndef COMPLEXE_H__
#define COMPLEXE_H__
typedef struct {
  float real;
  float imaginary;
} complexe_float_t;

typedef struct {
  double real;
  double imaginary;
} complexe_double_t;

static inline complexe_float_t add_complexe_float(const complexe_float_t c1,
                                                  const complexe_float_t c2) {
  complexe_float_t r;

  r.real = c1.real + c2.real;
  r.imaginary = c1.imaginary + c2.imaginary;

  return r;
}

static inline complexe_double_t
add_complexe_double(const complexe_double_t c1, const complexe_double_t c2) {
  complexe_double_t r;

  r.real = c1.real + c2.real;
  r.imaginary = c1.imaginary + c2.imaginary;

  return r;
}

static inline complexe_float_t mult_complexe_float(const complexe_float_t c1,
                                                   const complexe_float_t c2) {
  complexe_float_t r;

  r.real = (c1.real * c2.real) - (c1.imaginary * c2.imaginary);
  r.imaginary = (c1.real * c2.imaginary) + (c1.imaginary * c2.real);

  return r;
}

static inline complexe_double_t
mult_complexe_double(const complexe_double_t c1, const complexe_double_t c2) {
  complexe_double_t r;

  r.real = (c1.real * c2.real) - (c1.imaginary * c2.imaginary);
  r.imaginary = (c1.real * c2.imaginary) + (c1.imaginary * c2.real);

  return r;
}

static inline complexe_float_t conjugue_float(const complexe_float_t c1) {
  complexe_float_t r;

  r.real = c1.real;
  r.imaginary = -c1.imaginary;

  return r;
}

static inline complexe_double_t conjugue_double(const complexe_double_t c1) {
  complexe_double_t r;

  r.real = c1.real;
  r.imaginary = -c1.imaginary;

  return r;
}

#endif
