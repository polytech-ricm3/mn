#include "../include/complexe.h"
#include "../include/mnblas.h"

#include <math.h>

CBLAS_INDEX mnblas_isamax(const int N, const float  *X, const int incX)
{
  CBLAS_INDEX res = 0;
  float tmp = fabs(X[0]);
  register unsigned int i = 0;
  for(; i < N; i += incX)
  {
    if(fabs(X[i]) >= tmp)
    {
      res = i;
      tmp = fabs(X[i]);
    }
  }
  return res;
}
CBLAS_INDEX mnblas_idamax(const int N, const double *X, const int incX)
{
  CBLAS_INDEX res = 0;
  double tmp = fabs(X[0]);
  register unsigned int i = 0;
  for(; i < N; i += incX)
  {
    if(fabs(X[i]) >= tmp)
    {
      res = i;
      tmp = fabs(X[i]);
    }
  }
  return res;
}

CBLAS_INDEX mnblas_icamax(const int N, const void   *X, const int incX)
{
  CBLAS_INDEX res = 0;
  complexe_float_t *elems = ((complexe_float_t *) X);
  float tmp = fabs(elems[0].real) + fabs(elems[0].imaginary);
  register unsigned int i = 0;
  for(; i < N; i += incX)
  {
    if(fabs(elems[i].real) + fabs(elems[i].imaginary) >= tmp)
    {
      res = i;
      tmp = fabs(elems[i].real) + fabs(elems[i].imaginary);
    }
  }
  return res;
}

CBLAS_INDEX mnblas_izamax(const int N, const void   *X, const int incX)
{
  CBLAS_INDEX res = 0;
  complexe_double_t *elems = ((complexe_double_t *) X);
  double tmp = fabs(elems[0].real) + fabs(elems[0].imaginary);
  register unsigned int i = 0;
  for(; i < N; i += incX)
  {
    if((double)fabs(elems[i].real) + (double)fabs(elems[i].imaginary) >= tmp)
    {
      res = i;
      tmp = (double)fabs(elems[i].real) + (double)fabs(elems[i].imaginary);
    }
  }
  return res;
}
