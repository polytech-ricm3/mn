#include "../include/complexe.h"
#include "../include/mnblas.h"

#include <math.h>

float  mnblas_sasum(const int N, const float *X, const int incX)
{
    float res = 0;
    register unsigned int i = 0;
    for(; i < N; i += incX)
        res += fabs(X[i]);
    return res;
}

double mnblas_dasum(const int N, const double *X, const int incX)
{
    double res = 0;
    register unsigned int i = 0;
    for(; i < N; i += incX)
        res += (double)fabs(X[i]);
    return res;
}
float  mnblas_scasum(const int N, const void *X, const int incX)
{
    float res = 0;
    register unsigned int i = 0;
    for(; i < N; i += incX)
    {
        res += (fabs(((complexe_float_t *) X)[i].real) + fabs(((complexe_float_t *) X)[i].imaginary));
    }
    return res;
}
double mnblas_dzasum(const int N, const void *X, const int incX)
{
    double res = 0;
    register unsigned int i = 0;
    complexe_double_t *elems = ((complexe_double_t *) X);
    for(; i < N; i += incX)
    {
        res += ((double)fabs(elems[i].real) + (double)fabs(elems[i].imaginary));
    }
    return res;
}