#include "../include/complexe.h"
#include "../include/mnblas.h"

void mnblas_saxpy(const int N, const float alpha, const float *X,
                  const int incX, float *Y, const int incY)
{
    if(N <= 0)
        return;
    register unsigned int i = 0;
    register unsigned int j = 0;
    for(; i < N && j < N; i+= incX, j += incY)
    {
        Y[j] = X[i] * alpha;
    }
}

void mnblas_daxpy(const int N, const double alpha, const double *X,
                  const int incX, double *Y, const int incY)
{
    if(N <= 0)
        return;
    register unsigned int i = 0;
    register unsigned int j = 0;
    for(; i < N && j < N; i+= incX, j += incY)
    {
        Y[j] = X[i] * alpha;
    }
}

void mnblas_caxpy(const int N, const void *alpha, const void *X,
                 const int incX, void *Y, const int incY)
{
    if(N <= 0)
        return;
    register unsigned int i = 0;
    register unsigned int j = 0;
    complexe_float_t *alpha_c = (complexe_float_t *) alpha;
    for(; i < N && j < N; i+= incX, j += incY)
    {
        ((complexe_float_t *)Y) [j] = mult_complexe_float(((complexe_float_t *)X)[i],*alpha_c);
    }
}

void mnblas_zaxpy(const int N, const void *alpha, const void *X,
                  const int incX, void *Y, const int incY)
{
    if(N <= 0)
        return;
    register unsigned int i = 0;
    register unsigned int j = 0;
    complexe_double_t *alpha_c = (complexe_double_t *) alpha;
    for(; i < N && j < N; i+= incX, j += incY)
    {
        ((complexe_double_t *)Y) [j] = mult_complexe_double(((complexe_double_t *)X)[i],*alpha_c);
    }
}