#include "../include/complexe.h"
#include "../include/mnblas.h"

void mncblas_sgemm(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const float alpha, const float *A,
                 const int lda, const float *B, const int ldb,
                 const float beta, float *C, const int ldc)
{
    register unsigned int i;
    register unsigned int j;
    register unsigned int k;
    float f;
    for (i = 0; i < M; i++) {
        for (j = 0; j < M; j++) {
            C[i * M + j] = beta * C[i * M + j];
            f = 0;
            for (k = 0; k < M; k++) 
                f += A[i * M + k] * B[k * M + j];
            C[i * M + j] += f * alpha;
        }
    }
}

void mncblas_dgemm(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const double alpha, const double *A,
                 const int lda, const double *B, const int ldb,
                 const double beta, double *C, const int ldc)
{
    register unsigned int i;
    register unsigned int j;
    register unsigned int k;
    double d;
    for (i = 0; i < M; i++) {
        for (j = 0; j < M; j++) {
            C[i * M + j] = beta * C[i * M + j];
            d = 0;
            for (k = 0; k < M; k++) 
                d += A[i * M + k] * B[k * M + j];
            C[i * M + j] += d * alpha;
        }
    }
}

void mncblas_cgemm(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const void *alpha, const void *A,
                 const int lda, const void *B, const int ldb,
                 const void *beta, void *C, const int ldc)
{
    register unsigned int i;
    register unsigned int j;
    register unsigned int k;
    complexe_float_t *Ac = ((complexe_float_t*)A);
    complexe_float_t *Bc = ((complexe_float_t*)B);
    complexe_float_t *Cc = ((complexe_float_t*)C);
    complexe_float_t alphac = (*(complexe_float_t*)alpha);
    complexe_float_t betac = (*(complexe_float_t*)beta);
    
    complexe_float_t tmp = {0,0};

    for (i = 0; i < M; i++) {
        for (j = 0; j < M; j++) {
            Cc[i * M +j] = mult_complexe_float (Cc[i * M +j], betac);
            tmp.imaginary = 0;
            tmp.real = 0;
            for (k = 0; k < M; k++) { 
                tmp = add_complexe_float(tmp,mult_complexe_float(Ac[i * M + k], Bc[k * M + j]));
            }
            Cc[i * M + j] = add_complexe_float(Cc[i * M + j],mult_complexe_float (alphac, tmp));
        }
    }
}

void mncblas_zgemm(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const void *alpha, const void *A,
                 const int lda, const void *B, const int ldb,
                 const void *beta, void *C, const int ldc)
{
    register unsigned int i;
    register unsigned int j;
    register unsigned int k;
    complexe_double_t *Ac = ((complexe_double_t*)A);
    complexe_double_t *Bc = ((complexe_double_t*)B);
    complexe_double_t *Cc = ((complexe_double_t*)C);
    complexe_double_t alphac = (*(complexe_double_t*)alpha);
    complexe_double_t betac = (*(complexe_double_t*)beta);
    
    complexe_double_t tmp = {0,0};

    for (i = 0; i < M; i++) {
        for (j = 0; j < M; j++) {
            Cc[i * M +j] = mult_complexe_double (Cc[i * M +j], betac);
            tmp.imaginary = 0;
            tmp.real = 0;
            for (k = 0; k < M; k++) {
                tmp = add_complexe_double(tmp,mult_complexe_double(Ac[i * M + k], Bc[k * M + j]));
            }
            Cc[i * M + j] = add_complexe_double(Cc[i * M + j],mult_complexe_double (alphac, tmp));
        }
    }
}