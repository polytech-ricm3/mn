#include "../include/complexe.h"
#include "../include/mnblas.h"

void mncblas_sgemv(const MNCBLAS_LAYOUT layout,
  const MNCBLAS_TRANSPOSE TransA, const int M, const int N,
  const float alpha, const float *A, const int lda,
  const float *X, const int incX, const float beta,
  float *Y, const int incY)
  {
    register unsigned int i, j;
    float tmp;
    for (i = 0; i < N; i+=incX)
    {
      tmp = 0;
      for (j = 0; j < M; j += incY)
      {
        tmp += A[i * lda + j] * X[j];
      }
      Y[i] = alpha * tmp + beta * Y[i];
    }

  }

void mncblas_dgemv(MNCBLAS_LAYOUT layout,
  MNCBLAS_TRANSPOSE TransA, const int M, const int N,
  const double alpha, const double *A, const int lda,
  const double *X, const int incX, const double beta,
  double *Y, const int incY)
  {
    register unsigned int i, j;
    double tmp;
    for (i = 0; i < N; i+=incX)
    {
      tmp = 0;
      for (j = 0; j < M; j += incY)
      {
        tmp += A[i  * lda + j] * X[j];
      }
      Y[i] = alpha * tmp + beta * Y[i];
    }

  }

void mncblas_cgemv(MNCBLAS_LAYOUT layout,
  MNCBLAS_TRANSPOSE TransA, const int M, const int N,
  const void *alpha, const void *A, const int lda,
  const void *X, const int incX, const void *beta,
  void *Y, const int incY)
  {
    register unsigned int i, j;
    complexe_float_t *Xc = ((complexe_float_t *)X);
    complexe_float_t *Ac = ((complexe_float_t *)A);
    complexe_float_t *Yc = ((complexe_float_t *)Y);

    register complexe_float_t tmp = {0,0};
    complexe_float_t betac = (*(complexe_float_t*)beta);
    complexe_float_t alphac = (*(complexe_float_t*)alpha);
    for (i = 0; i < N; i+=incX)
    {
      tmp.imaginary = 0;
      tmp.real = 0;
      for (j = 0; j < M; j += incY)
      {
        tmp = add_complexe_float(tmp,mult_complexe_float (Ac[i* lda + j], Xc[j]));
      }
      Yc[i] = add_complexe_float(mult_complexe_float (alphac, tmp), mult_complexe_float (betac, Yc[i]));
    }
  }

void mncblas_zgemv(MNCBLAS_LAYOUT layout,
  MNCBLAS_TRANSPOSE TransA, const int M, const int N,
  const void *alpha, const void *A, const int lda,
  const void *X, const int incX, const void *beta,
  void *Y, const int incY)
  {
    register unsigned int i, j;
    complexe_double_t *Xc = ((complexe_double_t *)X);
    complexe_double_t *Ac = ((complexe_double_t *)A);
    complexe_double_t *Yc = ((complexe_double_t *)Y);

    register complexe_double_t tmp = {0,0};
    complexe_double_t betac = (*(complexe_double_t*)beta);
    complexe_double_t alphac = (*(complexe_double_t*)alpha);
    for (i = 0; i < N; i+=incX)
    {
      tmp.imaginary = 0;
      tmp.real = 0;
      for (j = 0; j < M; j += incY)
      {
        tmp = add_complexe_double(tmp,mult_complexe_double (Ac[i* lda + j], Xc[j]));
      }
      Yc[i] = add_complexe_double(mult_complexe_double (alphac, tmp), mult_complexe_double (betac, Yc[i]));
    }
  }
