#include "../include/mnblas.h"
#include "../include/complexe.h"

#include <math.h>

float  mnblas_snrm2(const int N, const float *X, const int incX)
{
  float res = 0;
  float tmp = 0;
  register unsigned int i = 0;
  for(; i < N; i += incX)
      tmp += powf(X[i], 2);
  res = sqrtf(tmp);
  return res;
}

double mnblas_dnrm2(const int N, const double *X, const int incX)
{
  double res = 0;
  double tmp = 0;
  register unsigned int i = 0;
  for(; i < N; i += incX)
      tmp += pow(X[i], 2);
  res = sqrt(tmp);
  return res;
  
}

float  mnblas_scnrm2(const int N, const void *X, const int incX)
{
  float res = 0;
  float tmp = 0;
  register unsigned int i = 0;
  for(; i < N; i += incX)
      tmp+= powf((((complexe_float_t *) X)[i].real), 2) + powf((((complexe_float_t *) X)[i].imaginary),2);
  res = sqrtf(tmp);
  return res;
}

double mnblas_dznrm2(const int N, const void *X, const int incX)
{
  double res = 0;
  double tmp = 0;
  register unsigned int i = 0;
  for(; i < N; i += incX)
      tmp+= pow((((complexe_double_t *) X)[i].real), 2) + pow((((complexe_double_t *) X)[i].imaginary),2);
  res = sqrt(tmp);
  return res;
}
