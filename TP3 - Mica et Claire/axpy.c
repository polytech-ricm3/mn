#include "complexe.h"
#include "mnblas.h"
#include <stdio.h>

#include <immintrin.h>
#include <omp.h>
#include <smmintrin.h>
#include <x86intrin.h>

void mnblas_saxpy(const int N, const float alpha, const float *X,
                  const int incX, float *Y, const int incY) {
  register unsigned int i = 0;
  register unsigned int j = 0;

  for (; ((i < N) && (j < N)); i += incX, j += incY) {
    Y[j] += alpha * X[i];
  }

  return;
}

void mnblas_daxpy(const int N, const double alpha, const double *X,
                  const int incX, double *Y, const int incY) {
  register unsigned int i = 0;
  register unsigned int j = 0;

  for (; ((i < N) && (j < N)); i += incX, j += incY) {
    Y[j] += alpha * X[i];
  }

  return;
}

void mnblas_caxpy(const int N, const void *alpha, const void *X, const int incX,
                  void *Y, const int incY) {
  register unsigned int i = 0;
  register unsigned int j = 0;
  register complexe_float_t tmp;

  for (; ((i < N) && (j < N)); i += incX, j += incY) {
    tmp = mult_complexe_float(*(complexe_float_t *)alpha,
                              ((complexe_float_t *)X)[i]);
    ((complexe_float_t *)Y)[j] =
        add_complexe_float(((complexe_float_t *)Y)[j], tmp);
  }

  return;
}

void mnblas_zaxpy(const int N, const void *alpha, const void *X, const int incX,
                  void *Y, const int incY) {
  register unsigned int i = 0;
  register unsigned int j = 0;
  register complexe_double_t tmp;

  for (; ((i < N) && (j < N)); i += incX, j += incY) {
    tmp = mult_complexe_double(*(complexe_double_t *)alpha,
                               ((complexe_double_t *)X)[i]);
    ((complexe_double_t *)Y)[j] =
        add_complexe_double(((complexe_double_t *)Y)[j], tmp);
  }

  return;
}

/*---------------OpenMP--------------------*/
void mnblas_saxpy_para(const int N, const float alpha, const float *X,
                       const int incX, float *Y, const int incY) {
  register unsigned int i;

#pragma omp parallel for schedule(static)
  for (i = 0; i < N; i++) {
    Y[i] += alpha * X[i];
  }

  return;
}

void mnblas_daxpy_para(const int N, const double alpha, const double *X,
                       const int incX, double *Y, const int incY) {
  register unsigned int i;

#pragma omp parallel for schedule(static)
  for (i = 0; i < N; i++) {
    Y[i] += alpha * X[i];
  }

  return;
}

void mnblas_caxpy_para(const int N, const void *alpha, const void *X,
                       const int incX, void *Y, const int incY) {
  register unsigned int i;
  register complexe_float_t tmp;

#pragma omp parallel for schedule(static)
  for (i = 0; i < N; i++) {
    tmp = mult_complexe_float(*(complexe_float_t *)alpha,
                              ((complexe_float_t *)X)[i]);
    ((complexe_float_t *)Y)[i] =
        add_complexe_float(((complexe_float_t *)Y)[i], tmp);
  }

  return;
}

void mnblas_zaxpy_para(const int N, const void *alpha, const void *X,
                       const int incX, void *Y, const int incY) {
  register unsigned int i;
  register complexe_double_t tmp;

#pragma omp parallel for schedule(static)
  for (i = 0; i < N; i++) {
    tmp = mult_complexe_double(*(complexe_double_t *)alpha,
                               ((complexe_double_t *)X)[i]);
    ((complexe_double_t *)Y)[i] =
        add_complexe_double(((complexe_double_t *)Y)[i], tmp);
  }

  return;
}
/*----------------Vectorisation-------------------*/
void mnblas_saxpy_vect(const int N, const float alpha, const float *X,
                       const int incX, float *Y, const int incY) {
  register unsigned int i;
  __m128 x, y, alpha_m = _mm_set1_ps(alpha), tmp;

  for (i = 0; i < N; i += 4) {
    x = _mm_load_ps(X + i);
    y = _mm_load_ps(Y + i);
    tmp = _mm_mul_ps(x, alpha_m);
    tmp = _mm_add_ps(tmp, y);

    _mm_store_ps((Y + i), tmp);
  }
  return;
}

void mnblas_daxpy_vect(const int N, const double alpha, const double *X,
                       const int incX, double *Y, const int incY) {
  register unsigned int i;
  __m128d x, y, alpha_m = _mm_set1_pd(alpha), tmp;

  for (i = 0; i < N; i += 2) {
    x = _mm_load_pd(X + i);
    y = _mm_load_pd(Y + i);
    tmp = _mm_mul_pd(x, alpha_m);
    tmp = _mm_add_pd(tmp, y);

    _mm_store_pd((Y + i), tmp);
  }
  return;
}

void mnblas_caxpy_vect(const int N, const void *alpha, const void *X,
                       const int incX, void *Y, const int incY) {
  register unsigned int i;
  register unsigned int j;

  __m128 x_real, x_imaginary, y_real, y_imaginary, alpha_real, alpha_imaginary,
      tmp_real, tmp_imaginary;

  register __m128i index = _mm_set_epi32(8 * 3, 8 * 2, 8 * 1, 0);

  complexe_float_t *x = (complexe_float_t *)X;
  complexe_float_t *y = (complexe_float_t *)Y;
  complexe_float_t *Alpha = (complexe_float_t *)alpha;

  float tab_real[4];
  float tab_imaginary[4];

  for (i = 0; i < N; i += 4) {
    x_real = _mm_i32gather_ps(((const float *)(x + i)), index, 1);
    x_imaginary =
        _mm_i32gather_ps(((const float *)&((x + i)->imaginary)), index, 1);
    y_real = _mm_i32gather_ps(((const float *)(y + i)), index, 1);
    y_imaginary =
        _mm_i32gather_ps(((const float *)&((y + i)->imaginary)), index, 1);
    alpha_real = _mm_i32gather_ps(((const float *)(Alpha)), index, 1);
    alpha_imaginary =
        _mm_i32gather_ps(((const float *)&((Alpha)->imaginary)), index, 1);

    mult_complexe_float_vect(&x_real, &x_imaginary, &alpha_real,
                             &alpha_imaginary, &tmp_real, &tmp_imaginary);

    tmp_real = _mm_add_ps(tmp_real, y_real);
    tmp_imaginary = _mm_add_ps(tmp_imaginary, y_imaginary);

    _mm_store_ps(tab_real, tmp_real);
    _mm_store_ps(tab_imaginary, tmp_imaginary);

    for (j = 0; j < 4; j++) {
      (y + i + j)->real = tab_real[j];
      (y + i + j)->imaginary = tab_imaginary[j];
    }
  }

  return;
}

void mnblas_zaxpy_vect(const int N, const void *alpha, const void *X,
                       const int incX, void *Y, const int incY) {
  register unsigned int i;
  register unsigned int j;

  __m128d x_real, x_imaginary, y_real, y_imaginary, alpha_real, alpha_imaginary,
      tmp_real, tmp_imaginary;

  register __m128i index = _mm_set_epi32(0, 0, 16, 0);

  complexe_double_t *x = (complexe_double_t *)X;
  complexe_double_t *y = (complexe_double_t *)Y;
  complexe_double_t *Alpha = (complexe_double_t *)alpha;

  double tab_real[2];
  double tab_imaginary[2];

  for (i = 0; i < N; i += 2) {
    x_real = _mm_i32gather_pd(((const double *)(x + i)), index, 1);
    x_imaginary =
        _mm_i32gather_pd(((const double *)&((x + i)->imaginary)), index, 1);
    y_real = _mm_i32gather_pd(((const double *)(y + i)), index, 1);
    y_imaginary =
        _mm_i32gather_pd(((const double *)&((y + i)->imaginary)), index, 1);
    alpha_real = _mm_i32gather_pd(((const double *)(Alpha)), index, 1);
    alpha_imaginary =
        _mm_i32gather_pd(((const double *)&((Alpha)->imaginary)), index, 1);

    mult_complexe_double_vect(&x_real, &x_imaginary, &alpha_real,
                              &alpha_imaginary, &tmp_real, &tmp_imaginary);

    tmp_real = _mm_add_pd(tmp_real, y_real);
    tmp_imaginary = _mm_add_pd(tmp_imaginary, y_imaginary);

    _mm_store_pd(tab_real, tmp_real);
    _mm_store_pd(tab_imaginary, tmp_imaginary);

    for (j = 0; j < 2; j++) {
      (y + i + j)->real = tab_real[j];
      (y + i + j)->imaginary = tab_imaginary[j];
    }
  }

  return;
}

/*-------------Vectorisation + Parrallélisation--------------*/
void mnblas_saxpy_both(const int N, const float alpha, const float *X,
                       const int incX, float *Y, const int incY) {
  register unsigned int i;
  __m128 x, y, alpha_m = _mm_set1_ps(alpha), tmp;

#pragma omp parallel for schedule(dynamic)
  for (i = 0; i < N; i += 4) {
    x = _mm_load_ps(X + i);
    y = _mm_load_ps(Y + i);
    tmp = _mm_mul_ps(x, alpha_m);
    tmp = _mm_add_ps(tmp, y);

    _mm_store_ps((Y + i), tmp);
  }
  return;
}

void mnblas_daxpy_both(const int N, const double alpha, const double *X,
                       const int incX, double *Y, const int incY) {
  register unsigned int i;
  __m128d x, y, alpha_m = _mm_set1_pd(alpha), tmp;

#pragma omp parallel for schedule(dynamic)
  for (i = 0; i < N; i += 2) {
    x = _mm_load_pd(X + i);
    y = _mm_load_pd(Y + i);
    tmp = _mm_mul_pd(x, alpha_m);
    tmp = _mm_add_pd(tmp, y);

    _mm_store_pd((Y + i), tmp);
  }
  return;
}

void mnblas_caxpy_both(const int N, const void *alpha, const void *X,
                       const int incX, void *Y, const int incY) {
  register unsigned int i;
  register unsigned int j;

  __m128 x_real, x_imaginary, y_real, y_imaginary, alpha_real, alpha_imaginary,
      tmp_real, tmp_imaginary;

  register __m128i index = _mm_set_epi32(8 * 3, 8 * 2, 8 * 1, 0);

  complexe_float_t *x = (complexe_float_t *)X;
  complexe_float_t *y = (complexe_float_t *)Y;
  complexe_float_t *Alpha = (complexe_float_t *)alpha;

  float tab_real[4];
  float tab_imaginary[4];

#pragma omp parallel for schedule(dynamic)
  for (i = 0; i < N; i += 4) {
    x_real = _mm_i32gather_ps(((const float *)(x + i)), index, 1);
    x_imaginary =
        _mm_i32gather_ps(((const float *)&((x + i)->imaginary)), index, 1);
    y_real = _mm_i32gather_ps(((const float *)(y + i)), index, 1);
    y_imaginary =
        _mm_i32gather_ps(((const float *)&((y + i)->imaginary)), index, 1);
    alpha_real = _mm_i32gather_ps(((const float *)(Alpha)), index, 1);
    alpha_imaginary =
        _mm_i32gather_ps(((const float *)&((Alpha)->imaginary)), index, 1);

    mult_complexe_float_vect(&x_real, &x_imaginary, &alpha_real,
                             &alpha_imaginary, &tmp_real, &tmp_imaginary);

    tmp_real = _mm_add_ps(tmp_real, y_real);
    tmp_imaginary = _mm_add_ps(tmp_imaginary, y_imaginary);

    _mm_store_ps(tab_real, tmp_real);
    _mm_store_ps(tab_imaginary, tmp_imaginary);

    for (j = 0; j < 4; j++) {
      (y + i + j)->real = tab_real[j];
      (y + i + j)->imaginary = tab_imaginary[j];
    }
  }

  return;
}

void mnblas_zaxpy_both(const int N, const void *alpha, const void *X,
                       const int incX, void *Y, const int incY) {
  register unsigned int i;
  register unsigned int j;

  __m128d x_real, x_imaginary, y_real, y_imaginary, alpha_real, alpha_imaginary,
      tmp_real, tmp_imaginary;

  register __m128i index = _mm_set_epi32(0, 0, 16, 0);

  complexe_double_t *x = (complexe_double_t *)X;
  complexe_double_t *y = (complexe_double_t *)Y;
  complexe_double_t *Alpha = (complexe_double_t *)alpha;

  double tab_real[2];
  double tab_imaginary[2];

#pragma omp parallel for schedule(dynamic)
  for (i = 0; i < N; i += 2) {
    x_real = _mm_i32gather_pd(((const double *)(x + i)), index, 1);
    x_imaginary =
        _mm_i32gather_pd(((const double *)&((x + i)->imaginary)), index, 1);
    y_real = _mm_i32gather_pd(((const double *)(y + i)), index, 1);
    y_imaginary =
        _mm_i32gather_pd(((const double *)&((y + i)->imaginary)), index, 1);
    alpha_real = _mm_i32gather_pd(((const double *)(Alpha)), index, 1);
    alpha_imaginary =
        _mm_i32gather_pd(((const double *)&((Alpha)->imaginary)), index, 1);

    mult_complexe_double_vect(&x_real, &x_imaginary, &alpha_real,
                              &alpha_imaginary, &tmp_real, &tmp_imaginary);

    tmp_real = _mm_add_pd(tmp_real, y_real);
    tmp_imaginary = _mm_add_pd(tmp_imaginary, y_imaginary);

    _mm_store_pd(tab_real, tmp_real);
    _mm_store_pd(tab_imaginary, tmp_imaginary);

    for (j = 0; j < 2; j++) {
      (y + i + j)->real = tab_real[j];
      (y + i + j)->imaginary = tab_imaginary[j];
    }
  }

  return;
}
