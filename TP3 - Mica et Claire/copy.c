#include "complexe.h"
#include "mnblas.h"
#include <stdio.h>

#include <omp.h>
#include <smmintrin.h>
#include <x86intrin.h>

void mncblas_scopy(const int N, const float *X, const int incX, float *Y,
                   const int incY) {
  register unsigned int i = 0;
  register unsigned int j = 0;

  for (; ((i < N) && (j < N)); i += incX, j += incY) {
    Y[j] = X[i];
  }

  return;
}

void mncblas_dcopy(const int N, const double *X, const int incX, double *Y,
                   const int incY) {
  register unsigned int i = 0;
  register unsigned int j = 0;

  for (; ((i < N) && (j < N)); i += incX, j += incY) {
    Y[j] = X[i];
  }

  return;
}

void mncblas_ccopy(const int N, const void *X, const int incX, void *Y,
                   const int incY) {
  register unsigned int i = 0;
  register unsigned int j = 0;

  for (; ((i < N) && (j < N)); i += incX, j += incY) {
    ((complexe_float_t *)Y)[j].real = ((complexe_float_t *)X)[i].real;
    ((complexe_float_t *)Y)[j].imaginary = ((complexe_float_t *)X)[i].imaginary;
  }

  return;
}

void mncblas_zcopy(const int N, const void *X, const int incX, void *Y,
                   const int incY) {
  register unsigned int i = 0;
  register unsigned int j = 0;

  for (; ((i < N) && (j < N)); i += incX, j += incY) {
    ((complexe_double_t *)Y)[j].real = ((complexe_double_t *)X)[i].real;
    ((complexe_double_t *)Y)[j].imaginary =
        ((complexe_double_t *)X)[i].imaginary;
  }

  return;
}

/*-------------OpenMP--------------------*/
void mncblas_scopy_para(const int N, const float *X, const int incX, float *Y,
                        const int incY) {
  register unsigned int i;

#pragma omp parallel for schedule(static)
  for (i = 0; i < N; i++) {
    Y[i] = X[i];
  }

  return;
}

void mncblas_dcopy_para(const int N, const double *X, const int incX, double *Y,
                        const int incY) {
  register unsigned int i;

#pragma omp parallel for schedule(static)
  for (i = 0; i < N; i++) {
    Y[i] = X[i];
  }

  return;
}

void mncblas_ccopy_para(const int N, const void *X, const int incX, void *Y,
                        const int incY) {
  register unsigned int i;

#pragma omp parallel for schedule(static)
  for (i = 0; i < N; i++) {
    ((complexe_float_t *)Y)[i].real = ((complexe_float_t *)X)[i].real;
    ((complexe_float_t *)Y)[i].imaginary = ((complexe_float_t *)X)[i].imaginary;
  }

  return;
}

void mncblas_zcopy_para(const int N, const void *X, const int incX, void *Y,
                        const int incY) {
  register unsigned int i;

#pragma omp parallel for schedule(static)
  for (i = 0; i < N; i++) {
    ((complexe_double_t *)Y)[i].real = ((complexe_double_t *)X)[i].real;
    ((complexe_double_t *)Y)[i].imaginary =
        ((complexe_double_t *)X)[i].imaginary;
  }

  return;
}

/*----------------Vectorisation--------------------*/
void mncblas_scopy_vect(const int N, const float *X, const int incX, float *Y,
                        const int incY) {
  register unsigned int i;
  __m128 x;

  for (i = 0; i < N; i += 4) {
    x = _mm_load_ps(X + i);
    _mm_store_ps((Y + i), x);
  }

  return;
}

void mncblas_dcopy_vect(const int N, const double *X, const int incX, double *Y,
                        const int incY) {
  register unsigned int i = 0;
  __m128d x;

  for (i = 0; i < N; i += 2) {
    x = _mm_load_pd(X + i);
    _mm_store_pd((Y + i), x);
  }

  return;
}

void mncblas_ccopy_vect(const int N, const void *X, const int incX, void *Y,
                        const int incY) {
  register unsigned int i;
  __m128 x_m;

  complexe_float_t *x = (complexe_float_t *)X;
  complexe_float_t *y = (complexe_float_t *)Y;

  for (i = 0; i < N; i += 4) {
    x_m = _mm_load_ps(((const float *)(x + i)));
    _mm_store_ps((float *)(y + i), x_m);
  }
  return;
}

void mncblas_zcopy_vect(const int N, const void *X, const int incX, void *Y,
                        const int incY) {
  register unsigned int i;
  __m128d x_m;

  complexe_double_t *x = (complexe_double_t *)X;
  complexe_double_t *y = (complexe_double_t *)Y;

  for (i = 0; i < N; i += 2) {
    x_m = _mm_load_pd(((const double *)(x + i)));
    _mm_store_pd((double *)(y + i), x_m);
  }

  return;
}

/*-------------Vectorisation + Parrallélisation--------------*/
void mncblas_scopy_both(const int N, const float *X, const int incX, float *Y,
                        const int incY) {
  register unsigned int i;
  __m128 x;

#pragma omp parallel for schedule(static)
  for (i = 0; i < N; i += 4) {
    x = _mm_load_ps(X + i);
    _mm_store_ps((Y + i), x);
  }

  return;
}

void mncblas_dcopy_both(const int N, const double *X, const int incX, double *Y,
                        const int incY) {
  register unsigned int i = 0;
  __m128d x;

#pragma omp parallel for schedule(static)
  for (i = 0; i < N; i += 2) {
    x = _mm_load_pd(X + i);
    _mm_store_pd((Y + i), x);
  }

  return;
}

void mncblas_ccopy_both(const int N, const void *X, const int incX, void *Y,
                        const int incY) {
  register unsigned int i;
  __m128 x_m;

  complexe_float_t *x = (complexe_float_t *)X;
  complexe_float_t *y = (complexe_float_t *)Y;

#pragma omp parallel for schedule(static)
  for (i = 0; i < N; i += 4) {
    x_m = _mm_load_ps(((const float *)(x + i)));
    _mm_store_ps((float *)(y + i), x_m);
  }
  return;
}

void mncblas_zcopy_both(const int N, const void *X, const int incX, void *Y,
                        const int incY) {
  register unsigned int i;
  __m128d x_m;

  complexe_double_t *x = (complexe_double_t *)X;
  complexe_double_t *y = (complexe_double_t *)Y;

#pragma omp parallel for schedule(static)
  for (i = 0; i < N; i += 2) {
    x_m = _mm_load_pd(((const double *)(x + i)));
    _mm_store_pd((double *)(y + i), x_m);
  }

  return;
}
