#include "complexe.h"
#include "mnblas.h"
#include <stdio.h>

#include <omp.h>
#include <smmintrin.h>
#include <x86intrin.h>

float mncblas_sdot(const int N, const float *X, const int incX, const float *Y,
                   const int incY) {

  if (N < 0) {
    return 0;
  }
  register unsigned int i = 0;
  register unsigned int j = 0;
  register float dot = 0.0;

  for (; ((i < N) && (j < N)); i += incX, j += incY) {
    dot = dot + X[i] * Y[j];
  }

  return dot;
}

double mncblas_ddot(const int N, const double *X, const int incX,
                    const double *Y, const int incY) {
  if (N < 0) {
    return 0;
  }
  register unsigned int i = 0;
  register unsigned int j = 0;
  register float dot = 0.0;

  for (; ((i < N) && (j < N)); i += incX, j += incY) {
    dot = dot + X[i] * Y[j];
  }

  return dot;
}

void mncblas_cdotu_sub(const int N, const void *X, const int incX,
                       const void *Y, const int incY, void *dotu) {

  register unsigned int i = 0;
  register unsigned int j = 0;

  for (; ((i < N) && (j < N)); i += incX, j += incY) {
    *(complexe_float_t *)dotu =
        add_complexe_float(*(complexe_float_t *)dotu,
                           mult_complexe_float(((complexe_float_t *)X)[i],
                                               ((complexe_float_t *)Y)[j]));
  }

  return;
}

void mncblas_cdotc_sub(const int N, const void *X, const int incX,
                       const void *Y, const int incY, void *dotc) {
  register unsigned int i = 0;
  register unsigned int j = 0;
  for (; ((i < N) && (j < N)); i += incX, j += incY) {
    *(complexe_float_t *)dotc = add_complexe_float(
        *(complexe_float_t *)dotc,
        mult_complexe_float(conjugue_complexe_float(((complexe_float_t *)X)[i]),
                            ((complexe_float_t *)Y)[j]));
  }

  return;
}

void mncblas_zdotu_sub(const int N, const void *X, const int incX,
                       const void *Y, const int incY, void *dotu) {
  register unsigned int i = 0;
  register unsigned int j = 0;
  for (; ((i < N) && (j < N)); i += incX, j += incY) {
    *(complexe_double_t *)dotu =
        add_complexe_double(*(complexe_double_t *)dotu,
                            mult_complexe_double(((complexe_double_t *)X)[i],
                                                 ((complexe_double_t *)Y)[j]));
  }

  return;
}

void mncblas_zdotc_sub(const int N, const void *X, const int incX,
                       const void *Y, const int incY, void *dotc) {
  register unsigned int i = 0;
  register unsigned int j = 0;
  for (; ((i < N) && (j < N)); i += incX, j += incY) {
    *(complexe_double_t *)dotc = add_complexe_double(
        *(complexe_double_t *)dotc,
        mult_complexe_double(
            conjugue_complexe_double(((complexe_double_t *)X)[i]),
            ((complexe_double_t *)Y)[j]));
  }

  return;
}

/* ---------------OpenMP-----------------------*/
float mncblas_sdot_para(const int N, const float *X, const int incX,
                        const float *Y, const int incY) {

  if (N < 0) {
    return 0;
  }
  register unsigned int i;
  register float dot = 0.0;

#pragma omp parallel for schedule(static) reduction(+ : dot)
  for (i = 0; i < N; i++) {
    dot = dot + X[i] * Y[i];
  }

  return dot;
}

double mncblas_ddot_para(const int N, const double *X, const int incX,
                         const double *Y, const int incY) {
  if (N < 0) {
    return 0;
  }
  register unsigned int i;
  register float dot = 0.0;

#pragma omp parallel for schedule(static) reduction(+ : dot)
  for (i = 0; i < N; i++) {
    dot = dot + X[i] * Y[i];
  }

  return dot;
}

void mncblas_cdotu_sub_para(const int N, const void *X, const int incX,
                            const void *Y, const int incY, void *dotu) {

  register unsigned int i;

  const complexe_float_t *x = X;
  const complexe_float_t *y = Y;
  complexe_float_t *c_dotu = dotu;

  float real = 0;
  float imaginary = 0;

#pragma omp parallel for schedule(static) reduction(+: real) reduction(+: imaginary)
  for (i = 0; i < N; i++) {
    real += x[i].real * y[i].real - x[i].imaginary * y[i].imaginary;
    imaginary += x[i].real * y[i].imaginary + x[i].imaginary * y[i].real;
  }

  c_dotu->real = real;
  c_dotu->imaginary = imaginary;

  return;
}

void mncblas_cdotc_sub_para(const int N, const void *X, const int incX,
                            const void *Y, const int incY, void *dotc) {
  register unsigned int i;

  const complexe_float_t *x = X;
  const complexe_float_t *y = Y;
  complexe_float_t *c_dotc = dotc;

  float real = 0;
  float imaginary = 0;

#pragma omp parallel for schedule(static) reduction(+: real) reduction(+: imaginary)
  for (i = 0; i < N; i++) {
    real += x[i].real * y[i].real + x[i].imaginary * y[i].imaginary;
    imaginary += x[i].real * y[i].imaginary - x[i].imaginary * y[i].real;
  }
  c_dotc->real = real;
  c_dotc->imaginary = imaginary;
  return;
}

void mncblas_zdotu_sub_para(const int N, const void *X, const int incX,
                            const void *Y, const int incY, void *dotu) {
  register unsigned int i;

  const complexe_double_t *x = X;
  const complexe_double_t *y = Y;
  complexe_double_t *z_dotu = dotu;

  double real = 0;
  double imaginary = 0;

#pragma omp parallel for schedule(static) reduction(+: real) reduction(+: imaginary)
  for (i = 0; i < N; i++) {
    real += x[i].real * y[i].real - x[i].imaginary * y[i].imaginary;
    imaginary += x[i].real * y[i].imaginary + x[i].imaginary * y[i].real;
  }

  z_dotu->real = real;
  z_dotu->imaginary = imaginary;

  return;
}

void mncblas_zdotc_sub_para(const int N, const void *X, const int incX,
                            const void *Y, const int incY, void *dotc) {
  register unsigned int i;

  const complexe_double_t *x = X;
  const complexe_double_t *y = Y;
  complexe_double_t *z_dotc = dotc;

  double real = 0;
  double imaginary = 0;

#pragma omp parallel for schedule(static) reduction(+: real) reduction(+: imaginary)
  for (i = 0; i < N; i++) {
    real += x[i].real * y[i].real + x[i].imaginary * y[i].imaginary;
    imaginary += x[i].real * y[i].imaginary - x[i].imaginary * y[i].real;
  }

  z_dotc->real = real;
  z_dotc->imaginary = imaginary;

  return;
}

/* ---------------Vectorisation-----------------------*/
float mncblas_sdot_vect(const int N, const float *X, const int incX,
                        const float *Y, const int incY) {

  if (N < 0) {
    return 0;
  }

  __m128 v1, v2, res;

  register unsigned int i;

  float dot[4] __attribute__((aligned(8)));
  ;
  float dot_to_return = 0.0;

  for (i = 0; i < N; i += 4) {
    v1 = _mm_load_ps((X + i));
    v2 = _mm_load_ps((Y + i));
    res = _mm_dp_ps(v1, v2, 0xFF);

    _mm_store_ps(dot, res);

    dot_to_return += dot[0];
  }

  return dot_to_return;
}

double mncblas_ddot_vect(const int N, const double *X, const int incX,
                         const double *Y, const int incY) {

  if (N < 0) {
    return 0;
  }

  __m128d v1, v2, res;

  register unsigned int i;
  double dot[2];
  double dot_to_return = 0.0;

  for (i = 0; i < N; i += 2) {
    v1 = _mm_load_pd(X + i);

    v2 = _mm_load_pd(Y + i);
    res = _mm_dp_pd(v1, v2, 0xFF);

    _mm_store_pd(dot, res);

    dot_to_return += dot[0];
  }

  return dot_to_return;
}

void mncblas_cdotu_sub_vect(const int N, const void *X, const int incX,
                            const void *Y, const int incY, void *dotu) {

  __m128 vx_real, vx_imaginary, vy_real, vy_imaginary, res_real, res_imaginary,
      dotu_m_real = _mm_set1_ps(0), dotu_m_imaginary = _mm_set1_ps(0);

  register __m128i index = _mm_set_epi32(8 * 3, 8 * 2, 8 * 1, 0);

  register unsigned int i, j;

  complexe_float_t *x = (complexe_float_t *)X;
  complexe_float_t *y = (complexe_float_t *)Y;
  complexe_float_t *dotu_c = (complexe_float_t *)dotu;

  float tab_real[4];
  float tab_imaginary[4];

  for (i = 0; i < N; i += 4) {
    vx_real = _mm_i32gather_ps(((const float *)(x + i)), index, 1);
    vx_imaginary =
        _mm_i32gather_ps(((const float *)&((x + i)->imaginary)), index, 1);
    vy_real = _mm_i32gather_ps(((const float *)(y + i)), index, 1);
    vy_imaginary =
        _mm_i32gather_ps(((const float *)&((y + i)->imaginary)), index, 1);

    mult_complexe_float_vect(&vx_real, &vx_imaginary, &vy_real, &vy_imaginary,
                             &res_real, &res_imaginary);

    dotu_m_real = _mm_add_ps(dotu_m_real, res_real);
    dotu_m_imaginary = _mm_add_ps(dotu_m_imaginary, res_imaginary);
  }
  _mm_store_ps(tab_real, dotu_m_real);
  _mm_store_ps(tab_imaginary, dotu_m_imaginary);

  (dotu_c)->real = 0;
  (dotu_c)->imaginary = 0;

  for (j = 0; j < 4; j++) {
    (dotu_c)->real = (dotu_c)->real + tab_real[j];
    (dotu_c)->imaginary = (dotu_c)->imaginary + tab_imaginary[j];
  }
  return;
}

void mncblas_cdotc_sub_vect(const int N, const void *X, const int incX,
                            const void *Y, const int incY, void *dotc) {
  __m128 vx_real, vx_imaginary, vy_real, vy_imaginary, res_real, res_imaginary,
      dotu_m_real = _mm_set1_ps(0), dotu_m_imaginary = _mm_set1_ps(0),
      norme = _mm_set1_ps(-1);

  register __m128i index = _mm_set_epi32(8 * 3, 8 * 2, 8 * 1, 0);

  register unsigned int i, j;

  complexe_float_t *x = (complexe_float_t *)X;
  complexe_float_t *y = (complexe_float_t *)Y;
  complexe_float_t *dotu_c = (complexe_float_t *)dotc;

  float tab_real[4];
  float tab_imaginary[4];

  for (i = 0; i < N; i += 4) {
    vx_real = _mm_i32gather_ps(((const float *)(x + i)), index, 1);
    vx_imaginary =
        _mm_i32gather_ps(((const float *)&((x + i)->imaginary)), index, 1);
    vy_real = _mm_i32gather_ps(((const float *)(y + i)), index, 1);
    vy_imaginary =
        _mm_i32gather_ps(((const float *)&((y + i)->imaginary)), index, 1);

    vx_imaginary = _mm_mul_ps(vx_imaginary, norme);

    mult_complexe_float_vect(&vx_real, &vx_imaginary, &vy_real, &vy_imaginary,
                             &res_real, &res_imaginary);

    dotu_m_real = _mm_add_ps(dotu_m_real, res_real);
    dotu_m_imaginary = _mm_add_ps(dotu_m_imaginary, res_imaginary);
  }

  _mm_store_ps(tab_real, dotu_m_real);
  _mm_store_ps(tab_imaginary, dotu_m_imaginary);

  (dotu_c)->real = 0;
  (dotu_c)->imaginary = 0;

  for (j = 0; j < 4; j++) {
    (dotu_c)->real = (dotu_c)->real + tab_real[j];
    (dotu_c)->imaginary = (dotu_c)->imaginary + tab_imaginary[j];
  }
  return;
}

void mncblas_zdotu_sub_vect(const int N, const void *X, const int incX,
                            const void *Y, const int incY, void *dotu) {
  __m128d vx_real, vx_imaginary, vy_real, vy_imaginary, res_real, res_imaginary,
      dotu_m_real = _mm_set1_pd(0), dotu_m_imaginary = _mm_set1_pd(0);

  register __m128i index = _mm_set_epi32(0, 0, 16, 0);

  register unsigned int i, j;

  complexe_double_t *x = (complexe_double_t *)X;
  complexe_double_t *y = (complexe_double_t *)Y;
  complexe_double_t *dotu_z = (complexe_double_t *)dotu;

  double tab_real[2];
  double tab_imaginary[2];

  for (i = 0; i < N; i += 2) {
    vx_real = _mm_i32gather_pd(((const double *)(x + i)), index, 1);
    vx_imaginary =
        _mm_i32gather_pd(((const double *)&((x + i)->imaginary)), index, 1);
    vy_real = _mm_i32gather_pd(((const double *)(y + i)), index, 1);
    vy_imaginary =
        _mm_i32gather_pd(((const double *)&((y + i)->imaginary)), index, 1);

    mult_complexe_double_vect(&vx_real, &vx_imaginary, &vy_real, &vy_imaginary,
                              &res_real, &res_imaginary);

    dotu_m_real = _mm_add_pd(dotu_m_real, res_real);
    dotu_m_imaginary = _mm_add_pd(dotu_m_imaginary, res_imaginary);
  }

  _mm_store_pd(tab_real, dotu_m_real);
  _mm_store_pd(tab_imaginary, dotu_m_imaginary);

  (dotu_z)->real = 0;
  (dotu_z)->imaginary = 0;

  for (j = 0; j < 2; j++) {
    (dotu_z)->real = (dotu_z)->real + tab_real[j];
    (dotu_z)->imaginary = (dotu_z)->imaginary + tab_imaginary[j];
  }
  return;
}

void mncblas_zdotc_sub_vect(const int N, const void *X, const int incX,
                            const void *Y, const int incY, void *dotc) {
  __m128d vx_real, vx_imaginary, vy_real, vy_imaginary, res_real, res_imaginary,
      dotu_m_real = _mm_set1_pd(0), dotu_m_imaginary = _mm_set1_pd(0),
      norme = _mm_set1_pd(-1);

  register __m128i index = _mm_set_epi32(0, 0, 16, 0);

  register unsigned int i, j;

  complexe_double_t *x = (complexe_double_t *)X;
  complexe_double_t *y = (complexe_double_t *)Y;
  complexe_double_t *dotu_z = (complexe_double_t *)dotc;

  double tab_real[2];
  double tab_imaginary[2];

  for (i = 0; i < N; i += 2) {
    vx_real = _mm_i32gather_pd(((const double *)(x + i)), index, 1);
    vx_imaginary =
        _mm_i32gather_pd(((const double *)&((x + i)->imaginary)), index, 1);
    vy_real = _mm_i32gather_pd(((const double *)(y + i)), index, 1);
    vy_imaginary =
        _mm_i32gather_pd(((const double *)&((y + i)->imaginary)), index, 1);

    vx_imaginary = _mm_mul_pd(vx_imaginary, norme);
    mult_complexe_double_vect(&vx_real, &vx_imaginary, &vy_real, &vy_imaginary,
                              &res_real, &res_imaginary);

    dotu_m_real = _mm_add_pd(dotu_m_real, res_real);
    dotu_m_imaginary = _mm_add_pd(dotu_m_imaginary, res_imaginary);
  }

  _mm_store_pd(tab_real, dotu_m_real);
  _mm_store_pd(tab_imaginary, dotu_m_imaginary);

  (dotu_z)->real = 0;
  (dotu_z)->imaginary = 0;

  for (j = 0; j < 2; j++) {
    (dotu_z)->real = (dotu_z)->real + tab_real[j];
    (dotu_z)->imaginary = (dotu_z)->imaginary + tab_imaginary[j];
  }
  return;
}

/*-------------Vectorisation + Parrallélisation--------------*/
float mncblas_sdot_both(const int N, const float *X, const int incX,
                        const float *Y, const int incY) {

  if (N < 0) {
    return 0;
  }

  __m128 v1, v2, res;

  register unsigned int i;

  float dot[4];
  float dot_to_return = 0.0;

#pragma omp parallel for schedule(static) reduction(+ : dot_to_return) private(v1, v2, res, dot)
  for (i = 0; i < N; i += 4) {
    v1 = _mm_load_ps((X + i));
    v2 = _mm_load_ps((Y + i));
    res = _mm_dp_ps(v1, v2, 0xFF);

    _mm_store_ps(dot, res);

    dot_to_return += dot[0];
  }

  return dot_to_return;
}

double mncblas_ddot_both(const int N, const double *X, const int incX,
                         const double *Y, const int incY) {

  if (N < 0) {
    return 0;
  }

  __m128d v1, v2, res;

  register unsigned int i;
  double dot[2];
  double dot_to_return = 0.0;

#pragma omp parallel for schedule(static) reduction(+ : dot_to_return) private(v1, v2, res, dot)
  for (i = 0; i < N; i += 2) {
    v1 = _mm_load_pd(X + i);

    v2 = _mm_load_pd(Y + i);
    res = _mm_dp_pd(v1, v2, 0xFF);

    _mm_store_pd(dot, res);

    dot_to_return += dot[0];
  }

  return dot_to_return;
}

void mncblas_cdotu_sub_both(const int N, const void *X, const int incX,
                            const void *Y, const int incY, void *dotu) {

  __m128 vx_real, vx_imaginary, vy_real, vy_imaginary, res_real, res_imaginary,
      dotu_m_real = _mm_set1_ps(0), dotu_m_imaginary = _mm_set1_ps(0);

  register __m128i index = _mm_set_epi32(8 * 3, 8 * 2, 8 * 1, 0);

  register unsigned int i, j;

  complexe_float_t *x = (complexe_float_t *)X;
  complexe_float_t *y = (complexe_float_t *)Y;
  complexe_float_t *dotu_c = (complexe_float_t *)dotu;

  float tab_real[4];
  float tab_imaginary[4];
  // float tmp_real, tmp_imaginary;

#pragma omp parallel for schedule(static) private(                             \
    vx_real, vx_imaginary, vy_real, vy_imaginary, res_real, res_imaginary,     \
    dotu_m_real, dotu_m_imaginary)
  for (i = 0; i < N; i += 4) {
    vx_real = _mm_i32gather_ps(((const float *)(x + i)), index, 1);
    vx_imaginary =
        _mm_i32gather_ps(((const float *)&((x + i)->imaginary)), index, 1);
    vy_real = _mm_i32gather_ps(((const float *)(y + i)), index, 1);
    vy_imaginary =
        _mm_i32gather_ps(((const float *)&((y + i)->imaginary)), index, 1);

    mult_complexe_float_vect(&vx_real, &vx_imaginary, &vy_real, &vy_imaginary,
                             &res_real, &res_imaginary);

    dotu_m_real = _mm_add_ps(dotu_m_real, res_real);
    dotu_m_imaginary = _mm_add_ps(dotu_m_imaginary, res_imaginary);
  }
  _mm_store_ps(tab_real, dotu_m_real);
  _mm_store_ps(tab_imaginary, dotu_m_imaginary);

  (dotu_c)->real = 0;
  (dotu_c)->imaginary = 0;

  for (j = 0; j < 4; j++) {
    (dotu_c)->real = (dotu_c)->real + tab_real[j];
    (dotu_c)->imaginary = (dotu_c)->imaginary + tab_imaginary[j];
  }

  return;
}

void mncblas_cdotc_sub_both(const int N, const void *X, const int incX,
                            const void *Y, const int incY, void *dotc) {
  __m128 vx_real, vx_imaginary, vy_real, vy_imaginary, res_real, res_imaginary,
      dotu_m_real = _mm_set1_ps(0), dotu_m_imaginary = _mm_set1_ps(0),
      norme = _mm_set1_ps(-1);

  register __m128i index = _mm_set_epi32(8 * 3, 8 * 2, 8 * 1, 0);

  register unsigned int i, j;

  complexe_float_t *x = (complexe_float_t *)X;
  complexe_float_t *y = (complexe_float_t *)Y;
  complexe_float_t *dotu_c = (complexe_float_t *)dotc;

  float tab_real[4];
  float tab_imaginary[4];

#pragma omp parallel for schedule(static) private(                             \
    vx_real, vx_imaginary, vy_real, vy_imaginary, res_real, res_imaginary,     \
    dotu_m_real, dotu_m_imaginary)
  for (i = 0; i < N; i += 4) {
    vx_real = _mm_i32gather_ps(((const float *)(x + i)), index, 1);
    vx_imaginary =
        _mm_i32gather_ps(((const float *)&((x + i)->imaginary)), index, 1);
    vy_real = _mm_i32gather_ps(((const float *)(y + i)), index, 1);
    vy_imaginary =
        _mm_i32gather_ps(((const float *)&((y + i)->imaginary)), index, 1);

    vx_imaginary = _mm_mul_ps(vx_imaginary, norme);

    mult_complexe_float_vect(&vx_real, &vx_imaginary, &vy_real, &vy_imaginary,
                             &res_real, &res_imaginary);

    dotu_m_real = _mm_add_ps(dotu_m_real, res_real);
    dotu_m_imaginary = _mm_add_ps(dotu_m_imaginary, res_imaginary);
  }

  _mm_store_ps(tab_real, dotu_m_real);
  _mm_store_ps(tab_imaginary, dotu_m_imaginary);

  (dotu_c)->real = 0;
  (dotu_c)->imaginary = 0;

  for (j = 0; j < 4; j++) {
    (dotu_c)->real = (dotu_c)->real + tab_real[j];
    (dotu_c)->imaginary = (dotu_c)->imaginary + tab_imaginary[j];
  }
  return;
}

void mncblas_zdotu_sub_both(const int N, const void *X, const int incX,
                            const void *Y, const int incY, void *dotu) {
  __m128d vx_real, vx_imaginary, vy_real, vy_imaginary, res_real, res_imaginary,
      dotu_m_real = _mm_set1_pd(0), dotu_m_imaginary = _mm_set1_pd(0);

  register __m128i index = _mm_set_epi32(0, 0, 16, 0);

  register unsigned int i, j;

  complexe_double_t *x = (complexe_double_t *)X;
  complexe_double_t *y = (complexe_double_t *)Y;
  complexe_double_t *dotu_z = (complexe_double_t *)dotu;

  double tab_real[2];
  double tab_imaginary[2];

#pragma omp parallel for schedule(static) private(                             \
    vx_real, vx_imaginary, vy_real, vy_imaginary, res_real, res_imaginary,     \
    dotu_m_real, dotu_m_imaginary)
  for (i = 0; i < N; i += 2) {
    vx_real = _mm_i32gather_pd(((const double *)(x + i)), index, 1);
    vx_imaginary =
        _mm_i32gather_pd(((const double *)&((x + i)->imaginary)), index, 1);
    vy_real = _mm_i32gather_pd(((const double *)(y + i)), index, 1);
    vy_imaginary =
        _mm_i32gather_pd(((const double *)&((y + i)->imaginary)), index, 1);

    mult_complexe_double_vect(&vx_real, &vx_imaginary, &vy_real, &vy_imaginary,
                              &res_real, &res_imaginary);

    dotu_m_real = _mm_add_pd(dotu_m_real, res_real);
    dotu_m_imaginary = _mm_add_pd(dotu_m_imaginary, res_imaginary);
  }

  _mm_store_pd(tab_real, dotu_m_real);
  _mm_store_pd(tab_imaginary, dotu_m_imaginary);

  (dotu_z)->real = 0;
  (dotu_z)->imaginary = 0;

  for (j = 0; j < 2; j++) {
    (dotu_z)->real = (dotu_z)->real + tab_real[j];
    (dotu_z)->imaginary = (dotu_z)->imaginary + tab_imaginary[j];
  }
  return;
}

void mncblas_zdotc_sub_both(const int N, const void *X, const int incX,
                            const void *Y, const int incY, void *dotc) {
  __m128d vx_real, vx_imaginary, vy_real, vy_imaginary, res_real, res_imaginary,
      dotu_m_real = _mm_set1_pd(0), dotu_m_imaginary = _mm_set1_pd(0),
      norme = _mm_set1_pd(-1);

  register __m128i index = _mm_set_epi32(0, 0, 16, 0);

  register unsigned int i, j;

  complexe_double_t *x = (complexe_double_t *)X;
  complexe_double_t *y = (complexe_double_t *)Y;
  complexe_double_t *dotu_z = (complexe_double_t *)dotc;

  double tab_real[2];
  double tab_imaginary[2];

#pragma omp parallel for schedule(static) private(                             \
    vx_real, vx_imaginary, vy_real, vy_imaginary, res_real, res_imaginary,     \
    dotu_m_real, dotu_m_imaginary)
  for (i = 0; i < N; i += 2) {
    vx_real = _mm_i32gather_pd(((const double *)(x + i)), index, 1);
    vx_imaginary =
        _mm_i32gather_pd(((const double *)&((x + i)->imaginary)), index, 1);
    vy_real = _mm_i32gather_pd(((const double *)(y + i)), index, 1);
    vy_imaginary =
        _mm_i32gather_pd(((const double *)&((y + i)->imaginary)), index, 1);

    vx_imaginary = _mm_mul_pd(vx_imaginary, norme);
    mult_complexe_double_vect(&vx_real, &vx_imaginary, &vy_real, &vy_imaginary,
                              &res_real, &res_imaginary);

    dotu_m_real = _mm_add_pd(dotu_m_real, res_real);
    dotu_m_imaginary = _mm_add_pd(dotu_m_imaginary, res_imaginary);
  }

  _mm_store_pd(tab_real, dotu_m_real);
  _mm_store_pd(tab_imaginary, dotu_m_imaginary);

  (dotu_z)->real = 0;
  (dotu_z)->imaginary = 0;

  for (j = 0; j < 2; j++) {
    (dotu_z)->real = (dotu_z)->real + tab_real[j];
    (dotu_z)->imaginary = (dotu_z)->imaginary + tab_imaginary[j];
  }
  return;
}
