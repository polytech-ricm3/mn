#include "../include/matrix.h"
#include <stdlib.h>

// Computes a matrix-matrix product with general matrices.
// C := alpha*A*B + beta*C
// A : matrice M*K, B : matrice K*N, C : matrice M*N
// paramètres : M, N, K, alpha, A, B, beta, C
// il faut ignorer les paramètres layout, TransA, TransB, lda, ldb, ldc

// C[nrow*ncol] = A[nrow*nrowcol] * B[nrowcol*ncol]
float *sprodmm(const int nrow, const int nrowcol, const int ncol,
               const float *A, const float *B) {
  float res;
  float *C = (float *)malloc(nrow * ncol * sizeof(float));

  for (int i = 0; i < nrow; i++) {
    for (int j = 0; j < ncol; j++) {
      res = 0;
      for (int k = 0; k < nrowcol; k++) {
        res += sgetm(A, nrowcol, i, k) * sgetm(B, ncol, k, j);
      }
      sputm(C, ncol, i, j, res);
    }
  }

  return C;
}

// a * A[nrow*ncol]
float *sprodms(const int nrow, const int ncol, const float *A, const float a) {
  float *B = (float *)malloc(nrow * ncol * sizeof(float));

  for (int i = 0; i < nrow * ncol; i++)
    B[i] = a * A[i];

  return B;
}

// A = a * A[nrow*ncol]
void sprodms_self(const int nrow, const int ncol, float *A, const float a) {
  for (int i = 0; i < nrow * ncol; i++)
    A[i] = a * A[i];
}

// B = A[nrow*ncol] + B[nrow*ncol]
void saddmm_self(const int nrow, const int ncol, const float *A, float *B) {
  for (int i = 0; i < nrow * ncol; i++)
    B[i] = A[i] + B[i];
}

// C := alpha*A*B + beta*C
// A : matrice M*K, B : matrice K*N, C : matrice M*N
// m = nrom, n = rcol, k = nrowcol
void mncblas_sgemm(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                   MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                   const int K, const float alpha, const float *A,
                   const int lda, const float *B, const int ldb,
                   const float beta, float *C, const int ldc) {
  sprodms_self(M, N, C, beta);
  saddmm_self(M, N, sprodms(M, N, sprodmm(M, K, N, A, B), alpha), C);
}

/******************************************************************************/

// A[nrow*nrowcol] * B[nrowcol*ncol]
double *dprodmm(const int nrow, const int nrowcol, const int ncol,
                const double *A, const double *B) {
  double res;
  double *C = (double *)malloc(nrow * ncol * sizeof(double));

  for (int i = 0; i < nrow; i++) {
    for (int j = 0; j < ncol; j++) {
      res = 0;
      for (int k = 0; k < nrowcol; k++) {
        res += dgetm(A, nrowcol, i, k) * dgetm(B, ncol, k, j);
      }
      dputm(C, ncol, i, j, res);
    }
  }

  return C;
}

// a * A[nrow*ncol]
double *dprodms(const int nrow, const int ncol, const double *A,
                const double a) {
  double *B = (double *)malloc(nrow * ncol * sizeof(double));

  for (int i = 0; i < nrow * ncol; i++)
    B[i] = a * A[i];

  return B;
}

// A = a * A[nrow*ncol]
void dprodms_self(const int nrow, const int ncol, double *A, const double a) {
  for (int i = 0; i < nrow * ncol; i++)
    A[i] = a * A[i];
}

// B = A[nrow*ncol] + B[nrow*ncol]
void daddmm_self(const int nrow, const int ncol, const double *A, double *B) {
  for (int i = 0; i < nrow * ncol; i++)
    B[i] = A[i] + B[i];
}

void mncblas_dgemm(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                   MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                   const int K, const double alpha, const double *A,
                   const int lda, const double *B, const int ldb,
                   const double beta, double *C, const int ldc) {
  dprodms_self(M, N, C, beta);
  daddmm_self(M, N, dprodms(M, N, dprodmm(M, K, N, A, B), alpha), C);
}

/******************************************************************************/

// A[nrow*nrowcol] * B[nrowcol*ncol]
complexe_float_t *cprodmm(const int nrow, const int nrowcol, const int ncol,
                          const complexe_float_t *A,
                          const complexe_float_t *B) {
  complexe_float_t res;
  complexe_float_t *C =
      (complexe_float_t *)malloc(nrow * ncol * sizeof(complexe_float_t));

  for (int i = 0; i < nrow; i++) {
    for (int j = 0; j < ncol; j++) {
      res.real = 0;
      res.imaginary = 0;
      for (int k = 0; k < nrowcol; k++) {
        res =
            add_complexe_float(res, mult_complexe_float(cgetm(A, nrowcol, i, k),
                                                        cgetm(B, ncol, k, j)));
      }
      cputm(C, ncol, i, j, res);
    }
  }

  return C;
}

// a * A[nrow*ncol]
complexe_float_t *cprodms(const int nrow, const int ncol,
                          const complexe_float_t *A, const complexe_float_t a) {
  complexe_float_t *B =
      (complexe_float_t *)malloc(nrow * ncol * sizeof(complexe_float_t));

  for (int i = 0; i < nrow * ncol; i++)
    B[i] = mult_complexe_float(a, A[i]);

  return B;
}

// A = a * A[nrow*ncol]
void cprodms_self(const int nrow, const int ncol, complexe_float_t *A,
                  const complexe_float_t a) {
  for (int i = 0; i < nrow * ncol; i++)
    A[i] = mult_complexe_float(a, A[i]);
}

// B = A[nrow*ncol] + B[nrow*ncol]
void caddmm_self(const int nrow, const int ncol, const complexe_float_t *A,
                 complexe_float_t *B) {
  for (int i = 0; i < nrow * ncol; i++)
    B[i] = add_complexe_float(A[i], B[i]);
}

void mncblas_cgemm(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                   MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                   const int K, const void *alpha, const void *A, const int lda,
                   const void *B, const int ldb, const void *beta, void *C,
                   const int ldc) {
  cprodms_self(M, N, C, *(complexe_float_t *)(beta));
  caddmm_self(
      M, N, cprodms(M, N, cprodmm(M, K, N, A, B), *(complexe_float_t *)(alpha)),
      C);
}

/******************************************************************************/

// A[nrow*nrowcol] * B[nrowcol*ncol]
complexe_double_t *zprodmm(const int nrow, const int nrowcol, const int ncol,
                           const complexe_double_t *A,
                           const complexe_double_t *B) {
  complexe_double_t res;
  complexe_double_t *C =
      (complexe_double_t *)malloc(nrow * ncol * sizeof(complexe_double_t));

  for (int i = 0; i < nrow; i++) {
    for (int j = 0; j < ncol; j++) {
      res.real = 0;
      res.imaginary = 0;
      for (int k = 0; k < nrowcol; k++) {
        res = add_complexe_double(res,
                                  mult_complexe_double(zgetm(A, nrowcol, i, k),
                                                       zgetm(B, ncol, k, j)));
      }
      zputm(C, ncol, i, j, res);
    }
  }

  return C;
}

// a * A[nrow*ncol]
complexe_double_t *zprodms(const int nrow, const int ncol,
                           const complexe_double_t *A,
                           const complexe_double_t a) {
  complexe_double_t *B =
      (complexe_double_t *)malloc(nrow * ncol * sizeof(complexe_double_t));

  for (int i = 0; i < nrow * ncol; i++)
    B[i] = mult_complexe_double(a, A[i]);

  return B;
}

// A = a * A[nrow*ncol]
void zprodms_self(const int nrow, const int ncol, complexe_double_t *A,
                  const complexe_double_t a) {
  for (int i = 0; i < nrow * ncol; i++)
    A[i] = mult_complexe_double(a, A[i]);
}

// B = A[nrow*ncol] + B[nrow*ncol]
void zaddmm_self(const int nrow, const int ncol, const complexe_double_t *A,
                 complexe_double_t *B) {
  for (int i = 0; i < nrow * ncol; i++)
    B[i] = add_complexe_double(A[i], B[i]);
}

void mncblas_zgemm(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                   MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                   const int K, const void *alpha, const void *A, const int lda,
                   const void *B, const int ldb, const void *beta, void *C,
                   const int ldc) {
  zprodms_self(M, N, C, *(complexe_double_t *)(beta));
  zaddmm_self(
      M, N,
      zprodms(M, N, zprodmm(M, K, N, A, B), *(complexe_double_t *)(alpha)), C);
}

// /*-------------OpenMp-----------------*/

// C[nrow*ncol] = A[nrow*nrowcol] * B[nrowcol*ncol]
float *sprodmm_para(const int nrow, const int nrowcol, const int ncol,
                    const float *A, const float *B) {
  register unsigned int i, j, k;
  float res;
  float *C = (float *)malloc(nrow * ncol * sizeof(float));

#pragma omp parallel for schedule(static)
  for (i = 0; i < nrow; i++) {
#pragma omp parallel for schedule(static)
    for (j = 0; j < ncol; j++) {
      res = 0;
#pragma omp parallel for schedule(static) reduction(+ : res)
      for (k = 0; k < nrowcol; k++) {
        res += sgetm(A, nrowcol, i, k) * sgetm(B, ncol, k, j);
      }
      sputm(C, ncol, i, j, res);
    }
  }

  return C;
}

// a * A[nrow*ncol]
float *sprodms_para(const int nrow, const int ncol, const float *A,
                    const float a) {
  float *B = (float *)malloc(nrow * ncol * sizeof(float));
  register unsigned i;

#pragma omp parallel for schedule(static)
  for (i = 0; i < nrow * ncol; i++) {
    B[i] = a * A[i];
  }

  return B;
}

// A = a * A[nrow*ncol]
void sprodms_self_para(const int nrow, const int ncol, float *A,
                       const float a) {
  register unsigned i;

#pragma omp parallel for schedule(static)
  for (i = 0; i < nrow * ncol; i++) {
    A[i] = a * A[i];
  }
}

// B = A[nrow*ncol] + B[nrow*ncol]
void saddmm_self_para(const int nrow, const int ncol, const float *A,
                      float *B) {
  register unsigned i;

#pragma omp parallel for schedule(static)
  for (i = 0; i < nrow * ncol; i++) {
    B[i] = A[i] + B[i];
  }
}

// C := alpha*A*B + beta*C
// A : matrice M*K, B : matrice K*N, C : matrice M*N
// m = nrom, n = rcol, k = nrowcol
void mncblas_sgemm_para(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                        MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                        const int K, const float alpha, const float *A,
                        const int lda, const float *B, const int ldb,
                        const float beta, float *C, const int ldc) {
  sprodms_self_para(M, N, C, beta);
  saddmm_self_para(M, N, sprodms_para(M, N, sprodmm_para(M, K, N, A, B), alpha),
                   C);
}

// /******************************************************************************/

// A[nrow*nrowcol] * B[nrowcol*ncol]
double *dprodmm_para(const int nrow, const int nrowcol, const int ncol,
                     const double *A, const double *B) {
  double res;
  double *C = (double *)malloc(nrow * ncol * sizeof(double));
  register unsigned i, j, k;

#pragma omp parallel for schedule(static)
  for (i = 0; i < nrow; i++) {
#pragma omp parallel for schedule(static)
    for (j = 0; j < ncol; j++) {
      res = 0;
#pragma omp parallel for schedule(static) reduction(+ : res)
      for (k = 0; k < nrowcol; k++) {
        res += dgetm(A, nrowcol, i, k) * dgetm(B, ncol, k, j);
      }
      dputm(C, ncol, i, j, res);
    }
  }

  return C;
}

// a * A[nrow*ncol]
double *dprodms_para(const int nrow, const int ncol, const double *A,
                     const double a) {
  double *B = (double *)malloc(nrow * ncol * sizeof(double));
  register unsigned i;

#pragma omp parallel for schedule(static)
  for (i = 0; i < nrow * ncol; i++) {
    B[i] = a * A[i];
  }

  return B;
}

// A = a * A[nrow*ncol]
void dprodms_self_para(const int nrow, const int ncol, double *A,
                       const double a) {
  register unsigned i;

#pragma omp parallel for schedule(static)
  for (i = 0; i < nrow * ncol; i++) {
    A[i] = a * A[i];
  }
}

// B = A[nrow*ncol] + B[nrow*ncol]
void daddmm_self_para(const int nrow, const int ncol, const double *A,
                      double *B) {
  register unsigned i;

#pragma omp parallel for schedule(static)
  for (i = 0; i < nrow * ncol; i++) {
    B[i] = A[i] + B[i];
  }
}

void mncblas_dgemm_para(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                        MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                        const int K, const double alpha, const double *A,
                        const int lda, const double *B, const int ldb,
                        const double beta, double *C, const int ldc) {
  dprodms_self_para(M, N, C, beta);
  daddmm_self_para(M, N, dprodms_para(M, N, dprodmm_para(M, K, N, A, B), alpha),
                   C);
}

// /******************************************************************************/

// A[nrow*nrowcol] * B[nrowcol*ncol]
complexe_float_t *cprodmm_para(const int nrow, const int nrowcol,
                               const int ncol, const complexe_float_t *A,
                               const complexe_float_t *B) {
  complexe_float_t res;
  complexe_float_t *C =
      (complexe_float_t *)malloc(nrow * ncol * sizeof(complexe_float_t));
  register unsigned i, j, k;

#pragma omp parallel for schedule(static)
  for (i = 0; i < nrow; i++) {
#pragma omp parallel for schedule(static)
    for (j = 0; j < ncol; j++) {
      res.real = 0;
      res.imaginary = 0;
#pragma omp parallel for schedule(static)
      for (k = 0; k < nrowcol; k++) {
        res =
            add_complexe_float(res, mult_complexe_float(cgetm(A, nrowcol, i, k),
                                                        cgetm(B, ncol, k, j)));
      }
      cputm(C, ncol, i, j, res);
    }
  }

  return C;
}

// a * A[nrow*ncol]
complexe_float_t *cprodms_para(const int nrow, const int ncol,
                               const complexe_float_t *A,
                               const complexe_float_t a) {
  complexe_float_t *B =
      (complexe_float_t *)malloc(nrow * ncol * sizeof(complexe_float_t));
  register unsigned i;

#pragma omp parallel for schedule(static)
  for (i = 0; i < nrow * ncol; i++) {
    B[i] = mult_complexe_float(a, A[i]);
  }

  return B;
}

// A = a * A[nrow*ncol]
void cprodms_self_para(const int nrow, const int ncol, complexe_float_t *A,
                       const complexe_float_t a) {
  register unsigned i;

#pragma omp parallel for schedule(static)
  for (i = 0; i < nrow * ncol; i++) {
    A[i] = mult_complexe_float(a, A[i]);
  }
}

// B = A[nrow*ncol] + B[nrow*ncol]
void caddmm_self_para(const int nrow, const int ncol, const complexe_float_t *A,
                      complexe_float_t *B) {
  register unsigned i;

#pragma omp parallel for schedule(static)
  for (i = 0; i < nrow * ncol; i++) {
    B[i] = add_complexe_float(A[i], B[i]);
  }
}

void mncblas_cgemm_para(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                        MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                        const int K, const void *alpha, const void *A,
                        const int lda, const void *B, const int ldb,
                        const void *beta, void *C, const int ldc) {
  cprodms_self_para(M, N, C, *(complexe_float_t *)(beta));
  caddmm_self_para(M, N,
                   cprodms_para(M, N, cprodmm_para(M, K, N, A, B),
                                *(complexe_float_t *)(alpha)),
                   C);
}

// /******************************************************************************/

// A[nrow*nrowcol] * B[nrowcol*ncol]
complexe_double_t *zprodmm_para(const int nrow, const int nrowcol,
                                const int ncol, const complexe_double_t *A,
                                const complexe_double_t *B) {
  complexe_double_t res;
  complexe_double_t *C =
      (complexe_double_t *)malloc(nrow * ncol * sizeof(complexe_double_t));
  register unsigned i, j, k;

#pragma omp parallel for schedule(static)
  for (i = 0; i < nrow; i++) {
#pragma omp parallel for schedule(static)
    for (j = 0; j < ncol; j++) {
      res.real = 0;
      res.imaginary = 0;
#pragma omp parallel for schedule(static)
      for (k = 0; k < nrowcol; k++) {
        res = add_complexe_double(res,
                                  mult_complexe_double(zgetm(A, nrowcol, i, k),
                                                       zgetm(B, ncol, k, j)));
      }
      zputm(C, ncol, i, j, res);
    }
  }

  return C;
}

// a * A[nrow*ncol]
complexe_double_t *zprodms_para(const int nrow, const int ncol,
                                const complexe_double_t *A,
                                const complexe_double_t a) {
  complexe_double_t *B =
      (complexe_double_t *)malloc(nrow * ncol * sizeof(complexe_double_t));
  register unsigned i;

#pragma omp parallel for schedule(static)
  for (i = 0; i < nrow * ncol; i++) {
    B[i] = mult_complexe_double(a, A[i]);
  }

  return B;
}

// A = a * A[nrow*ncol]
void zprodms_self_para(const int nrow, const int ncol, complexe_double_t *A,
                       const complexe_double_t a) {
  register unsigned i;

#pragma omp parallel for schedule(static)
  for (i = 0; i < nrow * ncol; i++) {
    A[i] = mult_complexe_double(a, A[i]);
  }
}

// B = A[nrow*ncol] + B[nrow*ncol]
void zaddmm_self_para(const int nrow, const int ncol,
                      const complexe_double_t *A, complexe_double_t *B) {
  register unsigned i;

#pragma omp parallel for schedule(static)
  for (i = 0; i < nrow * ncol; i++) {
    B[i] = add_complexe_double(A[i], B[i]);
  }
}

void mncblas_zgemm_para(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                        MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                        const int K, const void *alpha, const void *A,
                        const int lda, const void *B, const int ldb,
                        const void *beta, void *C, const int ldc) {
  zprodms_self_para(M, N, C, *(complexe_double_t *)(beta));
  zaddmm_self_para(M, N,
                   zprodms_para(M, N, zprodmm_para(M, K, N, A, B),
                                *(complexe_double_t *)(alpha)),
                   C);
}

/*----------------Vectorisation------------------*/

// __m128 alpha_m = _mm_set1_ps(alpha); // vectorise variable
// y = _mm_add_ps(y, tmp_mult);         // addition
// y = _mm_mul_ps(y, beta_m);           // multiplication
// y = _mm_load_ps(Y + i);              // accès lecture tableau
// _mm_store_ps((Y + i), y);            // accès écriture tableau

// C[nrow*ncol] = A[nrow*nrowcol] * B[nrowcol*ncol]
float *sprodmm_vect(const int nrow, const int nrowcol, const int ncol,
                    const float *A, const float *B) {
  float *C = (float *)malloc(nrow * ncol * sizeof(float));
  register unsigned i, j, k;
  __m128 res, mult1, mult2;

  // smatrix_init_vect(C, nrow, ncol, _mm_setzero_ps());
  smatrix_init(C, nrow, ncol, 0);

  for (i = 0; i < nrow; i++) {
    for (k = 0; k < nrowcol; k++) {
      mult1 = _mm_set1_ps(sgetm(A, nrowcol, i, k));
      for (j = 0; j < ncol; j += 4) {
        mult2 = sgetm_vect(B, ncol, k, j);
        res = _mm_mul_ps(mult1, mult2);
        res = _mm_add_ps(res, sgetm_vect(C, ncol, i, j));
        sputm_vect(C, ncol, i, j, res);
      }
    }
  }

  return C;
}

// B = a * A[nrow*ncol]
float *sprodms_vect(const int nrow, const int ncol, const float *A,
                    const float a) {
  float *B = (float *)malloc(nrow * ncol * sizeof(float));
  register unsigned i;
  __m128 res, a_m = _mm_set1_ps(a);

  for (i = 0; i < nrow * ncol; i += 4) {
    res = _mm_mul_ps(a_m, _mm_load_ps(A + i));
    _mm_store_ps((B + i), res);
  }

  return B;
}

// A = a * A[nrow*ncol]
void sprodms_self_vect(const int nrow, const int ncol, float *A,
                       const float a) {
  register unsigned i;
  __m128 res, a_m = _mm_set1_ps(a);

  for (i = 0; i < nrow * ncol; i += 4) {
    res = _mm_mul_ps(a_m, _mm_load_ps(A + i));
    _mm_store_ps((A + i), res);
  }
}

// B = A[nrow*ncol] + B[nrow*ncol]
void saddmm_self_vect(const int nrow, const int ncol, const float *A,
                      float *B) {
  register unsigned i;
  __m128 res;

  for (i = 0; i < nrow * ncol; i += 4) {
    res = _mm_add_ps(_mm_load_ps(A + i), _mm_load_ps(B + i));
    _mm_store_ps((B + i), res);
  }
}

// C := alpha*A*B + beta*C
// A : matrice M*K, B : matrice K*N, C : matrice M*N
// m = nrom, n = rcol, k = nrowcol
void mncblas_sgemm_vect(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                        MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                        const int K, const float alpha, const float *A,
                        const int lda, const float *B, const int ldb,
                        const float beta, float *C, const int ldc) {
  sprodms_self_vect(M, N, C, beta);
  // saddmm_self_vect(M, N, sprodms_vect(M, N, sprodmm(M, K, N, A, B), alpha),
  // C);
  saddmm_self_vect(M, N, sprodms_vect(M, N, sprodmm_vect(M, K, N, A, B), alpha),
                   C);
}

/******************************************************************************/

// __m128d alpha_m = _mm_set1_pd(alpha); // vectorise variable
// y = _mm_add_pd(y, tmp_mult);          // addition
// y = _mm_mul_pd(y, beta_m);            // multiplication
// y = _mm_load_pd(Y + i);               // accès lecture tableau
// _mm_store_pd((Y + i), y);             // accès écriture tableau

// A[nrow*nrowcol] * B[nrowcol*ncol]
double *dprodmm_vect(const int nrow, const int nrowcol, const int ncol,
                     const double *A, const double *B) {
  double *C = (double *)malloc(nrow * ncol * sizeof(double));
  register unsigned i, j, k;
  __m128d res, mult1, mult2;

  // dmatrix_init_vect(C, nrow, ncol, _mm_setzero_ps());
  dmatrix_init(C, nrow, ncol, 0);

  for (i = 0; i < nrow; i++) {
    for (k = 0; k < nrowcol; k++) {
      mult1 = _mm_set1_pd(dgetm(A, nrowcol, i, k));
      for (j = 0; j < ncol; j += 2) {
        mult2 = dgetm_vect(B, ncol, k, j);
        res = _mm_mul_pd(mult1, mult2);
        res = _mm_add_pd(res, dgetm_vect(C, ncol, i, j));
        dputm_vect(C, ncol, i, j, res);
      }
    }
  }

  return C;
}

// a * A[nrow*ncol]
double *dprodms_vect(const int nrow, const int ncol, const double *A,
                     const double a) {
  double *B = (double *)malloc(nrow * ncol * sizeof(double));
  register unsigned i;
  __m128d res, a_m = _mm_set1_pd(a);

  for (i = 0; i < nrow * ncol; i += 2) {
    res = _mm_mul_pd(a_m, _mm_load_pd(A + i));
    _mm_store_pd((B + i), res);
  }

  return B;
}

// A = a * A[nrow*ncol]
void dprodms_self_vect(const int nrow, const int ncol, double *A,
                       const double a) {
  register unsigned i;
  __m128d res, a_m = _mm_set1_pd(a);

  for (i = 0; i < nrow * ncol; i += 2) {
    res = _mm_mul_pd(a_m, _mm_load_pd(A + i));
    _mm_store_pd((A + i), res);
  }
}

// B = A[nrow*ncol] + B[nrow*ncol]
void daddmm_self_vect(const int nrow, const int ncol, const double *A,
                      double *B) {
  register unsigned i;
  __m128d res;

  for (i = 0; i < nrow * ncol; i += 2) {
    res = _mm_add_pd(_mm_load_pd(A + i), _mm_load_pd(B + i));
    _mm_store_pd((B + i), res);
  }
}

void mncblas_dgemm_vect(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                        MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                        const int K, const double alpha, const double *A,
                        const int lda, const double *B, const int ldb,
                        const double beta, double *C, const int ldc) {
  dprodms_self_vect(M, N, C, beta);
  // daddmm_self_vect(M, N, dprodms_vect(M, N, dprodmm(M, K, N, A, B), alpha), C);
  daddmm_self_vect(M, N, dprodms_vect(M, N, dprodmm_vect(M, K, N, A, B), alpha), C);
}

/******************************************************************************/

// // A[nrow*nrowcol] * B[nrowcol*ncol]
// complexe_float_t *cprodmm_vect(const int nrow, const int nrowcol,
//                                const int ncol, const complexe_float_t *A,
//                                const complexe_float_t *B) {
//   complexe_float_t res;
//   complexe_float_t *C =
//       (complexe_float_t *)malloc(nrow * ncol * sizeof(complexe_float_t));
//   register unsigned i, j, k;

//   for (i = 0; i < nrow; i++) {
//     for (j = 0; j < ncol; j++) {
//       res.real = 0;
//       res.imaginary = 0;
//       for (k = 0; k < nrowcol; k++) {
//         res =
//             add_complexe_float(res, mult_complexe_float(cgetm(A, nrowcol, i, k),
//                                                         cgetm(B, ncol, k, j)));
//       }
//       cputm(C, ncol, i, j, res);
//     }
//   }

//   return C;
// }

// // a * A[nrow*ncol]
// complexe_float_t *cprodms_vect(const int nrow, const int ncol,
//                                const complexe_float_t *A,
//                                const complexe_float_t a) {
//   complexe_float_t *B =
//       (complexe_float_t *)malloc(nrow * ncol * sizeof(complexe_float_t));
//   register unsigned i;

//   for (i = 0; i < nrow * ncol; i++)
//     B[i] = mult_complexe_float(a, A[i]);

//   return B;
// }

// // A = a * A[nrow*ncol]
// void cprodms_self_vect(const int nrow, const int ncol, complexe_float_t *A,
//                        const complexe_float_t a) {
//   register unsigned i;

//   for (i = 0; i < nrow * ncol; i++)
//     A[i] = mult_complexe_float(a, A[i]);
// }

// // B = A[nrow*ncol] + B[nrow*ncol]
// void caddmm_self_vect(const int nrow, const int ncol, const complexe_float_t *A,
//                       complexe_float_t *B) {
//   register unsigned i;

//   for (i = 0; i < nrow * ncol; i++)
//     B[i] = add_complexe_float(A[i], B[i]);
// }

// void mncblas_cgemm_vect(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
//                         MNCBLAS_TRANSPOSE TransB, const int M, const int N,
//                         const int K, const void *alpha, const void *A,
//                         const int lda, const void *B, const int ldb,
//                         const void *beta, void *C, const int ldc) {
//   cprodms_self_vect(M, N, C, *(complexe_float_t *)(beta));
//   caddmm_self_vect(M, N,
//                    cprodms_vect(M, N, cprodmm_vect(M, K, N, A, B),
//                                 *(complexe_float_t *)(alpha)),
//                    C);
// }

/******************************************************************************/

// // A[nrow*nrowcol] * B[nrowcol*ncol]
// complexe_double_t *zprodmm_vect(const int nrow, const int nrowcol,
//                                 const int ncol, const complexe_double_t *A,
//                                 const complexe_double_t *B) {
//   complexe_double_t res;
//   complexe_double_t *C =
//       (complexe_double_t *)malloc(nrow * ncol * sizeof(complexe_double_t));
//   register unsigned i, j, k;

//   for (i = 0; i < nrow; i++) {
//     for (j = 0; j < ncol; j++) {
//       res.real = 0;
//       res.imaginary = 0;
//       for (k = 0; k < nrowcol; k++) {
//         res = add_complexe_double(res,
//                                   mult_complexe_double(zgetm(A, nrowcol, i, k),
//                                                        zgetm(B, ncol, k, j)));
//       }
//       zputm(C, ncol, i, j, res);
//     }
//   }

//   return C;
// }

// // a * A[nrow*ncol]
// complexe_double_t *zprodms_vect(const int nrow, const int ncol,
//                                 const complexe_double_t *A,
//                                 const complexe_double_t a) {
//   complexe_double_t *B =
//       (complexe_double_t *)malloc(nrow * ncol * sizeof(complexe_double_t));
//   register unsigned i;

//   for (i = 0; i < nrow * ncol; i++)
//     B[i] = mult_complexe_double(a, A[i]);

//   return B;
// }

// // A = a * A[nrow*ncol]
// void zprodms_self_vect(const int nrow, const int ncol, complexe_double_t *A,
//                        const complexe_double_t a) {
//   register unsigned i;

//   for (i = 0; i < nrow * ncol; i++)
//     A[i] = mult_complexe_double(a, A[i]);
// }

// // B = A[nrow*ncol] + B[nrow*ncol]
// void zaddmm_self_vect(const int nrow, const int ncol,
//                       const complexe_double_t *A, complexe_double_t *B) {
//   register unsigned i;

//   for (i = 0; i < nrow * ncol; i++)
//     B[i] = add_complexe_double(A[i], B[i]);
// }

// void mncblas_zgemm_vect(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
//                         MNCBLAS_TRANSPOSE TransB, const int M, const int N,
//                         const int K, const void *alpha, const void *A,
//                         const int lda, const void *B, const int ldb,
//                         const void *beta, void *C, const int ldc) {
//   zprodms_self_vect(M, N, C, *(complexe_double_t *)(beta));
//   zaddmm_self_vect(M, N,
//                    zprodms_vect(M, N, zprodmm_vect(M, K, N, A, B),
//                                 *(complexe_double_t *)(alpha)),
//                    C);
// }

/*----------------Vectorisation + Parallélisation------------------*/

// C[nrow*ncol] = A[nrow*nrowcol] * B[nrowcol*ncol]
float *sprodmm_both(const int nrow, const int nrowcol, const int ncol,
                    const float *A, const float *B) {
  float *C = (float *)malloc(nrow * ncol * sizeof(float));
  register unsigned i, j, k;
  __m128 res, mult1, mult2;

  // smatrix_init_vect(C, nrow, ncol, _mm_setzero_ps());
  smatrix_init(C, nrow, ncol, 0);
#pragma omp parallel for schedule(static)
  for (i = 0; i < nrow; i++) {
#pragma omp parallel for schedule(static)
    for (k = 0; k < nrowcol; k++) {
      mult1 = _mm_set1_ps(sgetm(A, nrowcol, i, k));
#pragma omp parallel for schedule(static)
      for (j = 0; j < ncol; j += 4) {
        mult2 = sgetm_vect(B, ncol, k, j);
        res = _mm_mul_ps(mult1, mult2);
        res = _mm_add_ps(res, sgetm_vect(C, ncol, i, j));
        sputm_vect(C, ncol, i, j, res);
      }
    }
  }

  return C;
}

// B = a * A[nrow*ncol]
float *sprodms_both(const int nrow, const int ncol, const float *A,
                    const float a) {
  float *B = (float *)malloc(nrow * ncol * sizeof(float));
  register unsigned i;
  __m128 res, a_m = _mm_set1_ps(a);

#pragma omp parallel for schedule(static)
  for (i = 0; i < nrow * ncol; i += 4) {
    res = _mm_mul_ps(a_m, _mm_load_ps(A + i));
    _mm_store_ps((B + i), res);
  }

  return B;
}

// A = a * A[nrow*ncol]
void sprodms_self_both(const int nrow, const int ncol, float *A,
                       const float a) {
  register unsigned i;
  __m128 res, a_m = _mm_set1_ps(a);

#pragma omp parallel for schedule(static)
  for (i = 0; i < nrow * ncol; i += 4) {
    res = _mm_mul_ps(a_m, _mm_load_ps(A + i));
    _mm_store_ps((A + i), res);
  }
}

// B = A[nrow*ncol] + B[nrow*ncol]
void saddmm_self_both(const int nrow, const int ncol, const float *A,
                      float *B) {
  register unsigned i;
  __m128 res;

#pragma omp parallel for schedule(static)
  for (i = 0; i < nrow * ncol; i += 4) {
    res = _mm_add_ps(_mm_load_ps(A + i), _mm_load_ps(B + i));
    _mm_store_ps((B + i), res);
  }
}

// C := alpha*A*B + beta*C
// A : matrice M*K, B : matrice K*N, C : matrice M*N
// m = nrom, n = rcol, k = nrowcol
void mncblas_sgemm_both(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                        MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                        const int K, const float alpha, const float *A,
                        const int lda, const float *B, const int ldb,
                        const float beta, float *C, const int ldc) {
  sprodms_self_both(M, N, C, beta);
  // saddmm_self_both(M, N, sprodms_both(M, N, sprodmm(M, K, N, A, B), alpha),
  // C);
  saddmm_self_both(M, N, sprodms_both(M, N, sprodmm_both(M, K, N, A, B), alpha),
                   C);
}

/******************************************************************************/

// __m128d alpha_m = _mm_set1_pd(alpha); // vectorise variable
// y = _mm_add_pd(y, tmp_mult);          // addition
// y = _mm_mul_pd(y, beta_m);            // multiplication
// y = _mm_load_pd(Y + i);               // accès lecture tableau
// _mm_store_pd((Y + i), y);             // accès écriture tableau

// A[nrow*nrowcol] * B[nrowcol*ncol]
double *dprodmm_both(const int nrow, const int nrowcol, const int ncol,
                     const double *A, const double *B) {
  double *C = (double *)malloc(nrow * ncol * sizeof(double));
  register unsigned i, j, k;
  __m128d res, mult1, mult2;

  // dmatrix_init_vect(C, nrow, ncol, _mm_setzero_ps());
  dmatrix_init(C, nrow, ncol, 0);

#pragma omp parallel for schedule(static)
  for (i = 0; i < nrow; i++) {
#pragma omp parallel for schedule(static)
    for (k = 0; k < nrowcol; k++) {
      mult1 = dgetm_vect(A, nrowcol, i, k);
#pragma omp parallel for schedule(static)
      for (j = 0; j < ncol; j += 2) {
        mult2 = dgetm_vect(B, ncol, k, j);
        res = _mm_mul_pd(mult1, mult2);
        res = _mm_add_pd(res, dgetm_vect(C, ncol, i, j));
        dputm_vect(C, ncol, i, j, res);
      }
    }
  }

  return C;
}

// a * A[nrow*ncol]
double *dprodms_both(const int nrow, const int ncol, const double *A,
                     const double a) {
  double *B = (double *)malloc(nrow * ncol * sizeof(double));
  register unsigned i;
  __m128d res, a_m = _mm_set1_pd(a);

#pragma omp parallel for schedule(static)
  for (i = 0; i < nrow * ncol; i += 2) {
    res = _mm_mul_pd(a_m, _mm_load_pd(A + i));
    _mm_store_pd((B + i), res);
  }

  return B;
}

// A = a * A[nrow*ncol]
void dprodms_self_both(const int nrow, const int ncol, double *A,
                       const double a) {
  register unsigned i;
  __m128d res, a_m = _mm_set1_pd(a);

#pragma omp parallel for schedule(static)
  for (i = 0; i < nrow * ncol; i += 2) {
    res = _mm_mul_pd(a_m, _mm_load_pd(A + i));
    _mm_store_pd((A + i), res);
  }
}

// B = A[nrow*ncol] + B[nrow*ncol]
void daddmm_self_both(const int nrow, const int ncol, const double *A,
                      double *B) {
  register unsigned i;
  __m128d res;

#pragma omp parallel for schedule(static)
  for (i = 0; i < nrow * ncol; i += 2) {
    res = _mm_add_pd(_mm_load_pd(A + i), _mm_load_pd(B + i));
    _mm_store_pd((B + i), res);
  }
}

void mncblas_dgemm_both(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                        MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                        const int K, const double alpha, const double *A,
                        const int lda, const double *B, const int ldb,
                        const double beta, double *C, const int ldc) {
  dprodms_self_both(M, N, C, beta);
  // daddmm_self_both(M, N, dprodms_both(M, N, dprodmm(M, K, N, A, B), alpha), C);
  daddmm_self_both(M, N, dprodms_both(M, N, dprodmm_both(M, K, N, A, B), alpha), C);
}

/******************************************************************************/

// Les complexes n'ont pas été faits