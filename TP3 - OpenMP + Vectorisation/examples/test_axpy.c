#include <stdio.h>
#include <x86intrin.h>
#include <assert.h>

#include "test.h"
#include "../include/mnblas.h"
#include "utils/flop.h"
#include "utils/vector.h"

#define NB_FOIS 5

int test_axpy()
{
    printf("\n");
    printf("mnblas_saxpy\n");
    printf("--------------------------------------------\n");
    unsigned long long start, end;
    vfloat v1, v2;
    int i = 0;
    FILE *testsaxpy, *testdaxpy, *testcaxpy, *testzaxpy;
    float res;

    testsaxpy = fopen("test/saxpy.csv","w");
    fprintf(testsaxpy, "fonction;GFLOP/s\n");
    for(; i < NB_FOIS; i++)
    {

        /*  --------------------- saxpy -----------------------*/
        vector_init(v1,(float)i + 1);
        vector_init(v2,(float)i);

        start = _rdtsc();
            mnblas_saxpy(VECSIZE,(float) i,v1,1,v2,1);
        end   = _rdtsc();

        //Calculs des Flops et écriture dans le fichier csv
        res = calcul_flop("mnblas_saxpy",VECSIZE, end - start);
        fprintf(testsaxpy,"saxpy;%5.3f\n",res);

        /* ----------------------- saxpy_p -------------------*/
        vector_init(v1,(float)i + 1);
        vector_init(v2,(float)i);
        start = _rdtsc();
            mnblas_saxpy_p(VECSIZE,(float) i,v1,1,v2,1);
        end   = _rdtsc();
        
        //Calculs des Flops et écriture dans le fichier csv
        res = calcul_flop("mnblas_saxpy_p",VECSIZE, end - start);
        fprintf(testsaxpy,"saxpy_p;%5.3f\n",res);

        /* ----------------------- saxpy_v -------------------*/
        vector_init(v1,(float)i + 1);
        vector_init(v2,(float)i);
        start = _rdtsc();
            mnblas_saxpy_v(VECSIZE,(float) i,v1,1,v2,1);
        end   = _rdtsc();

        //Calculs des Flops et écriture dans le fichier csv
        res = calcul_flop("mnblas_saxpy_v",VECSIZE, end - start);
        fprintf(testsaxpy,"saxpy_v;%5.3f\n",res);

        /* ----------------------- saxpy_vp -------------------*/
        vector_init(v1,(float)i + 1);
        vector_init(v2,(float)i);
        start = _rdtsc();
            mnblas_saxpy_vp(VECSIZE,(float) i,v1,1,v2,1);
        end   = _rdtsc();
        
        //Calculs des Flops et écriture dans le fichier csv
        res = calcul_flop("mnblas_saxpy_vp",VECSIZE, end - start);
        fprintf(testsaxpy,"saxpy_vp;%5.3f\n",res);
        printf("--------------------------------------------\n");
    }
    fclose(testsaxpy);
    printf("\n");


    printf("mnblas_daxpy\n");
    printf("--------------------------------------------\n");
    i = 0;
    vdouble vd1, vd2;
    testdaxpy = fopen("test/daxpy.csv", "w");
    fprintf(testsaxpy, "fonction;GFLOP/s\n");
    for(; i < NB_FOIS; i++)
    {
        /* ---------------- daxpy ---------------------- */
        vector_init_double(vd1,(double)i + 1);
        vector_init_double(vd2,(double)i);
        start = _rdtsc();
            mnblas_daxpy(VECSIZE,(double) i,vd1,1,vd2,1);
        end   = _rdtsc();

        //Calculs des Flops et écriture dans le fichier csv
        res = calcul_flop("mnblas_daxpy",VECSIZE, end - start);
        fprintf(testdaxpy,"daxpy;%5.3f\n",res);

        /* -------------- daxpy_p --------------------- */
        vector_init_double(vd1,(double)i + 1);
        vector_init_double(vd2,(double)i);
        start = _rdtsc();
            mnblas_daxpy_p(VECSIZE,(double) i,vd1,1,vd2,1);
        end   = _rdtsc();
        
        //Calculs des Flops et écriture dans le fichier csv
        res = calcul_flop("mnblas_daxpy_p",VECSIZE, end - start);
        fprintf(testdaxpy,"daxpy_p;%5.3f\n",res);

        /* ------------- daxpy_v -------------------- */
        vector_init_double(vd1,(double)i + 1);
        vector_init_double(vd2,(double)i);
        start = _rdtsc();
            mnblas_daxpy_v(VECSIZE,(double) i,vd1,1,vd2,1);
        end   = _rdtsc();

        //Calculs des Flops et écriture dans le fichier csv
        res = calcul_flop("mnblas_daxpy_v",VECSIZE, end - start);
        fprintf(testdaxpy,"daxpy_v;%5.3f\n",res);

        /* ------------ daxpy_vp ------------------- */
        vector_init_double(vd1,(double)i + 1);
        vector_init_double(vd2,(double)i);
        start = _rdtsc();
            mnblas_daxpy_vp(VECSIZE,(double) i,vd1,1,vd2,1);
        end   = _rdtsc();
        
        //Calculs des Flops et écriture dans le fichier csv
        res = calcul_flop("mnblas_daxpy_vp",VECSIZE, end - start);
        fprintf(testdaxpy,"daxpy_vp;%5.3f\n",res);
        
        printf("--------------------------------------------\n");
    }
    fclose(testdaxpy);
    printf("\n");
    printf("mnblas_caxpy\n");
    printf("--------------------------------------------\n");
    i = 0;
    vcomplex_f vcf1, vcf2;

    testcaxpy = fopen("test/caxpy.csv", "w");
    fprintf(testsaxpy, "fonction;GFLOP/s\n");
    for(; i < NB_FOIS; i++)
    {
        complexe_float_t *alpha = malloc(sizeof(complexe_float_t));
        alpha->real      = i + 1;
        alpha->imaginary = i;

        /* ------------------- caxpy ------------------------- */
        vector_init_complex_f(vcf1,(float)i + 1,(float)i + 1);
        vector_init_complex_f(vcf2,(float)i,(float)i);
        start = _rdtsc();
            mnblas_caxpy(VECSIZE,alpha,vcf1,1,vcf2,1);
        end   = _rdtsc();

        //Calculs des Flops et écriture dans le fichier csv
        res = calcul_flop("mnblas_caxpy", 4 * VECSIZE, end - start);
        fprintf(testcaxpy,"caxpy;%5.3f\n",res);

        /* ----------------- caxpy_p ------------------------ */
        vector_init_complex_f(vcf1,(float)i + 1,(float)i + 1);
        vector_init_complex_f(vcf2,(float)i,(float)i);
         start = _rdtsc();
            mnblas_caxpy_p(VECSIZE,alpha,vcf1,1,vcf2,1);
        end   = _rdtsc();
        
        //Calculs des Flops et écriture dans le fichier csv
        res = calcul_flop("mnblas_caxpy_p", 4 * VECSIZE, end - start);
        fprintf(testcaxpy,"caxpy_p;%5.3f\n",res);

        /* ---------------- caxpy_v ----------------------- */
        vector_init_complex_f(vcf1,(float)i + 1,(float)i + 1);
        vector_init_complex_f(vcf2,(float)i,(float)i);
        start = _rdtsc();
            mnblas_caxpy_v(VECSIZE,alpha,vcf1,1,vcf2,1);
        end   = _rdtsc();
        
        //Calculs des Flops et écriture dans le fichier csv
        res = calcul_flop("mnblas_caxpy_v", 4 * VECSIZE, end - start);
        fprintf(testcaxpy,"caxpy_v;%5.3f\n",res);

        /* ---------------- caxpy_vp --------------------- */
        vector_init_complex_f(vcf1,(float)i + 1,(float)i + 1);
        vector_init_complex_f(vcf2,(float)i,(float)i);
        start = _rdtsc();
            mnblas_caxpy_vp(VECSIZE,alpha,vcf1,1,vcf2,1);
        end   = _rdtsc();
        
        //Calculs des Flops et écriture dans le fichier csv
        res = calcul_flop("mnblas_caxpy_vp", 4 * VECSIZE, end - start);
        fprintf(testcaxpy,"caxpy_vp;%5.3f\n",res);
        
        free(alpha);
        printf("--------------------------------------------\n");
    }
    fclose(testcaxpy);
    printf("\n");
    printf("mnblas_zaxpy\n");
    printf("--------------------------------------------\n");
    i = 0;
    vcomplex_d vcd1, vcd2;
    testzaxpy = fopen("test/zaxpy.csv", "w");
    fprintf(testsaxpy, "fonction;GFLOP/s\n");
    for(; i < NB_FOIS; i++)
    {
        complexe_double_t *alpha = malloc(sizeof(complexe_double_t));
        alpha->real      = i + 1;
        alpha->imaginary = i;

        /* ----------------------- zaxpy ----------------------- */
        vector_init_complex_d(vcd1,(double)i + 1,(double)i + 1);
        vector_init_complex_d(vcd2,(double)i,(double)i);
        start = _rdtsc();
            mnblas_zaxpy(VECSIZE,alpha,vcd1,1,vcd2,1);
        end   = _rdtsc();

        //Calculs des Flops et écriture dans le fichier csv
        res = calcul_flop("mnblas_zaxpy", 4 * VECSIZE, end - start);
        fprintf(testzaxpy,"zaxpy;%5.3f\n",res);

        

        /* ---------------------- zaxpy_p ---------------------- */
        calcul_flop("mnblas_zaxpy", 4 * VECSIZE, end - start);
        vector_init_complex_d(vcd1,(double)i + 1,(double)i + 1);
        vector_init_complex_d(vcd2,(double)i,(double)i);
         start = _rdtsc();
            mnblas_zaxpy_p(VECSIZE,alpha,vcd1,1,vcd2,1);
        end   = _rdtsc();
        
        //Calculs des Flops et écriture dans le fichier csv
        res = calcul_flop("mnblas_zaxpy_p", 4 * VECSIZE, end - start);
        fprintf(testzaxpy,"zaxpy_p;%5.3f\n",res);

        /* ------------------- zaxpy_v ----------------------- */
        vector_init_complex_d(vcd1,(double)i + 1,(double)i + 1);
        vector_init_complex_d(vcd2,(double)i,(double)i);
         start = _rdtsc();
            mnblas_zaxpy_v(VECSIZE,alpha,vcd1,1,vcd2,1);
        end   = _rdtsc();
        
        //Calculs des Flops et écriture dans le fichier csv
        res = calcul_flop("mnblas_zaxpy_v", 4 * VECSIZE, end - start);
        fprintf(testzaxpy,"zaxpy_v;%5.3f\n",res);

        /* ---------------- zaxpy_vp ----------------------- */
        vector_init_complex_d(vcd1,(double)i + 1,(double)i + 1);
        vector_init_complex_d(vcd2,(double)i,(double)i);
         start = _rdtsc();
            mnblas_zaxpy_vp(VECSIZE,alpha,vcd1,1,vcd2,1);
        end   = _rdtsc();
        
        //Calculs des Flops et écriture dans le fichier csv
        res = calcul_flop("mnblas_zaxpy_vp", 4 * VECSIZE, end - start);
        fprintf(testzaxpy,"zaxpy_vp;%5.3f\n",res);

        free(alpha);
        printf("--------------------------------------------\n");
    }
    fclose(testzaxpy);
    return 0;
}