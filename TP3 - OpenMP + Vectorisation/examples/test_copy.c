#include <stdio.h>
#include <x86intrin.h>
#include <assert.h>

#include "test.h"
#include "../include/mnblas.h"
#include "utils/flop.h"
#include "utils/vector.h"

#define NB_FOIS    5

int test_copy()
{
    unsigned long long start, end;
    vfloat v1, v2;
    int i = 0;

    FILE *testscopy, *testdcopy, *testccopy, *testzcopy;
    float res;

    testscopy = fopen("test/scopy.csv", "w");
    fprintf(testscopy, "fonction;O/s\n");
    for(; i < NB_FOIS; i++)
    {
        /* ------------------------------ scopy ------------------------------ */
        vector_init(v1,(float)i + 1);
        vector_init(v2,(float)i);
        start = _rdtsc();
            mncblas_scopy(VECSIZE,v1,1,v2,1);
        end   = _rdtsc();

        //Calculs des accès mémoire et écriture dans le fichier csv
        res = calcul_mem("mncblas_scopy",(VECSIZE / 2) * sizeof(float), end - start);
        fprintf(testscopy,"scopy;%5.3f\n",res);

        /* ----------------------------- scopy_p ---------------------------- */
        vector_init(v1,(float)i + 1);
        vector_init(v2,(float)i);
        start = _rdtsc();
            mncblas_scopy_p(VECSIZE,v1,1,v2,1);
        end   = _rdtsc();
        
        //Calculs des accès mémoire et écriture dans le fichier csv
        res = calcul_mem("mncblas_scopy_p",(VECSIZE / 2) * sizeof(float), end - start);
        fprintf(testscopy,"scopy_p;%5.3f\n",res);

        /* --------------------------- scopy_v ----------------------------- */
        vector_init(v1,(float)i + 1);
        vector_init(v2,(float)i);
        start = _rdtsc();
            mncblas_scopy_v(VECSIZE,v1,1,v2,1);
        end   = _rdtsc();

        //Calculs des accès mémoire et écriture dans le fichier csv
        res = calcul_mem("mncblas_scopy_v",(VECSIZE / 2) * sizeof(float), end - start);
        fprintf(testscopy,"scopy_v;%5.3f\n",res);

        /* ------------------------- scopy_vp ----------------------------- */
        vector_init(v1,(float)i + 1);
        vector_init(v2,(float)i);
        start = _rdtsc();
            mncblas_scopy_vp(VECSIZE,v1,1,v2,1);
        end   = _rdtsc();
        
        //Calculs des accès mémoire et écriture dans le fichier csv
        res = calcul_mem("mncblas_scopy_vp",(VECSIZE / 2) * sizeof(float), end - start);
        fprintf(testscopy,"scopy_vp;%5.3f\n",res);

        printf("\n");
    }
    fclose(testscopy);
    printf("--------------------------------------------\n\n");
    
    i = 0;
    vdouble vd1, vd2;
    testdcopy = fopen("test/dcopy.csv", "w");
    fprintf(testdcopy, "fonction;O/s\n");
    for(; i < NB_FOIS; i++)
    {
        /* ------------------------- dcopy --------------------------------- */
        vector_init_double(vd1,(double)i + 1);
        vector_init_double(vd2,(double)i);
        start = _rdtsc();
            mncblas_dcopy(VECSIZE,vd1,1,vd2,1);
        end   = _rdtsc();

        //Calculs des accès mémoire et écriture dans le fichier csv
        res = calcul_mem("mncblas_dcopy",(VECSIZE / 2) * sizeof(double), end - start);
        fprintf(testdcopy,"dcopy;%5.3f\n",res);

        /* ----------------------- dcopy_p ---------------------------------- */
        vector_init_double(vd1,(double)i + 1);
        vector_init_double(vd2,(double)i);
        start = _rdtsc();
            mncblas_dcopy_p(VECSIZE,vd1,1,vd2,1);
        end   = _rdtsc();

        //Calculs des accès mémoire et écriture dans le fichier csv
        res = calcul_mem("mncblas_dcopy_p",(VECSIZE / 2) * sizeof(double), end - start);
        fprintf(testdcopy,"dcopy_p;%5.3f\n",res);

        /* ---------------------- dcopy_v ----------------------------------- */
        vector_init_double(vd1,(double)i + 1);
        vector_init_double(vd2,(double)i);
        start = _rdtsc();
            mncblas_dcopy_v(VECSIZE,vd1,1,vd2,1);
        end   = _rdtsc();

        //Calculs des accès mémoire et écriture dans le fichier csv
        res = calcul_mem("mncblas_dcopy_v",(VECSIZE / 2) * sizeof(double), end - start);
        fprintf(testdcopy,"dcopy_v;%5.3f\n",res);

        /* ---------------------- dcopy_vp ---------------------------------- */
        vector_init_double(vd1,(double)i + 1);
        vector_init_double(vd2,(double)i);
        start = _rdtsc();
            mncblas_dcopy_vp(VECSIZE,vd1,1,vd2,1);
        end   = _rdtsc();
        
        //Calculs des accès mémoire et écriture dans le fichier csv
        res = calcul_mem("mncblas_dcopy_vp",(VECSIZE / 2) * sizeof(double), end - start);
        fprintf(testdcopy,"dcopy_vp;%5.3f\n",res);
        printf("\n");
    }
    fclose(testdcopy);
    printf("--------------------------------------------\n\n");

    i = 0;
    vcomplex_f vcf1, vcf2;
    testccopy = fopen("test/ccopy.csv", "w");
    fprintf(testccopy, "fonction;O/s\n");
    for(; i < NB_FOIS; i++)
    {
        /* ------------------------- ccopy --------------------------------------------- */
        vector_init_complex_f(vcf1,(float)i + 1,(float)i + 1);
        vector_init_complex_f(vcf2,(float)i,(float)i);
        start = _rdtsc();
            mncblas_ccopy(VECSIZE,vcf1,1,vcf2,1);
        end   = _rdtsc();

        //Calculs des accès mémoire et écriture dans le fichier csv
        res = calcul_mem("mncblas_ccopy",(VECSIZE / 2) * sizeof(complexe_float_t), end - start);
        fprintf(testccopy,"ccopy;%5.3f\n",res);

        /* ------------------------- ccopy_p --------------------------------------------- */
        vector_init_complex_f(vcf1,(float)i + 1,(float)i + 1);
        vector_init_complex_f(vcf2,(float)i,(float)i);
        start = _rdtsc();
            mncblas_ccopy_p(VECSIZE,vcf1,1,vcf2,1);
        end   = _rdtsc();
        
        //Calculs des accès mémoire et écriture dans le fichier csv
        res = calcul_mem("mncblas_ccopy_p",(VECSIZE / 2) * sizeof(complexe_float_t), end - start);
        fprintf(testccopy,"ccopy_p;%5.3f\n",res);

        /* ----------------------- ccopy_v ---------------------------------------------- */
        vector_init_complex_f(vcf1,(float)i + 1,(float)i + 1);
        vector_init_complex_f(vcf2,(float)i,(float)i);
        start = _rdtsc();
            mncblas_ccopy_v(VECSIZE,vcf1,1,vcf2,1);
        end   = _rdtsc();
        
        //Calculs des accès mémoire et écriture dans le fichier csv
        res = calcul_mem("mncblas_ccopy_v",(VECSIZE / 2) * sizeof(complexe_float_t), end - start);
        fprintf(testccopy,"ccopy_v;%5.3f\n",res);

        /* ---------------------- ccopy_vp --------------------------------------------- */
        vector_init_complex_f(vcf1,(float)i + 1,(float)i + 1);
        vector_init_complex_f(vcf2,(float)i,(float)i);
        start = _rdtsc();
            mncblas_ccopy_vp(VECSIZE,vcf1,1,vcf2,1);
        end   = _rdtsc();
        
        //Calculs des accès mémoire et écriture dans le fichier csv
        res = calcul_mem("mncblas_ccopy_vp",(VECSIZE / 2) * sizeof(complexe_float_t), end - start);
        fprintf(testccopy,"ccopy_vp;%5.3f\n",res);
        printf("\n");
    }
    fclose(testccopy);
    printf("--------------------------------------------\n\n");

    i = 0;
    vcomplex_d vcd1, vcd2;
    testzcopy = fopen("test/zcopy.csv", "w");
    fprintf(testzcopy, "fonction;O/s\n");
    for(; i < NB_FOIS; i++)
    {
        /* -------------------------- zcopy ---------------------------------------------- */
        vector_init_complex_d(vcd1,(double)i + 1,(double)i + 1);
        vector_init_complex_d(vcd2,(double)i,(double)i);
        start = _rdtsc();
            mncblas_zcopy(VECSIZE,vcd1,1,vcd2,1);
        end   = _rdtsc();

        //Calculs des accès mémoire et écriture dans le fichier csv
        res = calcul_mem("mncblas_zcopy",(VECSIZE / 2) * sizeof(complexe_double_t), end - start);
        fprintf(testzcopy,"zcopy;%5.3f\n",res);

        /* ------------------------- zcopy_p --------------------------------------------- */
        vector_init_complex_d(vcd1,(double)i + 1,(double)i + 1);
        vector_init_complex_d(vcd2,(double)i,(double)i);
        start = _rdtsc();
            mncblas_zcopy_p(VECSIZE,vcd1,1,vcd2,1);
        end   = _rdtsc();
        
        //Calculs des accès mémoire et écriture dans le fichier csv
        res = calcul_mem("mncblas_zcopy_p",(VECSIZE / 2) * sizeof(complexe_double_t), end - start);
        fprintf(testzcopy,"zcopy_p;%5.3f\n",res);

        /* ------------------------ zcopy_v --------------------------------------------- */
        vector_init_complex_d(vcd1,(double)i + 1,(double)i + 1);
        vector_init_complex_d(vcd2,(double)i,(double)i);
        start = _rdtsc();
            mncblas_zcopy_v(VECSIZE,vcd1,1,vcd2,1);
        end   = _rdtsc();
        
        //Calculs des accès mémoire et écriture dans le fichier csv
        res = calcul_mem("mncblas_zcopy_v",(VECSIZE / 2) * sizeof(complexe_double_t), end - start);
        fprintf(testzcopy,"zcopy_v;%5.3f\n",res);

        /* --------------------- zcopy_vp ---------------------------------------------- */
        vector_init_complex_d(vcd1,(double)i + 1,(double)i + 1);
        vector_init_complex_d(vcd2,(double)i,(double)i);
        start = _rdtsc();
            mncblas_zcopy_vp(VECSIZE,vcd1,1,vcd2,1);
        end   = _rdtsc();
        
        //Calculs des accès mémoire et écriture dans le fichier csv
        res = calcul_mem("mncblas_zcopy_vp",(VECSIZE / 2) * sizeof(complexe_double_t), end - start);
        fprintf(testzcopy,"zcopy_vp;%5.3f\n",res);

        printf("\n");
    }
    fclose(testzcopy);
    printf("\n");
    return 0;
}