#include <stdio.h>
#include <x86intrin.h>
#include <assert.h>
#include <math.h>
#include <float.h>

#include "test.h"
#include "../include/mnblas.h"
#include "utils/flop.h"
#include "utils/vector.h"
#include "../include/complexe.h"

#define NB_FOIS    5

int test_sdot();
int test_ddot();
int test_cdotu_sub();
int test_cdotc_sub();
int test_zdotu_sub();
int test_zdotc_sub();


int test_dot()
{
  test_sdot();
  test_ddot();
  test_cdotu_sub();
  test_cdotc_sub();
  test_zdotu_sub();
  test_zdotc_sub();
  return 1;
}

int test_sdot()
{
  printf("\n");
  printf("mncblas_sdot\n");
  printf("--------------------------------------------\n");
  unsigned long long start, end;
  vfloat v1, v2;
  FILE *testsdot;
  float res;

  testsdot = fopen("test/sdottest.csv", "w");
  fprintf(testsdot, "fonction;GFLOP/s\n");

  for(int i = 0; i < NB_FOIS; i++)
  {
    res = 0.0;

    /* ------------------------ sdot ----------------------------- */
    vector_init(v1,(float)i);
    vector_init(v2,1.0);
    start = _rdtsc();
      res = mncblas_sdot(VECSIZE,v1,1,v2,1);
    end   = _rdtsc();

    //Calculs des Flops et écriture dans le fichier csv
    res = calcul_flop("mncblas_sdot",2 * VECSIZE, end - start);
    fprintf(testsdot,"sdot;%5.3f\n",res);

    /* ------------------------ sdot_p --------------------------- */
    vector_init(v1,(float)i);
    vector_init(v2,1.0);
    start = _rdtsc();
      res = mncblas_sdot_p(VECSIZE,v1,1,v2,1);
    end   = _rdtsc();
    
    //Calculs des Flops et écriture dans le fichier csv
    res = calcul_flop("mncblas_sdot_p",2 * VECSIZE, end - start);
    fprintf(testsdot,"sdot_p;%5.3f\n",res);
    
    /* ----------------------- sdot_v ----------------------------- */
    vector_init(v1,(float)i);
    vector_init(v2,1.0);
    start = _rdtsc();
      res = mncblas_sdot_v(VECSIZE,v1,1,v2,1);
    end   = _rdtsc();
    
    //Calculs des Flops et écriture dans le fichier csv
    res = calcul_flop("mncblas_sdot_v",2 * VECSIZE, end - start);
    fprintf(testsdot,"sdot_v;%5.3f\n",res);

    /* ------------------------ sdot_vp -------------------------- */
    vector_init(v1,(float)i);
    vector_init(v2,1.0);
    start = _rdtsc();
      res = mncblas_sdot_vp(VECSIZE,v1,1,v2,1);
    end   = _rdtsc();

    //Calculs des Flops et écriture dans le fichier csv
    res = calcul_flop("mncblas_sdot_vp",2 * VECSIZE, end - start);
    fprintf(testsdot,"sdot_vp;%5.3f\n",res);
    printf("--------------------------------------------\n");
  }
  return 1;

}

int test_ddot()
{
  printf("\n");
  printf("mncblas_ddot\n");
  printf("--------------------------------------------\n");
  unsigned long long start, end;
  vdouble v1, v2;
  
  float perf;
  FILE *f;
  if((f = fopen("test/ddot.csv","w")) == NULL) return 0;
  fprintf(f,"fonction;GFLOP/s\n");

  for(int i = 0; i < NB_FOIS; i++)
  {

    /* --------------------- ddot ------------------- */
    vector_init_double(v1,(double)i);
    vector_init_double(v2,1.0);
    start = _rdtsc();
      mncblas_ddot(VECSIZE,v1,1,v2,1);
    end   = _rdtsc();

    perf = calcul_flop("mncblas_ddot",2 * VECSIZE, end - start);
    fprintf(f,"%s;%5.3f\n","mncblas_ddot",perf);

    /* ------------------- ddot_p ------------------- */
    vector_init_double(v1,(double)i);
    vector_init_double(v2,1.0);
    start = _rdtsc();
      mncblas_ddot_p(VECSIZE,v1,1,v2,1);
    end   = _rdtsc();
    
    perf = calcul_flop("mncblas_ddot_p",2 * VECSIZE, end - start);
    fprintf(f,"%s;%5.3f\n","mncblas_ddot_p",perf);
    
    /* ------------------- ddot_v ------------------ */
    vector_init_double(v1,(double)i);
    vector_init_double(v2,1.0);
    start = _rdtsc();
      mncblas_ddot_v(VECSIZE,v1,1,v2,1);
    end   = _rdtsc();

    perf = calcul_flop("mncblas_ddot_v",2 * VECSIZE, end - start);
    fprintf(f,"%s;%5.3f\n","mncblas_ddot_v",perf);

    /* ----------------- ddot_vp ------------------ */
    vector_init_double(v1,(double)i);
    vector_init_double(v2,1.0);
    start = _rdtsc();
      mncblas_ddot_vp(VECSIZE,v1,1,v2,1);
    end   = _rdtsc();
    

    perf = calcul_flop("mncblas_ddot_vp",2 * VECSIZE, end - start);
    fprintf(f,"%s;%5.3f\n","mncblas_ddot_vp",perf);
    printf("--------------------------------------------\n");
  }
  fclose(f);
  return 1;

}

int test_cdotu_sub()
{
  printf("\n");
  printf("mncblas_cdotu_sub\n");
  printf("--------------------------------------------\n");
  unsigned long long start, end;
  vcomplex_f v1, v2;

  complexe_float_t res = {0,0};
  
  FILE *testcdotusub;
  float gflops;

  if((testcdotusub = fopen("test/cdotusub.csv","w")) == NULL) return 0;
  fprintf(testcdotusub,"fonction;GFLOP/s\n");
  for(int i = 0; i < NB_FOIS; i++)
  {
    res.real = 0.0; res.imaginary = 0.0;
    
    /* --------------------- cdotu_sub --------------------- */
    vector_init_complex_f(v1,(float)i,(float) i + 1.0);
    vector_init_complex_f(v2,1.0,1.0);
    start = _rdtsc();
      mncblas_cdotu_sub(VECSIZE,v1,1,v2,1, &res);
    end   = _rdtsc();

    gflops = calcul_flop("mncblas_cdotu_sub", 6 * VECSIZE, end - start);
    fprintf(testcdotusub,"cdotusub;%5.3f\n",gflops);

    
    /* -------------------- cdotu_sub_p ------------------- */
    vector_init_complex_f(v1,(float)i,(float) i + 1.0);
    vector_init_complex_f(v2,1.0,1.0);
    start = _rdtsc();
      mncblas_cdotu_sub_p(VECSIZE,v1,1,v2,1, &res);
    end   = _rdtsc();

    gflops = calcul_flop("mncblas_cdotu_sub_p", 6 * VECSIZE, end - start);
    fprintf(testcdotusub,"cdotusub_p;%5.3f\n",gflops);

    /* ------------------ cdotu_sub_v ---------------------- */
    vector_init_complex_f(v1,(float)i,(float) i + 1.0);
    vector_init_complex_f(v2,1.0,1.0);
    start = _rdtsc();
      mncblas_cdotu_sub_v(VECSIZE,v1,1,v2,1, &res);
    end   = _rdtsc();
    
    gflops = calcul_flop("mncblas_cdotu_sub_v", 6 * VECSIZE, end - start);
    fprintf(testcdotusub,"cdotusub_v;%5.3f\n",gflops);

    /* ------------------ cdotu_sub_vp ------------------- */
    vector_init_complex_f(v1,(float)i,(float) i + 1.0);
    vector_init_complex_f(v2,1.0,1.0);
    start = _rdtsc();
      mncblas_cdotu_sub_vp(VECSIZE,v1,1,v2,1, &res);
    end   = _rdtsc();

    gflops = calcul_flop("mncblas_cdotu_sub_vp", 6 * VECSIZE, end - start);
    fprintf(testcdotusub,"cdotusub_vp;%5.3f\n",gflops);
    printf("--------------------------------------------\n");
  }
  fclose(testcdotusub);
  return 1;
}

int test_cdotc_sub()
{
  printf("\n");
  printf("mncblas_cdotc_sub\n");
  printf("--------------------------------------------\n");
  unsigned long long start, end;
  vcomplex_f v, v1, v2;

  float perf;
  FILE *f;
  if((f = fopen("test/cdotcsub.csv","w")) == NULL) return 0;
  fprintf(f,"fonction;GFLOP/s\n");

  complexe_double_t res = {0,0};

  for(int i = 0; i < NB_FOIS; i++)
  {
    res.real = 0.0; res.imaginary = 0.0;

    /* -------------------- cdotc_sub -------------------------- */
    vector_init_complex_f(v1,(float)i,(float) i + 1.0);
    vector_init_complex_f(v2,1.0,1.0);
    vector_init_complex_f(v,0.0,0.0);
    start = _rdtsc();
      mncblas_cdotc_sub(VECSIZE,v1,1,v2,1, &res);
    end   = _rdtsc();

    perf = calcul_flop("mncblas_cdotc_sub", 6 * VECSIZE, end - start);
    fprintf(f,"%s;%5.3f\n","mncblas_cdotc_sub",perf);

    /* -------------------- cdotc_sub_p ----------------------- */
    vector_init_complex_f(v1,(float)i,(float) i + 1.0);
    vector_init_complex_f(v2,1.0,1.0);
    vector_init_complex_f(v,0.0,0.0);
    start = _rdtsc();
      mncblas_cdotc_sub_p(VECSIZE,v1,1,v2,1, &res);
    end   = _rdtsc();

    perf = calcul_flop("mncblas_cdotc_sub_p", 6 * VECSIZE, end - start);
    fprintf(f,"%s;%5.3f\n","mncblas_cdotc_sub_p",perf);

    /* ----------------- cdotc_sub_v ------------------------- */
    vector_init_complex_f(v1,(float)i,(float) i + 1.0);
    vector_init_complex_f(v2,1.0,1.0);
    vector_init_complex_f(v,0.0,0.0);
    start = _rdtsc();
       mncblas_cdotc_sub_v(VECSIZE,v1,1,v2,1, &res);
    end   = _rdtsc();

    perf = calcul_flop("mncblas_cdotc_sub_v", 6 * VECSIZE, end - start);
    fprintf(f,"%s;%5.3f\n","mncblas_cdotc_sub_v",perf);

    /* --------------- cdotc_sub_vp ------------------------- */
    vector_init_complex_f(v1,(float)i,(float) i + 1.0);
    vector_init_complex_f(v2,1.0,1.0);
    vector_init_complex_f(v,0.0,0.0);
    start = _rdtsc();
      mncblas_cdotc_sub_vp(VECSIZE,v1,1,v2,1, &res);
    end   = _rdtsc();
    perf = calcul_flop("mncblas_cdotc_sub_vp", 6 * VECSIZE, end - start);
    fprintf(f,"%s;%5.3f\n","mncblas_cdotc_sub_vp",perf);

    printf("--------------------------------------------\n");
  }
  fclose(f);
  return 1;
}

int test_zdotu_sub()
{
  printf("\n");
  printf("mncblas_zdotu_sub\n");
  printf("--------------------------------------------\n");
  unsigned long long start, end;
  vcomplex_d v1, v2;

  FILE *testzdotusub;
  float gflops;

  complexe_double_t res= {0,0};

  testzdotusub = fopen("test/zdotusub.csv", "w");
  fprintf(testzdotusub,"fonction;GFLOP/s\n");
  for(int i = 0; i < NB_FOIS; i++)
  {
    res.real = 0.0; res.imaginary = 0.0;

    /* ------------------- zdotu_sub ----------------------- */
    vector_init_complex_d(v1,(double)i, (double)i+1.0);
    vector_init_complex_d(v2,1.0,1.0);
    start = _rdtsc();
      mncblas_zdotu_sub(VECSIZE,v1,1,v2,1, &res);
    end   = _rdtsc();

    gflops = calcul_flop("mncblas_zdotu_sub",6 * VECSIZE, end - start);
    fprintf(testzdotusub,"zdotusub;%5.3f\n",gflops);

    /* ------------------ zdotu_sub_p --------------------- */
    vector_init_complex_d(v1,(double)i, (double)i+1.0);
    vector_init_complex_d(v2,1.0,1.0);
    start = _rdtsc();
      mncblas_zdotu_sub_p(VECSIZE,v1,1,v2,1, &res);
    end   = _rdtsc();

    gflops = calcul_flop("mncblas_zdotu_sub_p",6 * VECSIZE, end - start);
    fprintf(testzdotusub,"zdotusub_p;%5.3f\n",gflops);

    /* ------------------ zdotu_sub_v --------------------- */
    vector_init_complex_d(v1,(double)i, (double)i+1.0);
    vector_init_complex_d(v2,1.0,1.0);
    start = _rdtsc();
      mncblas_zdotu_sub_v(VECSIZE,v1,1,v2,1, &res);
    end   = _rdtsc();

    gflops = calcul_flop("mncblas_zdotu_sub_v",6 * VECSIZE, end - start);
    fprintf(testzdotusub,"zdotusub_v;%5.3f\n",gflops);

    /* ----------------- zdotu_sub_vp --------------------- */
    vector_init_complex_d(v1,(double)i, (double)i+1.0);
    vector_init_complex_d(v2,1.0,1.0);
    start = _rdtsc();
      mncblas_zdotu_sub_vp(VECSIZE,v1,1,v2,1, &res);
    end   = _rdtsc();

    gflops = calcul_flop("mncblas_zdotu_sub_vp",6 * VECSIZE, end - start);
    fprintf(testzdotusub,"zdotusub_vp;%5.3f\n",gflops);

    printf("--------------------------------------------\n");
  }
  fclose(testzdotusub);
  return 1;
}

int test_zdotc_sub()
{
  printf("\n");
  printf("mncblas_zdotc_sub\n");
  printf("--------------------------------------------\n");
  unsigned long long start, end;
  vcomplex_d v, v1, v2;

  float perf;
  FILE *f;
  if((f = fopen("test/zdotc_sub.csv","w")) == NULL) return 0;
  fprintf(f,"fonction;GFLOP/s\n");


  for(int i = 0; i < NB_FOIS; i++)
  {
    complexe_double_t res= {0,0};

    /* ------------------------- zdotc_sub ---------------------- */
    vector_init_complex_d(v1,(double)i, (double)i+1.0);
    vector_init_complex_d(v2,1.0,1.0);
    vector_init_complex_d(v,0.0,0.0);
    start = _rdtsc();
      mncblas_zdotc_sub(VECSIZE,v1,1,v2,1, &res);
    end   = _rdtsc();

    perf = calcul_flop("mncblas_zdotc_sub", 6 * VECSIZE, end - start);
    fprintf(f,"%s;%5.3f\n","mncblas_zdotc_sub",perf);

    /* ------------------------- zdotc_sub_p ---------------------- */
    vector_init_complex_d(v1,(double)i, (double)i+1.0);
    vector_init_complex_d(v2,1.0,1.0);
    vector_init_complex_d(v,0.0,0.0);
    start = _rdtsc();
    mncblas_zdotc_sub_p(VECSIZE,v1,1,v2,1, &res);
    end   = _rdtsc();

    perf = calcul_flop("mncblas_zdotc_sub_p", 6 * VECSIZE, end - start);
    fprintf(f,"%s;%5.3f\n","mncblas_zdotc_sub_p",perf);

    /* ------------------------- zdotc_sub_v ---------------------- */
    vector_init_complex_d(v1,(double)i, (double)i+1.0);
    vector_init_complex_d(v2,1.0,1.0);
    vector_init_complex_d(v,0.0,0.0);
    start = _rdtsc();
     mncblas_zdotc_sub_v(VECSIZE,v1,1,v2,1, &res);
    end   = _rdtsc();

    perf = calcul_flop("mncblas_zdotc_sub_v", 6 * VECSIZE, end - start);
    fprintf(f,"%s;%5.3f\n","mncblas_zdotc_sub_v",perf);

    /* ------------------------- zdotc_sub_vp ---------------------- */
    vector_init_complex_d(v1,(double)i, (double)i+1.0);
    vector_init_complex_d(v2,1.0,1.0);
    vector_init_complex_d(v,0.0,0.0);
    start = _rdtsc();
      mncblas_zdotc_sub_vp(VECSIZE,v1,1,v2,1, &res);
    end   = _rdtsc();

    perf = calcul_flop("mncblas_zdotc_sub_vp", 6 * VECSIZE, end - start);
    fprintf(f,"%s;%5.3f\n","mncblas_zdotc_sub_vp",perf);
    printf("--------------------------------------------\n");
  }
  fclose(f);
  return 1;
}
