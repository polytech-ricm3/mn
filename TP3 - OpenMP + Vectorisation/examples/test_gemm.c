#include <stdio.h>
#include <x86intrin.h>
#include <assert.h>
#include <math.h>
#include <float.h>

#include "test.h"
#include "../include/mnblas.h"
#include "utils/flop.h"
#include "utils/matrix.h"
#include "utils/vector.h"

#define NB_FOIS 5
#define MATSIZE 128
#define FLT_ROUNDS 0


int test_sgemm();
int test_dgemm();
int test_cgemm();
int test_zgemm();

int test_gemm()
{
    test_sgemm();
    test_dgemm();
    test_cgemm();
    test_zgemm();
    return 1;
}

int test_sgemm()
{
    printf("\n");
    printf("mnblas_sgemm\n");
    printf("--------------------------------------------\n");
    unsigned long long start, end;
    float v1[MATSIZE * MATSIZE], v2[MATSIZE * MATSIZE], v2s[MATSIZE * MATSIZE], A[MATSIZE * MATSIZE];
    float alpha = 1, beta = 2, perf;

    FILE *f;
    if((f = fopen("test/sgemm.csv","w")) == NULL) return 0;
    fprintf(f,"fonction;GFLOPS/s\n");

    for(int i = 0; i < MATSIZE * MATSIZE; i++)
    {
        v1[i] = 1; v2[i] = 2;
        A[i]  = 3;
    }

    mncblas_scopy(MATSIZE * MATSIZE,v2,1,v2s,1);
    for(int i = 0; i < NB_FOIS; i++)
    {        
        start = _rdtsc();
            mncblas_sgemm(101, 111,111, MATSIZE, MATSIZE,MATSIZE, alpha, A, MATSIZE, v1, MATSIZE, beta, v2s, MATSIZE);
        end = _rdtsc();
        perf = calcul_flop("mnblas_sgemm",2*MATSIZE * MATSIZE *MATSIZE, end - start);
        fprintf(f,"%s;%3.5f\n","mnblas_sgemm",perf);

        start = _rdtsc();
            mncblas_sgemm_p(101, 111,111, MATSIZE, MATSIZE,MATSIZE, alpha, A, MATSIZE, v1, MATSIZE, beta, v2, MATSIZE);
        end = _rdtsc();
        perf = calcul_flop("mnblas_sgemm_p",2*MATSIZE * MATSIZE *MATSIZE, end - start);
        fprintf(f,"%s;%3.5f\n","mnblas_sgemm_p",perf);
        start = _rdtsc();
            mncblas_sgemm_v(101, 111,111, MATSIZE, MATSIZE,MATSIZE, alpha, A, MATSIZE, v1, MATSIZE, beta, v2, MATSIZE);
        end = _rdtsc();
        perf = calcul_flop("mnblas_sgemm_v",2*MATSIZE * MATSIZE *MATSIZE, end - start);
        fprintf(f,"%s;%3.5f\n","mnblas_sgemm_v",perf);
        start = _rdtsc();
            mncblas_sgemm_vp(101, 111,111, MATSIZE, MATSIZE,MATSIZE, alpha, A, MATSIZE, v1, MATSIZE, beta, v2, MATSIZE);
        end = _rdtsc();
        perf = calcul_flop("mnblas_sgemm_vp",2*MATSIZE * MATSIZE *MATSIZE, end - start);
        fprintf(f,"%s;%3.5f\n","mnblas_sgemm_vp",perf);

        printf("[%i] Test de %s : OK\n",i,"mnblas_sgemm");
    }
    fclose(f);
    return 1;
}

int test_dgemm()
{
    printf("\n");
    printf("mnblas_dgemm\n");
    printf("--------------------------------------------\n");
    unsigned long long start, end;
    double v1[MATSIZE * MATSIZE], v2[MATSIZE * MATSIZE], v2s[MATSIZE * MATSIZE], A[MATSIZE * MATSIZE];
    double alpha = 1, beta = 2;
    float  perf;

    FILE *f;
    if((f = fopen("test/dgemm.csv","w")) == NULL) return 0;
    fprintf(f,"fonction;GFPLO/s\n");

    for(int i = 0; i < MATSIZE * MATSIZE; i++)
    {
        v1[i] = 1; v2[i] = 2;
        A[i]  = 3;
    }

    mncblas_dcopy(MATSIZE * MATSIZE,v2,1,v2s,1);
    for(int i = 0; i < NB_FOIS; i++)
    {   
        start = _rdtsc();
            mncblas_dgemm(101, 111,111, MATSIZE, MATSIZE,MATSIZE, alpha, A, MATSIZE, v1, MATSIZE, beta, v2s, MATSIZE);
        end = _rdtsc();
        perf = calcul_flop("mnblas_dgemm",2*MATSIZE * MATSIZE *MATSIZE, end - start);
        fprintf(f,"%s;%5.3f\n","mnblas_dgemm",perf);

        start = _rdtsc();
            mncblas_dgemm_p(101, 111,111, MATSIZE, MATSIZE,MATSIZE, alpha, A, MATSIZE, v1, MATSIZE, beta, v2, MATSIZE);
        end = _rdtsc();
        perf = calcul_flop("mnblas_dgemm_p",2*MATSIZE * MATSIZE *MATSIZE, end - start);
        fprintf(f,"%s;%5.3f\n","mnblas_dgemm_p",perf);
        start = _rdtsc();
            mncblas_dgemm_v(101, 111,111, MATSIZE, MATSIZE,MATSIZE, alpha, A, MATSIZE, v1, MATSIZE, beta, v2, MATSIZE);
        end = _rdtsc();
        perf = calcul_flop("mnblas_dgemm_v",2*MATSIZE * MATSIZE *MATSIZE, end - start);
        fprintf(f,"%s;%5.3f\n","mnblas_dgemm_v",perf);
        start = _rdtsc();
            mncblas_dgemm_vp(101, 111,111, MATSIZE, MATSIZE,MATSIZE, alpha, A, MATSIZE, v1, MATSIZE, beta, v2, MATSIZE);
        end = _rdtsc();
        perf = calcul_flop("mnblas_dgemm_vp",2*MATSIZE * MATSIZE *MATSIZE, end - start);
        fprintf(f,"%s;%5.3f\n","mnblas_dgemm_vp",perf);
        printf("[%i] Test de %s : OK\n",i,"mnblas_dgemm");
    }
    fclose(f);
    return 1;
}

int test_cgemm()
{
    printf("\n");
    printf("mncblas_cgemm\n");
    printf("--------------------------------------------\n");

    unsigned long long start, end;
    int i;
    float perf;
    complexe_float_t cf1 = {1,1}, cf2 = {2,2};
    complexe_float_t alpha, beta;
    complexe_float_t v1[MATSIZE * MATSIZE], v2[MATSIZE * MATSIZE], v2s[MATSIZE * MATSIZE], A[MATSIZE * MATSIZE];

    FILE *f;
    if((f = fopen("test/cgemm.csv","w")) == NULL) return 0;
    fprintf(f,"fonction;GFLOP/s\n");

    alpha = cf2; beta = cf1;

    for(i = 0; i < MATSIZE * MATSIZE; i++)
    {
        v1[i] = cf1; v2[i] = cf2;
        A[i] = cf1;
    }

    mncblas_ccopy(MATSIZE*MATSIZE, v2, 1, v2s, 1);
    for (i = 0; i < NB_FOIS; i++) {

        start = _rdtsc();
        mncblas_cgemm(101, 111, 111,MATSIZE, MATSIZE,MATSIZE,&alpha, A, MATSIZE, v1, MATSIZE, &beta, v2s, MATSIZE);
        end = _rdtsc();
        perf = calcul_flop("mncblas_cgemm",(MATSIZE * MATSIZE) * (2*MATSIZE+4), end - start);
        fprintf(f,"%s;%5.3f\n","mncblas_cgemm",perf);
         start = _rdtsc();
        mncblas_cgemm_p(101, 111,111, MATSIZE, MATSIZE,MATSIZE,&alpha, A, MATSIZE, v1, MATSIZE, &beta, v2, MATSIZE);
        end = _rdtsc();
        perf = calcul_flop("mncblas_cgemm_p",(MATSIZE * MATSIZE) * (2*MATSIZE+4), end - start);
        fprintf(f,"%s;%5.3f\n","mncblas_cgemm_p",perf);
         start = _rdtsc();
        mncblas_cgemm_v(101, 111,111, MATSIZE, MATSIZE,MATSIZE,&alpha, A, MATSIZE, v1, MATSIZE, &beta, v2, MATSIZE);
        end = _rdtsc();
        perf = calcul_flop("mncblas_cgemm_v",(MATSIZE * MATSIZE) * (2*MATSIZE+4), end - start);
        fprintf(f,"%s;%5.3f\n","mncblas_cgemm_v",perf);
         start = _rdtsc();
        mncblas_cgemm_vp(101, 111,111, MATSIZE, MATSIZE,MATSIZE,&alpha, A, MATSIZE, v1, MATSIZE, &beta, v2, MATSIZE);
        end = _rdtsc();
        perf = calcul_flop("mncblas_cgemm_vp",(MATSIZE * MATSIZE) * (2*MATSIZE+4), end - start);
        fprintf(f,"%s;%5.3f\n","mncblas_cgemm_vp",perf);
        printf("[%i] Test de %s : OK\n",i,"mncblas_cgemm");
    }
    fclose(f);
    return 1;
}

int test_zgemm()
{
    printf("\n");
    printf("mncblas_Zgemm\n");
    printf("--------------------------------------------\n");

    unsigned long long start, end;
    int i;
    float perf;
    complexe_double_t cf1 = {1,1}, cf2 = {2,2};
    complexe_double_t alpha, beta;
    complexe_double_t v1[MATSIZE * MATSIZE], v2[MATSIZE * MATSIZE], v2s[MATSIZE * MATSIZE], A[MATSIZE * MATSIZE];

    FILE *f;
    if((f = fopen("test/zgemm.csv","w")) == NULL) return 0;
    fprintf(f,"fonction,GFLOP/s\n");

    alpha = cf2; beta = cf1;

    for(i = 0; i < MATSIZE * MATSIZE; i++)
    {
        v1[i] = cf1; v2[i] = cf2;
        A[i] = cf1;
    }

    mncblas_zcopy(MATSIZE*MATSIZE, v2, 1, v2s, 1);
    for (i = 0; i < NB_FOIS; i++) {

    /*                                Test_zgemm                                              */

        start = _rdtsc();
        mncblas_zgemm(101, 111,111, MATSIZE, MATSIZE,MATSIZE,&alpha, A, MATSIZE, v1, MATSIZE, &beta, v2s, MATSIZE);
        end = _rdtsc();
        perf = calcul_flop("mncblas_Zgemm",(MATSIZE * MATSIZE) * (2*MATSIZE+4), end - start);
        fprintf(f,"%s;%5.3f\n","mncblas_Zgemm",perf);
        start = _rdtsc();
        mncblas_zgemm_p(101, 111,111, MATSIZE, MATSIZE,MATSIZE,&alpha, A, MATSIZE, v1, MATSIZE, &beta, v2s, MATSIZE);
        end = _rdtsc();
        perf = calcul_flop("mncblas_Zgemm_p",(MATSIZE * MATSIZE) * (2*MATSIZE+4), end - start);
        fprintf(f,"%s;%5.3f\n","mncblas_Zgemm",perf);
        start = _rdtsc();
        mncblas_zgemm_v(101, 111,111, MATSIZE, MATSIZE,MATSIZE,&alpha, A, MATSIZE, v1, MATSIZE, &beta, v2s, MATSIZE);
        end = _rdtsc();
        perf = calcul_flop("mncblas_Zgemm_v",(MATSIZE * MATSIZE) * (2*MATSIZE+4), end - start);
        fprintf(f,"%s;%5.3f\n","mncblas_Zgemm",perf);
        start = _rdtsc();
        mncblas_zgemm_vp(101, 111,111, MATSIZE, MATSIZE,MATSIZE,&alpha, A, MATSIZE, v1, MATSIZE, &beta, v2s, MATSIZE);
        end = _rdtsc();
        perf = calcul_flop("mncblas_Zgemm_vp",(MATSIZE * MATSIZE) * (2*MATSIZE+4), end - start);
        fprintf(f,"%s;%5.3f\n","mncblas_Zgemm",perf);
        printf("[%i] Test de %s : OK\n",i,"mncblas_Zgemm");
    }
    fclose(f);
    return 1;
}
