#include <stdio.h>
#include <x86intrin.h>
#include <assert.h>
#include <math.h>
#include <float.h>

#include "test.h"
#include "../include/mnblas.h"
#include "utils/flop.h"
#include "utils/matrix.h"
#include "utils/vector.h"

#define NB_FOIS 2
#define MATSIZE 256
#define FLT_ROUNDS 0

//Verifie l'égalité de deux vecteurs simple

int test_sgemv();
int test_dgemv();
int test_cgemv();
int test_zgemv();

int test_gemv()
{
  test_sgemv();
  test_dgemv();
  test_cgemv();
  test_zgemv();
  return 1;
}

int test_sgemv()
{
  FILE *sgemv_f;
  if((sgemv_f = fopen("test/sgemv.csv","w")) == NULL) return 0;
  fprintf(sgemv_f,"fonction;GFLOP/s\n");
  printf("\n");
  printf("mnblas_sgemv\n");
  printf("--------------------------------------------\n");
  unsigned long long start, end;
  vfloat v1, v2, v2s;
  float alpha, beta, perf;
  mfloat* A;

  int i;

  vector_random(v1);
  vector_random(v2);

  alpha = 10.0;
  beta = 1.2;

  mncblas_scopy(MATSIZE, v2, 1, v2s, 1);

  A = matrix_random(MATSIZE, MATSIZE);

  for (i = 0; i < NB_FOIS; i++) {

    /*                                Test_sgemv                                              */

    start = _rdtsc();
      mncblas_sgemv(101, 111, MATSIZE, MATSIZE, alpha, A->donnees, MATSIZE, v1, 1, beta, v2, 1);
    end = _rdtsc();
    perf = calcul_flop("mnblas_sgemv",2 * (MATSIZE * MATSIZE) + 3 * MATSIZE, end - start);
    fprintf(sgemv_f,"%s;%5.3f\n","mnblas_sgemv",perf);
    start = _rdtsc();
    mncblas_sgemv_p(101, 111, MATSIZE, MATSIZE, alpha, A->donnees, MATSIZE, v1, 1, beta, v2s, 1);
    end = _rdtsc();
    perf = calcul_flop("mnblas_sgemv_p",2 * (MATSIZE * MATSIZE) + 3 * MATSIZE, end - start);
    fprintf(sgemv_f,"%s;%5.3f\n","mnblas_sgemv_p",perf);
    start = _rdtsc();
    mncblas_sgemv_v(101, 111, MATSIZE, MATSIZE, alpha, A->donnees, MATSIZE, v1, 1, beta, v2s, 1);
    end = _rdtsc();
    perf = calcul_flop("mnblas_sgemv_v",2 * (MATSIZE * MATSIZE) + 3 * MATSIZE, end - start);
    fprintf(sgemv_f,"%s;%5.3f\n","mncblas_sgemv_v",perf);
    start = _rdtsc();
    mncblas_sgemv_vp(101, 111, MATSIZE, MATSIZE, alpha, A->donnees, MATSIZE, v1, 1, beta, v2s, 1);
    end = _rdtsc();
    perf = calcul_flop("mnblas_sgemv_vp",2 * (MATSIZE * MATSIZE) + 3 * MATSIZE, end - start);
    fprintf(sgemv_f,"%s;%5.3f\n","mncblas_sgemv_vp",perf);
    printf("[%i] Test de %s : OK\n",i,"mnblas_sgemv");

  }

  free(A->donnees);
  free(A);
  fclose(sgemv_f);

  return 1;

}

int test_dgemv()
{
  printf("\n");
  printf("mnblas_dgemv\n");
  printf("--------------------------------------------\n");
  unsigned long long start, end;
  vdouble v1, v2, v2s;
  double alpha, beta, perf;
  mdouble* B;
  int i;
  FILE *f;

  if((f = fopen("test/dgemv.csv","w")) == NULL) return 0;
  fprintf(f,"fonction;GFLOP/s\n");

  vector_random_double(v1);
  vector_random_double(v2);

  alpha = 10.0;
  beta = 1.2;

  mncblas_dcopy(MATSIZE, v2, 1, v2s, 1);

  B = matrix_random_double(MATSIZE, MATSIZE);

  for (i = 0; i < NB_FOIS; i++) {
    /*                                Test_dgemv                                              */
    start = _rdtsc();
    mncblas_dgemv(101, 111, MATSIZE, MATSIZE, alpha, B->donnees, MATSIZE, v1, 1, beta, v2s, 1);
    end = _rdtsc();
    perf = calcul_flop("mnblas_dgemv",2 * (MATSIZE * MATSIZE) + 3 * MATSIZE, end - start);
    fprintf(f,"%s;%5.3f\n","mnblas_dgemv",perf);
    start = _rdtsc();
    mncblas_dgemv_p(101, 111, MATSIZE, MATSIZE, alpha, B->donnees, MATSIZE, v1, 1, beta, v2s, 1);
    end = _rdtsc();
    perf = calcul_flop("mnblas_dgemv_p",2 * (MATSIZE * MATSIZE) + 3 * MATSIZE, end - start);
    fprintf(f,"%s;%5.3f\n","mnblas_dgemv_p",perf);
    start = _rdtsc();
    mncblas_dgemv_v(101, 111, MATSIZE, MATSIZE, alpha, B->donnees, MATSIZE, v1, 1, beta, v2s, 1);
    end = _rdtsc();
    perf = calcul_flop("mnblas_dgemv_v",2 * (MATSIZE * MATSIZE) + 3 * MATSIZE, end - start);
    fprintf(f,"%s;%5.3f\n","mnblas_dgemv_v",perf);
    start = _rdtsc();
    mncblas_dgemv_vp(101, 111, MATSIZE, MATSIZE, alpha, B->donnees, MATSIZE, v1, 1, beta, v2s, 1);
    end = _rdtsc();
    perf = calcul_flop("mnblas_dgemv_vp",2 * (MATSIZE * MATSIZE) + 3 * MATSIZE, end - start);
    fprintf(f,"%s;%5.3f\n","mnblas_dgemv_vp",perf);
    printf("[%i] Test de %s : OK\n",i,"mnblas_dgemv");



  }
  free(B->donnees);
  free(B);
  fclose(f);
  return 1;

}


int test_cgemv()
{
  printf("\n");
  printf("mnblas_cgemv\n");
  printf("--------------------------------------------\n");
  
  unsigned long long start, end;
  int i;
  float perf;
  complexe_float_t cf1 = {1,1}, cf2 = {2,2};
  complexe_float_t alpha, beta;
  complexe_float_t v1[MATSIZE], v2[MATSIZE], v2s[MATSIZE];
  complexe_float_t B[MATSIZE * MATSIZE];

  FILE *f;
  if((f = fopen("test/cgemv.csv","w")) == NULL) return 0;
  fprintf(f,"fonction;GFLOP/s\n");

  alpha = cf2; beta = cf1;

  for(i = 0; i < MATSIZE; i++)
  {
    v1[i] = cf1; v2[i] = cf2;
  }

  for(i = 0; i < MATSIZE * MATSIZE; i++)
  {
     B[i] = cf1;
  }

  mncblas_ccopy(MATSIZE, v2, 1, v2s, 1);
  for (i = 0; i < NB_FOIS; i++) {

    start = _rdtsc();
    mncblas_cgemv(101, 111, MATSIZE, MATSIZE, &alpha, B, MATSIZE, v1, 1, &beta, v2s, 1);
    end = _rdtsc();
    perf = calcul_flop("mnblas_cgemv",2 * (MATSIZE * MATSIZE) + 2 * MATSIZE, end - start);
    fprintf(f,"%s;%5.3f\n","mnblas_cgemv",perf);

    start = _rdtsc();
    mncblas_cgemv_p(101, 111, MATSIZE, MATSIZE, &alpha, B, MATSIZE, v1, 1, &beta, v2s, 1);
    end = _rdtsc();
    perf = calcul_flop("mnblas_cgemv_p",2 * (MATSIZE * MATSIZE) + 2 * MATSIZE, end - start);
    fprintf(f,"%s;%5.3f\n","mncblas_cgemv_p",perf);
    start = _rdtsc();
    mncblas_cgemv_v(101, 111, MATSIZE, MATSIZE, &alpha, B, MATSIZE, v1, 1, &beta, v2s, 1);
    end = _rdtsc();
    perf = calcul_flop("mnblas_cgemv_v",2 * (MATSIZE * MATSIZE) + 2 * MATSIZE, end - start);
    fprintf(f,"%s;%5.3f\n","mncblas_cgemv_v",perf);
    start = _rdtsc();
    mncblas_cgemv_vp(101, 111, MATSIZE, MATSIZE, &alpha, B, MATSIZE, v1, 1, &beta, v2s, 1);
    end = _rdtsc();
    perf = calcul_flop("mnblas_cgemv_vp",2 * (MATSIZE * MATSIZE) + 2 * MATSIZE, end - start);
    fprintf(f,"%s;%5.3f\n","mncblas_cgemv_vp",perf);

    printf("[%i] Test de %s : OK\n",i,"mnblas_cgemv");
  }
  fclose(f);
  return 1;
}

int test_zgemv()
{
  printf("\n\n");
  printf("mnblas_zgemv\n");
  printf("--------------------------------------------\n");
  
  unsigned long long start, end;
  int i;
  float perf;
  complexe_double_t cf1 = {1,1}, cf2 = {2,2};
  complexe_double_t alpha, beta;
  complexe_double_t v1[MATSIZE], v2[MATSIZE], v2s[MATSIZE];
  complexe_double_t B[MATSIZE * MATSIZE];

  FILE *f;
  if((f = fopen("test/zgemv.csv","w")) == NULL) return 0;
  fprintf(f,"fonction;GFLOP/s\n");

  alpha = cf2; beta = cf1;

  for(i = 0; i < MATSIZE; i++)
  {
    v1[i] = cf1; v2[i] = cf2;
  }

  for(i = 0; i < MATSIZE * MATSIZE; i++)
  {
     B[i] = cf1;
  }

  mncblas_zcopy(MATSIZE, v2, 1, v2s, 1);
  for (i = 0; i < NB_FOIS; i++) {

    /*                                Test_zgemv                                              */
    start = _rdtsc();
    mncblas_zgemv(101, 111, MATSIZE, MATSIZE, &alpha, B, MATSIZE, v1, 1, &beta, v2s, 1);
    end = _rdtsc();
    perf = calcul_flop("mnblas_zgemv",2 * (MATSIZE * MATSIZE) + 2 * MATSIZE, end - start);
    fprintf(f,"%s;%5.3f\n","mnblas_zgemv",perf);
    start = _rdtsc();
    mncblas_zgemv_p(101, 111, MATSIZE, MATSIZE, &alpha, B, MATSIZE, v1, 1, &beta, v2s, 1);
    end = _rdtsc();
    perf = calcul_flop("mnblas_zgemv_p",2 * (MATSIZE * MATSIZE) + 2 * MATSIZE, end - start);
    fprintf(f,"%s;%5.3f\n","mnblas_zgemv_p",perf);
    start = _rdtsc();
    mncblas_zgemv_v(101, 111, MATSIZE, MATSIZE, &alpha, B, MATSIZE, v1, 1, &beta, v2s, 1);
    end = _rdtsc();
    perf = calcul_flop("mnblas_zgemv_v",2 * (MATSIZE * MATSIZE) + 2 * MATSIZE, end - start);
    fprintf(f,"%s;%5.3f\n","mnblas_zgemv_v",perf);
    start = _rdtsc();
    mncblas_zgemv_vp(101, 111, MATSIZE, MATSIZE, &alpha, B, MATSIZE, v1, 1, &beta, v2s, 1);
    end = _rdtsc();
    perf = calcul_flop("mnblas_zgemv_vp",2 * (MATSIZE * MATSIZE) + 2 * MATSIZE, end - start);
    fprintf(f,"%s;%5.3f\n","mnblas_zgemv_vp",perf);
    printf("[%i] Test de %s : OK\n",i,"mnblas_cgemv");
  }
  fclose(f);
  return 1;
}
