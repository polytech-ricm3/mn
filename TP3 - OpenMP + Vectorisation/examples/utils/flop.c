#include <stdio.h>
#include <x86intrin.h>

static const float duree_cycle = (float) 1 / (float) 2.6 ;
// duree du cycle en nano seconde 10^-9

float calcul_flop (char *message, int nb_operations_flottantes, unsigned long long int cycles)
{
  float res = ((float) nb_operations_flottantes) / (((float) cycles) * duree_cycle);
  printf ("%s %d operations %5.3f GFLOP/s\n", message, nb_operations_flottantes, res);
  return res;
}

float calcul_mem  (char *message, int nb_access_mem, unsigned long long int time)
{
  float res = (float)(nb_access_mem/(float)(time * duree_cycle));
  printf("%s %d acces mem %5.3f o/s\n",message,nb_access_mem,res);
  return res;
}