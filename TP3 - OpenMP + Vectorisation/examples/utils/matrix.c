#include "matrix.h"

#include <assert.h>
#include <time.h>
#include <stdlib.h>

mfloat* matrix_random(int row, int col)
{
  mfloat *m = malloc(sizeof(mfloat));
  m->l = row;
  m->c = col;
  m->donnees = malloc(sizeof(float)*row*col);

  for (int i = 0; i < row * col; i++) {
    m->donnees[i] = (float) (rand()%MAX_RAND);
  }
  return m;
}

mdouble* matrix_random_double(int row, int col)
{
  mdouble *m = malloc(sizeof(mfloat));
  m->l = row;
  m->c = col;
  m->donnees = malloc(sizeof(double)*row*col);

  for (int i = 0; i < row * col; i++) {
    m->donnees[i] = (double) (rand()%MAX_RAND);
  }
  return m;
}

m_complexe_float* matrix_random_complexe_f(int row, int col)
{
  m_complexe_float *m = malloc(sizeof(m_complexe_float));
  m->l = row;
  m->c = col;
  m->donnees = malloc(sizeof(complexe_float_t)*row*col);

  for (int i = 0; i < row * col; i++) {
    complexe_float_t *tmp = malloc(sizeof(complexe_float_t));
    tmp->real = rand()%MAX_RAND;
    tmp->imaginary = rand()%MAX_RAND;
    //printf("%f %f\n",tmp->real,tmp->imaginary);
    m->donnees[i] = tmp;
  }
//  printf("-------------------------FIN------------------------------\n");
  return m;
}

m_complexe_double* matrix_random_complexe_d(int row, int col)
{
  m_complexe_double *m = malloc(sizeof(m_complexe_double));
  m->l = row;
  m->c = col;
  m->donnees = malloc(sizeof(complexe_double_t)*row*col);

  for (int i = 0; i < row * col; i++) {
    complexe_double_t *tmp = malloc(sizeof(complexe_double_t));
    tmp->real = (double)(rand()%MAX_RAND);
    tmp->imaginary = (double)(rand()%MAX_RAND);
    m->donnees[i] = tmp;
  }
  return m;
}
