#include <stdio.h>
#include <stdlib.h>

#include "../../include/complexe.h"

#define MAX_RAND 50

typedef struct {
  int l, c;
  float *donnees;
} mfloat;

typedef struct {
  int l, c;
  double *donnees;
} mdouble;

typedef struct {
  int l, c;
  complexe_float_t **donnees;
} m_complexe_float;

typedef struct {
  int l, c;
  complexe_double_t **donnees;
} m_complexe_double;

mfloat* matrix_random(int row, int col);
mdouble* matrix_random_double(int row, int col);
m_complexe_float* matrix_random_complexe_f(int row, int col);
m_complexe_double* matrix_random_complexe_d(int row, int col);
