#ifndef COMPLEXE_H__
#define COMPLEXE_H__

#include <x86intrin.h>
#include <smmintrin.h>

typedef struct {
  float real;
  float imaginary;
} complexe_float_t;

typedef struct {
  double real;
  double imaginary;
} complexe_double_t;

static inline complexe_float_t add_complexe_float(const complexe_float_t c1,
                                                  const complexe_float_t c2) {
  complexe_float_t r;

  r.real = c1.real + c2.real;
  r.imaginary = c1.imaginary + c2.imaginary;

  return r;
}

static inline void add_complexe_float_v(__m128 *Xr, __m128 *Xi, __m128 *Yr, __m128 *Yi, __m128 *Rr, __m128 *Ri) {

  *Rr = _mm_add_ps(*Xr, *Yr);
  *Ri = _mm_add_ps(*Xi, *Yi);
}

static inline complexe_double_t add_complexe_double(const complexe_double_t c1, const complexe_double_t c2) {
  complexe_double_t r;

  r.real = c1.real + c2.real;
  r.imaginary = c1.imaginary + c2.imaginary;

  return r;
}

static inline void add_complexe_double_v(__m128d *Xr, __m128d *Xi, __m128d *Yr, __m128d *Yi, __m128d *Rr, __m128d *Ri) {
  *Rr = _mm_add_pd(*Xr, *Yr);
  *Ri = _mm_add_pd(*Xi, *Yi);
}

static inline complexe_float_t mult_complexe_float(const complexe_float_t c1,
                                                   const complexe_float_t c2) {
  complexe_float_t r;

  r.real = (c1.real * c2.real) - (c1.imaginary * c2.imaginary);
  r.imaginary = (c1.real * c2.imaginary) + (c1.imaginary * c2.real);

  return r;
}

static inline void mult_complexe_float_v(__m128 *Xr, __m128 *Xi, __m128 *Yr, __m128 *Yi, __m128 *Rr, __m128 *Ri) {

  *Rr = _mm_sub_ps(_mm_mul_ps(*Xr,*Yr),_mm_mul_ps(*Xi,*Yi));
  *Ri = _mm_add_ps(_mm_mul_ps(*Xr,*Yi),_mm_mul_ps(*Xi,*Yr));
}

static inline complexe_double_t mult_complexe_double(const complexe_double_t c1, const complexe_double_t c2) {
  complexe_double_t r;

  r.real = (c1.real * c2.real) - (c1.imaginary * c2.imaginary);
  r.imaginary = (c1.real * c2.imaginary) + (c1.imaginary * c2.real);

  return r;
}
static inline void mult_complexe_double_v(__m128d *Xr, __m128d *Xi, __m128d *Yr, __m128d *Yi, __m128d *Rr, __m128d *Ri) {

  *Rr = _mm_sub_pd(_mm_mul_pd(*Xr,*Yr),_mm_mul_pd(*Xi,*Yi));
  *Ri = _mm_add_pd(_mm_mul_pd(*Xr,*Yi),_mm_mul_pd(*Xi,*Yr));
}

static inline complexe_float_t conjugue_float(const complexe_float_t c1) {
  complexe_float_t r;

  r.real = c1.real;
  r.imaginary = -c1.imaginary;

  return r;
}

static inline void conjugue_float_v(__m128 *Xr, __m128 *Xi, __m128 *Rr, __m128 *Ri) {

  __m128 coeff = _mm_set1_ps(-1.0);
  *Rr = *Xr;
  *Ri = _mm_mul_ps(*Xi,coeff);
}

static inline complexe_double_t conjugue_double(const complexe_double_t c1) {
  complexe_double_t r;

  r.real = c1.real;
  r.imaginary = -c1.imaginary;

  return r;
}

static inline void conjugue_double_v(__m128d *Xr, __m128d *Xi, __m128d *Rr, __m128d *Ri) {

  __m128d coeff = _mm_set1_pd(-1.0);
  *Rr = *Xr;
  *Ri = _mm_mul_pd(*Xi,coeff);
}

#endif
