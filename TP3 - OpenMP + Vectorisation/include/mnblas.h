#include <omp.h>
#include <smmintrin.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <x86intrin.h>
#define CBLAS_INDEX int  /* this may vary between platforms */

typedef enum {MNCblasRowMajor=101, MNCblasColMajor=102} MNCBLAS_LAYOUT;
typedef enum {MNCblasNoTrans=111, MNCblasTrans=112, MNCblasConjTrans=113} MNCBLAS_TRANSPOSE;
typedef enum {MNCblasUpper=121, MNCblasLower=122} MNCBLAS_UPLO;
typedef enum {MNCblasNonUnit=131, MNCblasUnit=132} MNCBLAS_DIAG;
typedef enum {MNCblasLeft=141, MNCblasRight=142} MNCBLAS_SIDE;

/*
 * ===========================================================================
 * Prototypes for level 1 BLAS functions
 * ===========================================================================
 * 
 *  Les fonctions sans suffixes sont les fonctions "normales", celles avec un
 *  suffixe p sont parallélisées avec OpenMP, celles avec un suffixe v sont
 *  vectorisées, et enfin celles avec un suffixe vp sont à la fois vectorisées
 *  et parallélisées
 */


/*
  BLAS copy
*/

/* ---------------------------- scopy ---------------------------*/
void mncblas_scopy(const int N, const float *X, const int incX,
                 float *Y, const int incY);

void mncblas_scopy_p(const int N, const float *X, const int incX,
                 float *Y, const int incY);                

void mncblas_scopy_v(const int N, const float *X, const int incX,
                 float *Y, const int incY);

void mncblas_scopy_vp(const int N, const float *X, const int incX,
                 float *Y, const int incY);     

/* -------------------------- dcopy ----------------------------*/
void mncblas_dcopy(const int N, const double *X, const int incX,
                 double *Y, const int incY);

void mncblas_dcopy_p(const int N, const double *X, const int incX,
                 double *Y, const int incY);

void mncblas_dcopy_v(const int N, const double *X, const int incX,
                 double *Y, const int incY);

void mncblas_dcopy_vp(const int N, const double *X, const int incX,
                 double *Y, const int incY);

/* -------------------------- ccopy ----------------------------*/
void mncblas_ccopy(const int N, const void *X, const int incX,
                 void *Y, const int incY);

void mncblas_ccopy_p(const int N, const void *X, const int incX,
                 void *Y, const int incY);

void mncblas_ccopy_v(const int N, const void *X, const int incX,
                 void *Y, const int incY);

void mncblas_ccopy_vp(const int N, const void *X, const int incX,
                 void *Y, const int incY);

/* -------------------------- zcopy ----------------------------*/
void mncblas_zcopy(const int N, const void *X, const int incX,
                 void *Y, const int incY);

void mncblas_zcopy_p(const int N, const void *X, const int incX,
                 void *Y, const int incY);

void mncblas_zcopy_v(const int N, const void *X, const int incX,
                 void *Y, const int incY);

void mncblas_zcopy_vp(const int N, const void *X, const int incX,
                 void *Y, const int incY);

/*
  end COPY BLAS
*/


/*

  BLAS DOT

*/

/* ---------------------------- sdot ---------------------------*/
float  mncblas_sdot(const int N, const float  *X, const int incX,
                  const float  *Y, const int incY);

float  mncblas_sdot_p(const int N, const float  *X, const int incX,
                  const float  *Y, const int incY);

float  mncblas_sdot_v(const int N, const float  *X, const int incX,
                  const float  *Y, const int incY);

float  mncblas_sdot_vp(const int N, const float  *X, const int incX,
                  const float  *Y, const int incY);     

/* ---------------------------- ddot ---------------------------*/
double mncblas_ddot(const int N, const double *X, const int incX,
                  const double *Y, const int incY);

double mncblas_ddot_p(const int N, const double *X, const int incX,
                  const double *Y, const int incY);

double mncblas_ddot_v(const int N, const double *X, const int incX,
                  const double *Y, const int incY);

double mncblas_ddot_vp(const int N, const double *X, const int incX,
                  const double *Y, const int incY);

/* ---------------------------- cdotu ---------------------------*/
void   mncblas_cdotu_sub(const int N, const void *X, const int incX,
                       const void *Y, const int incY, void *dotu);

void   mncblas_cdotu_sub_p(const int N, const void *X, const int incX,
                       const void *Y, const int incY, void *dotu);

void   mncblas_cdotu_sub_v(const int N, const void *X, const int incX,
                       const void *Y, const int incY, void *dotu);

void   mncblas_cdotu_sub_vp(const int N, const void *X, const int incX,
                       const void *Y, const int incY, void *dotu);                       

/* ---------------------------- cdotc ---------------------------*/
void   mncblas_cdotc_sub(const int N, const void *X, const int incX,
                       const void *Y, const int incY, void *dotc);

void   mncblas_cdotc_sub_p(const int N, const void *X, const int incX,
                       const void *Y, const int incY, void *dotc);

void   mncblas_cdotc_sub_v(const int N, const void *X, const int incX,
                       const void *Y, const int incY, void *dotc);

void   mncblas_cdotc_sub_vp(const int N, const void *X, const int incX,
                       const void *Y, const int incY, void *dotc);

/* ---------------------------- zdotu ---------------------------*/
void   mncblas_zdotu_sub(const int N, const void *X, const int incX,
                       const void *Y, const int incY, void *dotu);

void   mncblas_zdotu_sub_p(const int N, const void *X, const int incX,
                       const void *Y, const int incY, void *dotu);

void   mncblas_zdotu_sub_v(const int N, const void *X, const int incX,
                       const void *Y, const int incY, void *dotu);

void   mncblas_zdotu_sub_vp(const int N, const void *X, const int incX,
                       const void *Y, const int incY, void *dotu);

/* ---------------------------- zdtoc ---------------------------*/
void   mncblas_zdotc_sub(const int N, const void *X, const int incX,
                       const void *Y, const int incY, void *dotc);

void   mncblas_zdotc_sub_p(const int N, const void *X, const int incX,
                       const void *Y, const int incY, void *dotc);

void   mncblas_zdotc_sub_v(const int N, const void *X, const int incX,
                       const void *Y, const int incY, void *dotc);

void   mncblas_zdotc_sub_vp(const int N, const void *X, const int incX,
                       const void *Y, const int incY, void *dotc);

/*
  END BLAS DOT
*/

/*
  BLAS AXPY
*/

/* ---------------------------- saxpy ---------------------------*/
void mnblas_saxpy(const int N, const float alpha, const float *X,
                 const int incX, float *Y, const int incY);

void mnblas_saxpy_p(const int N, const float alpha, const float *X,
                 const int incX, float *Y, const int incY);

void mnblas_saxpy_v(const int N, const float alpha, const float *X,
                 const int incX, float *Y, const int incY);

void mnblas_saxpy_vp(const int N, const float alpha, const float *X,
                 const int incX, float *Y, const int incY);

/* ---------------------------- daxpy ---------------------------*/
void mnblas_daxpy(const int N, const double alpha, const double *X,
                 const int incX, double *Y, const int incY);

void mnblas_daxpy_p(const int N, const double alpha, const double *X,
                 const int incX, double *Y, const int incY);

void mnblas_daxpy_v(const int N, const double alpha, const double *X,
                 const int incX, double *Y, const int incY);

void mnblas_daxpy_vp(const int N, const double alpha, const double *X,
                 const int incX, double *Y, const int incY);

/* ---------------------------- caxpy ---------------------------*/
void mnblas_caxpy(const int N, const void *alpha, const void *X,
                 const int incX, void *Y, const int incY);

void mnblas_caxpy_p(const int N, const void *alpha, const void *X,
                 const int incX, void *Y, const int incY);

void mnblas_caxpy_v(const int N, const void *alpha, const void *X,
                 const int incX, void *Y, const int incY);

void mnblas_caxpy_vp(const int N, const void *alpha, const void *X,
                 const int incX, void *Y, const int incY);

/* ---------------------------- zaxpy ---------------------------*/
void mnblas_zaxpy(const int N, const void *alpha, const void *X,
                 const int incX, void *Y, const int incY);

void mnblas_zaxpy_p(const int N, const void *alpha, const void *X,
                 const int incX, void *Y, const int incY);

void mnblas_zaxpy_v(const int N, const void *alpha, const void *X,
                 const int incX, void *Y, const int incY);

void mnblas_zaxpy_vp(const int N, const void *alpha, const void *X,
                 const int incX, void *Y, const int incY);

/*
 END BLAS AXPY
*/


/*
 * ===========================================================================
 * Prototypes for level 2 BLAS
 * ===========================================================================
 */


/* ---------------------------- sgemv ---------------------------*/
void mncblas_sgemv(const MNCBLAS_LAYOUT layout,
                 const MNCBLAS_TRANSPOSE TransA, const int M, const int N,
                 const float alpha, const float *A, const int lda,
                 const float *X, const int incX, const float beta,
                 float *Y, const int incY);

void mncblas_sgemv_p(const MNCBLAS_LAYOUT layout,
                 const MNCBLAS_TRANSPOSE TransA, const int M, const int N,
                 const float alpha, const float *A, const int lda,
                 const float *X, const int incX, const float beta,
                 float *Y, const int incY);

void mncblas_sgemv_v(const MNCBLAS_LAYOUT layout,
                 const MNCBLAS_TRANSPOSE TransA, const int M, const int N,
                 const float alpha, const float *A, const int lda,
                 const float *X, const int incX, const float beta,
                 float *Y, const int incY);

void mncblas_sgemv_vp(const MNCBLAS_LAYOUT layout,
                 const MNCBLAS_TRANSPOSE TransA, const int M, const int N,
                 const float alpha, const float *A, const int lda,
                 const float *X, const int incX, const float beta,
                 float *Y, const int incY);

/* ---------------------------- dgemv ---------------------------*/
void mncblas_dgemv(MNCBLAS_LAYOUT layout,
                 MNCBLAS_TRANSPOSE TransA, const int M, const int N,
                 const double alpha, const double *A, const int lda,
                 const double *X, const int incX, const double beta,
                 double *Y, const int incY);

void mncblas_dgemv_p(MNCBLAS_LAYOUT layout,
                 MNCBLAS_TRANSPOSE TransA, const int M, const int N,
                 const double alpha, const double *A, const int lda,
                 const double *X, const int incX, const double beta,
                 double *Y, const int incY);

void mncblas_dgemv_v(MNCBLAS_LAYOUT layout,
                 MNCBLAS_TRANSPOSE TransA, const int M, const int N,
                 const double alpha, const double *A, const int lda,
                 const double *X, const int incX, const double beta,
                 double *Y, const int incY);

void mncblas_dgemv_vp(MNCBLAS_LAYOUT layout,
                 MNCBLAS_TRANSPOSE TransA, const int M, const int N,
                 const double alpha, const double *A, const int lda,
                 const double *X, const int incX, const double beta,
                 double *Y, const int incY);

/* ---------------------------- cgemv ---------------------------*/
void mncblas_cgemv(MNCBLAS_LAYOUT layout,
                 MNCBLAS_TRANSPOSE TransA, const int M, const int N,
                 const void *alpha, const void *A, const int lda,
                 const void *X, const int incX, const void *beta,
                 void *Y, const int incY);

void mncblas_cgemv_p(MNCBLAS_LAYOUT layout,
                 MNCBLAS_TRANSPOSE TransA, const int M, const int N,
                 const void *alpha, const void *A, const int lda,
                 const void *X, const int incX, const void *beta,
                 void *Y, const int incY);

void mncblas_cgemv_v(MNCBLAS_LAYOUT layout,
                 MNCBLAS_TRANSPOSE TransA, const int M, const int N,
                 const void *alpha, const void *A, const int lda,
                 const void *X, const int incX, const void *beta,
                 void *Y, const int incY);

void mncblas_cgemv_vp(MNCBLAS_LAYOUT layout,
                 MNCBLAS_TRANSPOSE TransA, const int M, const int N,
                 const void *alpha, const void *A, const int lda,
                 const void *X, const int incX, const void *beta,
                 void *Y, const int incY);                

/* ---------------------------- zgemv ---------------------------*/
void mncblas_zgemv(MNCBLAS_LAYOUT layout,
                 MNCBLAS_TRANSPOSE TransA, const int M, const int N,
                 const void *alpha, const void *A, const int lda,
                 const void *X, const int incX, const void *beta,
                 void *Y, const int incY);

void mncblas_zgemv_p(MNCBLAS_LAYOUT layout,
                 MNCBLAS_TRANSPOSE TransA, const int M, const int N,
                 const void *alpha, const void *A, const int lda,
                 const void *X, const int incX, const void *beta,
                 void *Y, const int incY);

void mncblas_zgemv_v(MNCBLAS_LAYOUT layout,
                 MNCBLAS_TRANSPOSE TransA, const int M, const int N,
                 const void *alpha, const void *A, const int lda,
                 const void *X, const int incX, const void *beta,
                 void *Y, const int incY);

void mncblas_zgemv_vp(MNCBLAS_LAYOUT layout,
                 MNCBLAS_TRANSPOSE TransA, const int M, const int N,
                 const void *alpha, const void *A, const int lda,
                 const void *X, const int incX, const void *beta,
                 void *Y, const int incY);

/*
 * ===========================================================================
 * Prototypes for level 3 BLAS
 * ===========================================================================
 */

/* ---------------------------- sgemm ---------------------------*/
void mncblas_sgemm(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const float alpha, const float *A,
                 const int lda, const float *B, const int ldb,
                 const float beta, float *C, const int ldc);

void mncblas_sgemm_p(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const float alpha, const float *A,
                 const int lda, const float *B, const int ldb,
                 const float beta, float *C, const int ldc);

void mncblas_sgemm_v(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const float alpha, const float *A,
                 const int lda, const float *B, const int ldb,
                 const float beta, float *C, const int ldc);

void mncblas_sgemm_vp(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const float alpha, const float *A,
                 const int lda, const float *B, const int ldb,
                 const float beta, float *C, const int ldc);

/* ---------------------------- dgemm ---------------------------*/
void mncblas_dgemm(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const double alpha, const double *A,
                 const int lda, const double *B, const int ldb,
                 const double beta, double *C, const int ldc);

void mncblas_dgemm_p(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const double alpha, const double *A,
                 const int lda, const double *B, const int ldb,
                 const double beta, double *C, const int ldc);

void mncblas_dgemm_v(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const double alpha, const double *A,
                 const int lda, const double *B, const int ldb,
                 const double beta, double *C, const int ldc);

void mncblas_dgemm_vp(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const double alpha, const double *A,
                 const int lda, const double *B, const int ldb,
                 const double beta, double *C, const int ldc);

/* ---------------------------- cgemm ---------------------------*/
void mncblas_cgemm(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const void *alpha, const void *A,
                 const int lda, const void *B, const int ldb,
                 const void *beta, void *C, const int ldc);

void mncblas_cgemm_p(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const void *alpha, const void *A,
                 const int lda, const void *B, const int ldb,
                 const void *beta, void *C, const int ldc);

void mncblas_cgemm_v(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const void *alpha, const void *A,
                 const int lda, const void *B, const int ldb,
                 const void *beta, void *C, const int ldc);

void mncblas_cgemm_vp(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const void *alpha, const void *A,
                 const int lda, const void *B, const int ldb,
                 const void *beta, void *C, const int ldc);

/* ---------------------------- zgemm ---------------------------*/
void mncblas_zgemm(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const void *alpha, const void *A,
                 const int lda, const void *B, const int ldb,
                 const void *beta, void *C, const int ldc);

void mncblas_zgemm_p(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const void *alpha, const void *A,
                 const int lda, const void *B, const int ldb,
                 const void *beta, void *C, const int ldc);

void mncblas_zgemm_v(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const void *alpha, const void *A,
                 const int lda, const void *B, const int ldb,
                 const void *beta, void *C, const int ldc);

void mncblas_zgemm_vp(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const void *alpha, const void *A,
                 const int lda, const void *B, const int ldb,
                 const void *beta, void *C, const int ldc);

