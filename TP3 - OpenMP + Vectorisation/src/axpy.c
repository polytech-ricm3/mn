#include "../include/complexe.h"
#include "../include/mnblas.h"

/*-----------------------saxpy---------------------------------*/

//Normal
void mnblas_saxpy(const int N, const float alpha, const float *X,
                  const int incX, float *Y, const int incY)
{
    if(N <= 0)
        return;
    register unsigned int i = 0;
    register unsigned int j = 0;
    for(; i < N && j < N; i+= incX, j += incY)
    {
        Y[j] += X[i] * alpha;
    }
}

//Parallélisé
void mnblas_saxpy_p(const int N, const float alpha, const float *X, const int incX, float *Y, const int incY)
{
  if(N <= 0)
  {
    return;
  }
  register unsigned int i = 0;

  #pragma omp parallel for schedule(static)
  for(i = 0; i < N; i+= incX)
  {
    Y[i] += X[i] * alpha;
  }
}

//Vectorisé
void mnblas_saxpy_v(const int N, const float alpha, const float *X,
                       const int incX, float *Y, const int incY) {
  register unsigned int i;
  __m128 x, y, alpha_m = _mm_set1_ps(alpha), tmp;

  for (i = 0; i < N; i += 4) {
    x = _mm_load_ps(X + i);
    y = _mm_load_ps(Y + i);
    tmp = _mm_mul_ps(x, alpha_m);
    tmp = _mm_add_ps(tmp, y);

    _mm_store_ps((Y + i), tmp);
  }
}

//Parallélisé + Vectorisé
void mnblas_saxpy_vp(const int N, const float alpha, const float *X,
                       const int incX, float *Y, const int incY) {
  register unsigned int i;
  __m128 x, y, alpha_m = _mm_set1_ps(alpha), tmp;

#pragma omp parallel for schedule (static)
  for (i = 0; i < N; i += 4) {
    x = _mm_load_ps(X + i);
    y = _mm_load_ps(Y + i);
    tmp = _mm_mul_ps(x, alpha_m);
    tmp = _mm_add_ps(tmp, y);

    _mm_store_ps((Y + i), tmp);
  }
}

/*-----------------------daxpy---------------------------------*/

//Normal
void mnblas_daxpy(const int N, const double alpha, const double *X,
                  const int incX, double *Y, const int incY)
{
    if(N <= 0)
        return;
    register unsigned int i = 0;
    register unsigned int j = 0;
    for(; i < N && j < N; i+= incX, j += incY)
    {
        Y[j] += X[i] * alpha;
    }
}

//Parallélisé
void mnblas_daxpy_p(const int N, const double alpha, const double *X, const int incX, double *Y, const int incY)
{
  if(N <= 0)
  {
    return;
  }
  register unsigned int i = 0;
  #pragma omp parallel for schedule(static)
  for(i = 0; i < N; i+= incX)
  {
    Y[i] += X[i] * alpha;
  }
}

//Vectorisé
void mnblas_daxpy_v(const int N, const double alpha, const double *X,
                       const int incX, double *Y, const int incY) {
  register unsigned int i;
  __m128d x, y, alpha_m = _mm_set1_pd(alpha), tmp;

  for (i = 0; i < N; i += 2) {
    x = _mm_load_pd(X + i);
    y = _mm_load_pd(Y + i);
    tmp = _mm_mul_pd(x, alpha_m);
    tmp = _mm_add_pd(tmp, y);

    _mm_store_pd((Y + i), tmp);
  }
}

//Parallélisé + Vectorisé
void mnblas_daxpy_vp(const int N, const double alpha, const double *X,
                       const int incX, double *Y, const int incY) {
  register unsigned int i;
  __m128d x, y, alpha_m = _mm_set1_pd(alpha), tmp;

#pragma omp parallel for schedule(static)
  for (i = 0; i < N; i += 2) {
    x = _mm_load_pd(X + i);
    y = _mm_load_pd(Y + i);
    tmp = _mm_mul_pd(x, alpha_m);
    tmp = _mm_add_pd(tmp, y);

    _mm_store_pd((Y + i), tmp);
  }
}
/*-----------------------caxpy---------------------------------*/

//Normal
void mnblas_caxpy(const int N, const void *alpha, const void *X,
                 const int incX, void *Y, const int incY)
{
    register unsigned int i = 0;
    register unsigned int j = 0;
    register complexe_float_t tmp;
    complexe_float_t alpha_c = (*(complexe_float_t*)alpha);
    complexe_float_t *Xc = (complexe_float_t *)X;
    complexe_float_t *Yc = (complexe_float_t *)Y;
    
    for(; i < N && j < N; i+= incX, j += incY)
    {
        tmp =  mult_complexe_float(Xc[i],alpha_c);
        Yc[j] = add_complexe_float(Yc[j], tmp);
    }
}

//Parallélisé
void mnblas_caxpy_p(const int N, const void *alpha, const void *X,
                 const int incX, void *Y, const int incY)
{
    register unsigned int i = 0;
    register complexe_float_t tmp;
    complexe_float_t alpha_c = (*(complexe_float_t*)alpha);
    complexe_float_t *Xc = (complexe_float_t *)X;
    complexe_float_t *Yc = (complexe_float_t *)Y;
    
    #pragma omp parallel for
    for(i = 0; i < N; i+= incX)
    {
        tmp =  mult_complexe_float(Xc[i],alpha_c);
        Yc[i].real += tmp.real;
        Yc[i].imaginary += tmp.imaginary;
    }
}

//Vectorisé
void mnblas_caxpy_v(const int N, const void *alpha, const void *X,
                 const int incX, void *Y, const int incY)
{ 
  register unsigned int i;
  complexe_float_t *Xc = (complexe_float_t *)X;
  complexe_float_t *Yc = (complexe_float_t *)Y;
  complexe_float_t *Alpha = (complexe_float_t *)alpha;

  __m128 Xr, Xi, Yr, Yi, alpha_r, alpha_i, tmp_real, tmp_imaginary;
  alpha_r = _mm_set1_ps(Alpha->real);
  alpha_i = _mm_set1_ps(Alpha->imaginary); 
  float tmp_f; 

  for (i = 0; i < N; i += 4) {
    Xr = _mm_set1_ps(Xc[i].real);
    Xi = _mm_set1_ps(Xc[i].imaginary);
    Yr = _mm_set1_ps(Yc[i].real);
    Yi = _mm_set1_ps(Yc[i].imaginary);
   
    mult_complexe_float_v(&Xr, &Xi, &alpha_r,
                             &alpha_i, &tmp_real, &tmp_imaginary);

    tmp_real = _mm_add_ps(tmp_real, Yr);
    tmp_imaginary = _mm_add_ps(tmp_imaginary, Yi);

    _mm_store_ps(&tmp_f, tmp_real);
    Yc[i].real = tmp_f;
    _mm_store_ps(&tmp_f, tmp_imaginary);
    Yc[i].imaginary = tmp_f;
  }
  Y = Yc;
}

//Parallélisé + Vectorisé
void mnblas_caxpy_vp(const int N, const void *alpha, const void *X,
                 const int incX, void *Y, const int incY)
{ 
  register unsigned int i;
  complexe_float_t *Xc = (complexe_float_t *)X;
  complexe_float_t *Yc = (complexe_float_t *)Y;
  complexe_float_t *Alpha = (complexe_float_t *)alpha;

  __m128 Xr, Xi, Yr, Yi, alpha_r, alpha_i, tmp_real, tmp_imaginary;
  alpha_r = _mm_set1_ps(Alpha->real);
  alpha_i = _mm_set1_ps(Alpha->imaginary); 
  float tmp_f;   
  #pragma omp parallel for private(tmp_f)
  for (i = 0; i < N; i += 4) {
    Xr = _mm_set1_ps(Xc[i].real);
    Xi = _mm_set1_ps(Xc[i].imaginary);
    Yr = _mm_set1_ps(Yc[i].real);
    Yi = _mm_set1_ps(Yc[i].imaginary);
   
    mult_complexe_float_v(&Xr, &Xi, &alpha_r,
                             &alpha_i, &tmp_real, &tmp_imaginary);

    tmp_real = _mm_add_ps(tmp_real, Yr);
    tmp_imaginary = _mm_add_ps(tmp_imaginary, Yi);

    _mm_store_ps(&tmp_f, tmp_real);
    Yc[i].real = tmp_f;
    _mm_store_ps(&tmp_f, tmp_imaginary);
    Yc[i].imaginary = tmp_f;
  }
  Y = Yc;
}
/*-----------------------zaxpy---------------------------------*/

//Normal
void mnblas_zaxpy(const int N, const void *alpha, const void *X,
                  const int incX, void *Y, const int incY)
{
    register unsigned int i = 0;
    register unsigned int j = 0;
    register complexe_double_t tmp;
    complexe_double_t alpha_c = (*(complexe_double_t*)alpha);
    complexe_double_t *Xc = (complexe_double_t *)X;
    complexe_double_t *Yc = (complexe_double_t *)Y;

    for(; i < N && j < N; i+= incX, j += incY)
    {
      tmp =  mult_complexe_double(Xc[i],alpha_c);
      Yc[j] = add_complexe_double(Yc[j], tmp);
    }
}

//Parallélisé
void mnblas_zaxpy_p(const int N, const void *alpha, const void *X,
                  const int incX, void *Y, const int incY)
{
    register unsigned int i = 0;
    register complexe_double_t tmp;
    complexe_double_t alpha_c = (*(complexe_double_t*)alpha);
    complexe_double_t *Xc = (complexe_double_t *)X;
    complexe_double_t *Yc = (complexe_double_t *)Y;

    #pragma omp parallel for schedule(static)
    for(i = 0; i < N; i+= incX)
    {
      tmp =  mult_complexe_double(Xc[i],alpha_c);
      Yc[i].real += tmp.real;
      Yc[i].imaginary += tmp.imaginary;
    }
}

//Vectorisé
void mnblas_zaxpy_v(const int N, const void *alpha, const void *X,
                  const int incX, void *Y, const int incY)
{ 
  register unsigned int i;
  complexe_double_t *Xc = (complexe_double_t *)X;
  complexe_double_t *Yc = (complexe_double_t *)Y;
  complexe_double_t *Alpha = (complexe_double_t *)alpha;

  __m128d Xr, Xi, Yr, Yi, alpha_r, alpha_i, tmp_real, tmp_imaginary;
  alpha_r = _mm_set1_pd(Alpha->real);
  alpha_i = _mm_set1_pd(Alpha->imaginary);  
  double tmp_d; 
  for (i = 0; i < N; i += 2) {
    Xr = _mm_set1_pd(Xc[i].real);
    Xi = _mm_set1_pd(Xc[i].imaginary);
    Yr = _mm_set1_pd(Yc[i].real);
    Yi = _mm_set1_pd(Yc[i].imaginary);
   
    mult_complexe_double_v(&Xr, &Xi, &alpha_r,
                             &alpha_i, &tmp_real, &tmp_imaginary);

    tmp_real = _mm_add_pd(tmp_real, Yr);
    tmp_imaginary = _mm_add_pd(tmp_imaginary, Yi);

    _mm_store_pd(&tmp_d, tmp_real);
    Yc[i].real = tmp_d;
    _mm_store_pd(&tmp_d, tmp_imaginary);
    Yc[i].imaginary = tmp_d;
  }
  Y = (void*)Yc;
}

//Prallélisé + Vectorisé
void mnblas_zaxpy_vp(const int N, const void *alpha, const void *X,
                  const int incX, void *Y, const int incY)
{ 
  register unsigned int i;
  complexe_double_t *Xc = (complexe_double_t *)X;
  complexe_double_t *Yc = (complexe_double_t *)Y;
  complexe_double_t *Alpha = (complexe_double_t *)alpha;

  __m128d Xr, Xi, Yr, Yi, alpha_r, alpha_i, tmp_real, tmp_imaginary;
  alpha_r = _mm_set1_pd(Alpha->real);
  alpha_i = _mm_set1_pd(Alpha->imaginary);  
  double tmp_d;
  #pragma omp parallel for schedule(static) private(tmp_d)
  for (i = 0; i < N; i += 2) {
    Xr = _mm_set1_pd(Xc[i].real);
    Xi = _mm_set1_pd(Xc[i].imaginary);
    Yr = _mm_set1_pd(Yc[i].real);
    Yi = _mm_set1_pd(Yc[i].imaginary);
   
    mult_complexe_double_v(&Xr, &Xi, &alpha_r,
                             &alpha_i, &tmp_real, &tmp_imaginary);

    tmp_real = _mm_add_pd(tmp_real, Yr);
    tmp_imaginary = _mm_add_pd(tmp_imaginary, Yi);

    _mm_store_pd(&tmp_d, tmp_real);
    Yc[i].real = tmp_d;
    _mm_store_pd(&tmp_d, tmp_imaginary);
    Yc[i].imaginary = tmp_d;
  }
  Y = (void*)Yc;
}