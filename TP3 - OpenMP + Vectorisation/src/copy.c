#include "../include/mnblas.h"
#include "../include/complexe.h"


/* -------------------  scopy -----------------------*/

//Normal
void mncblas_scopy(const int N, const float *X, const int incX, float *Y, const int incY)
{
  register unsigned int i = 0 ;
  register unsigned int j = 0 ;

  for (; ((i < N) && (j < N)) ; i += incX, j += incY)
  {
    Y [j] = X [i] ;
  }
}

//Parallélisé
void mncblas_scopy_p(const int N, const float *X, const int incX, float *Y, const int incY)
{
  if(N <= 0)
  {
    return;
  }
  register unsigned int i = 0 ;

  #pragma omp parallel for schedule(static)
  for (i = 0; i < N; i += incX)
  {
    Y [i] = X [i] ;
  }
}

//Vectorisé
void mncblas_scopy_v(const int N, const float *X, const int incX, float *Y, const int incY)
{
  if(N <= 0)
  {
    return;
  }
  register unsigned int i = 0;
  __m128 x;

  for(i=0; i < N; i += 4)
  {
    x = _mm_load_ps(X + i);
    _mm_store_ps((Y + i), x);
  }
}

// Parallélisé + Vectorisé
void mncblas_scopy_vp(const int N, const float *X, const int incX, float *Y, const int incY)
{
  if(N <= 0)
  {
    return;
  }
  register unsigned int i = 0;
  __m128 x;

  #pragma omp parallel for schedule(static) private(x)
  for(i=0; i < N; i += 4)
  {
    x = _mm_load_ps(X + i);
    _mm_store_ps((Y + i), x);
  }
}

/* -------------------  dcopy -----------------------*/

//Normal
void mncblas_dcopy(const int N, const double *X, const int incX, double *Y, const int incY)
{
  if(N <= 0) 
    return;
  register unsigned int i = 0;
  register unsigned int j = 0;
  for(; ((i < N) && (j < N)); i += incX, j += incY)
  {
    Y[j] = X[i];
  }
}

//Parallélisé
void mncblas_dcopy_p(const int N, const double *X, const int incX, double *Y, const int incY)
{
  if(N <= 0)
  {
    return;
  }
  register unsigned int i = 0;
  
  #pragma omp parallel for schedule(static)
  for(i=0; i < N; i += incX)
  {
    Y[i] = X[i];
  }
}

//Vectorisé
void mncblas_dcopy_v(const int N, const double *X, const int incX, double *Y, const int incY)
{
  if(N <= 0)
  {
    return;
  }
  register unsigned int i = 0;
  __m128d x;

  for(i=0; i < N; i += 2)
  {
    x = _mm_load_pd(X + i);
    _mm_store_pd((Y + i), x);
  }
}

// Parallélisé + Vectorisé
void mncblas_dcopy_vp(const int N, const double *X, const int incX, double *Y, const int incY)
{
  if(N <= 0)
  {
    return;
  }
  register unsigned int i = 0;
  __m128d x;

  #pragma omp parallel for schedule(static) private(x)
  for(i=0; i < N; i += 2)
  {
    x = _mm_load_pd(X + i);
    _mm_store_pd((Y + i), x);
  }
}

/* -------------------  ccopy -----------------------*/

//Normal
void mncblas_ccopy(const int N, const void *X, const int incX, void *Y, const int incY)
{
  if(N <= 0) 
    return;
  register unsigned int i = 0;
  register unsigned int j = 0;
  for(; ((i < N) && (j < N)); i += incX, j += incY)
  {
    ((complexe_float_t *)Y) [j] = ((complexe_float_t *)X)[i];
  }
}

//Parallélisé
void mncblas_ccopy_p(const int N, const void *X, const int incX, void *Y, const int incY)
{
  if(N <= 0)
  {
    return;
  }
  register unsigned int i = 0;

  #pragma omp parallel for schedule(static)
  for(i = 0; i < N; i += incX)
  {
    ((complexe_float_t *)Y) [i] = ((complexe_float_t *)X)[i];
  }
}

//Vectorisé
void mncblas_ccopy_v(const int N, const void *X, const int incX, void *Y, const int incY)
{
  if(N <= 0)
  {
    return;
  }
  register unsigned int i = 0;
  __m128 x_r, x_i;

  complexe_float_t *X_f = (complexe_float_t *)X;
  complexe_float_t *Y_f = (complexe_float_t *)Y; 
  float tmp;

  for(i=0; i < N; i += 4)
  {
    //((complexe_float_t *)Y) [i] = ((complexe_float_t *)X)[i];
    x_r = _mm_set1_ps(X_f[i].real);
    x_i = _mm_set1_ps(X_f[i].imaginary);
    _mm_store_ps(&tmp,x_r);
    Y_f[i].real = tmp;
    _mm_store_ps(&tmp,x_i);
    Y_f[i].imaginary = tmp;
  }
}

// Parallélisé + Vectorisé 
void mncblas_ccopy_vp(const int N, const void *X, const int incX, void *Y, const int incY)
{
  if(N <= 0)
  {
    return;
  }
  register unsigned int i = 0;
  __m128 x_r, x_i;
  float tmp;

  complexe_float_t *X_f = (complexe_float_t *)X;
  complexe_float_t *Y_f = (complexe_float_t *)Y; 


  #pragma omp parallel for schedule(static) private(x_r,x_i,tmp)
  for(i=0; i < N; i += 4)
  {
    //((complexe_double_t *)Y) [i] = ((complexe_double_t *)X)[i];
     x_r = _mm_set1_ps(X_f[i].real);
    x_i = _mm_set1_ps(X_f[i].imaginary);
    _mm_store_ps(&tmp,x_r);
    Y_f[i].real = tmp;
    _mm_store_ps(&tmp,x_i);
    Y_f[i].imaginary = tmp;
  }
}



/* -------------------  zcopy -----------------------*/


//Normal
void mncblas_zcopy(const int N, const void *X, const int incX, void *Y, const int incY)
{
  if(N <= 0) 
    return;
  register unsigned int i = 0;
  register unsigned int j = 0;
  for(; ((i < N) && (j < N)); i += incX, j += incY)
  {
    ((complexe_double_t *)Y) [j] = ((complexe_double_t *)X)[i];
  }
}

//Parallélisé
void mncblas_zcopy_p(const int N, const void *X, const int incX, void *Y, const int incY)
{
  if(N <= 0)
  {
    return;
  }
  register unsigned int i = 0;

  #pragma omp parallel for schedule(static)
  for(i=0; i < N; i += incX)
  {
    ((complexe_double_t *)Y) [i] = ((complexe_double_t *)X)[i];
  }
}

//Vectorisé
void mncblas_zcopy_v(const int N, const void *X, const int incX, void *Y, const int incY)
{
  if(N <= 0)
  {
    return;
  }
  register unsigned int i = 0;
  __m128d x_r, x_i;

  complexe_double_t *X_d = (complexe_double_t *)X;
  complexe_double_t *Y_d = (complexe_double_t *)Y; 
  double tmp;

  for(i=0; i < N; i += 2)
  {
    //((complexe_double_t *)Y) [i] = ((complexe_double_t *)X)[i];
    x_r = _mm_set1_pd(X_d[i].real);
    x_i = _mm_set1_pd(X_d[i].imaginary);
    _mm_store_pd(&tmp,x_r);
    Y_d[i].real = tmp;
    _mm_store_pd(&tmp,x_i);
    Y_d[i].imaginary = tmp;
  }
}

// Parallélisé + Vectorisé 
void mncblas_zcopy_vp(const int N, const void *X, const int incX, void *Y, const int incY)
{
  if(N <= 0)
  {
    return;
  }
  register unsigned int i = 0;
  __m128d x_r, x_i;

  complexe_double_t *X_d = (complexe_double_t *)X;
  complexe_double_t *Y_d = (complexe_double_t *)Y; 
  double tmp;

  #pragma omp parallel for schedule(static) private(x_r,x_i,tmp)
  for(i=0; i < N; i += 2)
  {
    //((complexe_double_t *)Y) [i] = ((complexe_double_t *)X)[i];
    x_r = _mm_set1_pd(X_d[i].real);
    x_i = _mm_set1_pd(X_d[i].imaginary);
    _mm_store_pd(&tmp,x_r);
    Y_d[i].real = tmp;
    _mm_store_pd(&tmp,x_i);
    Y_d[i].imaginary = tmp;
  }
}