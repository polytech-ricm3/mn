#include "../include/mnblas.h"
#include "../include/complexe.h"

/*-------------------------sdot---------------------------*/

//Normal
float mncblas_sdot(const int N, const float *X, const int incX, const float *Y, const int incY)
{
  register unsigned int i = 0 ;
  register unsigned int j = 0 ;
  register float dot = 0.0 ;

  for (; ((i < N) && (j < N)) ; i += incX, j+=incY)
  {
    dot = dot + X [i] * Y [j] ;
  }

  return dot ;
}

//Parallélisé
float mncblas_sdot_p(const int N, const float *X, const int incX, const float *Y, const int incY)
{
  register unsigned int i = 0;
  register float dot = 0.0;

  #pragma omp parallel for schedule(static) reduction(+:dot)
  for (i = 0; i < N; i += incX)
  {
    dot += X[i] * Y[i];
  }

  return dot;
}

//Vectorisé
float mncblas_sdot_v(const int N, const float *X, const int incX, const float *Y, const int incY)
{
  __m128 v1, v2, res;
  register unsigned int i ;
  float dot [4] __attribute__ ((aligned (8))) ;
  float dot_total = 0.0 ;

    for (i = 0; i < N; i = i + 4)
    {
      v1 = _mm_load_ps (X+i) ;
      v2 = _mm_load_ps (Y+i) ;

      res = _mm_dp_ps (v1, v2, 0xFF);

      _mm_store_ps (dot, res) ;

      dot_total += dot [0] ;
    }

    return dot_total;
}

//Parallélisé + Vectorisé
float mncblas_sdot_vp(const int N, const float *X, const int incX, const float *Y, const int incY)
{
  __m128 v1, v2, res ;
  register unsigned int i ;
  float dot [4] __attribute__ ((aligned (8))) ;
  float dot_total = 0.0 ;
  #pragma omp parallel for schedule(static) reduction(+ : dot_total) private(v1, v2, res, dot)
  for (i = 0; i < N; i = i + 4)
  {
    v1 = _mm_load_ps (X+i) ;
    v2 = _mm_load_ps (Y+i) ;
    res = _mm_dp_ps (v1, v2, 0xFF) ;
    _mm_store_ps (dot, res) ;
    dot_total += dot [0] ;
  }
  return dot_total;
}



/*--------------------------ddot---------------------------------*/

//Normal
double mncblas_ddot(const int N, const double *X, const int incX, const double *Y, const int incY)
{
  register unsigned int i = 0 ;
  register unsigned int j = 0 ;
  register double dot = 0.0 ;

  for (; ((i < N) && (j < N)) ; i += incX, j+=incY)
  {
    dot = dot + X [i] * Y [j] ;
  }

  return dot ;
}

//Parallélisé
double mncblas_ddot_p(const int N, const double *X, const int incX, const double *Y, const int incY)
{
  register unsigned int i = 0;
  register double dot = 0.0;

  #pragma omp parallel for schedule(static) reduction(+:dot)
  for (i = 0; i < N; i += incX)
  {
    dot += X[i] * Y[i];
  }

  return dot;
}

//Vectorisé
double mncblas_ddot_v(const int N, const double *X, const int incX, const double *Y, const int incY)
{
 __m128d v1, v2, res ;
  register unsigned int i ;
  double dot [2] __attribute__ ((aligned (16))) ;
  double dot_total = 0.0 ;

    for (i = 0; i < N; i = i + 2)
    {
      v1 = _mm_load_pd (X+i) ;
      v2 = _mm_load_pd (Y+i) ;

      res = _mm_dp_pd (v1, v2, 0xFF) ;

      _mm_store_pd (dot, res) ;

      dot_total += dot[0] ;
    }

    return dot_total ;
}

//Parallélisé + Vectorisé
double mncblas_ddot_vp(const int N, const double *X, const int incX, const double *Y, const int incY)
{
 __m128d v1, v2, res ;
  register unsigned int i ;
  double dot [2] __attribute__ ((aligned (16))) ;
  double dot_total = 0.0 ;

  #pragma omp parallel for schedule(static) reduction(+ : dot_total) private(v1, v2, res, dot)
  for (i = 0; i < N; i = i + 2)
  {
    v1 = _mm_load_pd (X+i) ;
    v2 = _mm_load_pd (Y+i) ;
    res = _mm_dp_pd (v1, v2, 0xFF) ;
    _mm_store_pd (dot, res) ;
    dot_total += dot [0] ;
  }
  return dot_total ;
}

/*-----------------------cdotu_sub--------------------------*/

//Normal
void mncblas_cdotu_sub(const int N, const void *X, const int incX, const void *Y, const int incY, void *dotu)
{
  register unsigned int i = 0 ;
  register unsigned int j = 0 ;
  register complexe_float_t tmp;

  for (; ((i < N) && (j < N)) ; i += incX, j+=incY)
  {
    tmp = mult_complexe_float (((complexe_float_t*)X)[i],((complexe_float_t*)Y)[j]);
    *(complexe_float_t *)dotu = add_complexe_float(*(complexe_float_t *)dotu, tmp) ;
  }

}

//Parallélisé
void mncblas_cdotu_sub_p(const int N, const void *X, const int incX, const void *Y, const int incY, void *dotu)
{
  register unsigned int i = 0;
  register complexe_float_t tmp;
  register float im = 0.0, re = 0.0;

  #pragma omp parallel for schedule(static) private(tmp) reduction(+:re) reduction(+:im)
  for (i = 0; i < N; i += incX)
  {
    tmp = mult_complexe_float(((complexe_float_t *)X)[i],((complexe_float_t *)Y)[i]);
    re += tmp.real;
    im += tmp.imaginary;
  }
  (*(complexe_float_t*)dotu).real = re;
  (*(complexe_float_t*)dotu).imaginary = im;
}

//Vectorisé
void mncblas_cdotu_sub_v(const int N, const void *X, const int incX, const void *Y, const int incY, void *dotu)
{
__m128 v1_r, v1_i, v2_r, v2_i, res_r, res_i, dotu_r, dotu_i;
  register unsigned int i ;
  complexe_float_t *x = (complexe_float_t *)X;
  complexe_float_t *y = (complexe_float_t *)Y;
  complexe_float_t Dotu = (*(complexe_float_t *)dotu);
  Dotu.real = 0.0;
  Dotu.imaginary = 0.0;
  dotu_r =  _mm_set1_ps(Dotu.real);
  dotu_i =  _mm_set1_ps(Dotu.imaginary);
  
  for (i = 0; i < N; i = i + 4)
  {
    v1_r = _mm_load_ps (&(x[i].real)) ;
    v1_i = _mm_load_ps (&(x[i].imaginary)) ;
    v2_r = _mm_load_ps (&(y[i].real)) ;
    v2_i = _mm_load_ps (&(y[i].imaginary)) ;
    mult_complexe_float_v(&v1_r, &v1_i, &v2_r, &v2_i, &res_r, &res_i);

    dotu_r = _mm_add_ps(dotu_r, res_r);
    dotu_i = _mm_add_ps(dotu_i, res_i);
  }
  _mm_store_ps(&(Dotu.real), dotu_r);
  _mm_store_ps(&(Dotu.imaginary), dotu_i);
}

//Parallélisé + Vectorisé
void mncblas_cdotu_sub_vp(const int N, const void *X, const int incX, const void *Y, const int incY, void *dotu)
{
__m128 v1_r, v1_i, v2_r, v2_i, res_r, res_i, dotu_r, dotu_i ;
  register unsigned int i ;
  complexe_float_t *x = (complexe_float_t *)X;
  complexe_float_t *y = (complexe_float_t *)Y;
  complexe_float_t Dotu = (*(complexe_float_t *)dotu);
  Dotu.real = 0.0;
  Dotu.imaginary = 0.0;
  dotu_r =  _mm_set1_ps(Dotu.real);
  dotu_i =  _mm_set1_ps(Dotu.imaginary);
  float tmp;
  #pragma omp parallel for schedule(static)  private(v1_r, v1_i, v2_r, v2_i,tmp)
  for (i = 0; i < N; i = i + 4)
  {
    v1_r = _mm_set1_ps (x[i].real) ;
    v1_i = _mm_set1_ps (x[i].imaginary) ;
    v2_r = _mm_set1_ps (y[i].real);
    v2_i = _mm_set1_ps (y[i].imaginary) ;
    mult_complexe_float_v(&v1_r, &v1_i, &v2_r, &v2_i, &res_r, &res_i);

    dotu_r = _mm_add_ps(dotu_r, res_r);
    dotu_i = _mm_add_ps(dotu_i, res_i);
  }
  _mm_store_ps(&tmp, dotu_r);
  Dotu.real = tmp;
  _mm_store_ps(&tmp, dotu_i);
  Dotu.imaginary = tmp;
}

/*----------------------------------cdotc_sub---------------------------*/

//Normal
void   mncblas_cdotc_sub(const int N, const void *X, const int incX, const void *Y, const int incY, void *dotc)
{
  register unsigned int i = 0 ;
  register unsigned int j = 0 ;
  register complexe_float_t tmp;
  register complexe_float_t conjX;

  for (; ((i < N) && (j < N)) ; i += incX, j+=incY)
  {
    conjX = conjugue_float(((complexe_float_t*)X)[i]);
    tmp = mult_complexe_float (conjX,((complexe_float_t*)Y)[j]);
    *(complexe_float_t *)dotc = add_complexe_float(*(complexe_float_t *)dotc, tmp) ;
  }

}

//Parallélisé
void mncblas_cdotc_sub_p(const int N, const void *X, const int incX, const void *Y, const int incY, void *dotc)
{
  register unsigned int i = 0;
  register complexe_float_t tmp;
  register complexe_float_t conjX;
  register float im = 0.0, re = 0.0;

  #pragma omp parallel for schedule(static) private(conjX,tmp) reduction(+:re) reduction(+:im)
  for (i = 0; i < N; i += incX)
  {
    conjX = conjugue_float(((complexe_float_t *)X)[i]);
    tmp = mult_complexe_float(conjX, ((complexe_float_t *)Y)[i]);
    re += tmp.real;
    im += tmp.imaginary;
  }
  (*(complexe_float_t*)dotc).real = re;
  (*(complexe_float_t*)dotc).imaginary = im;
}

//Vectorisé
void mncblas_cdotc_sub_v(const int N, const void *X, const int incX, const void *Y, const int incY, void *dotc)
{
__m128 v1_r, v1_i, v2_r, v2_i, res_r, res_i, dotc_r, dotc_i, tmp_r, tmp_i;
  register unsigned int i ;
  complexe_float_t *x = (complexe_float_t *)X;
  complexe_float_t *y = (complexe_float_t *)Y;
  complexe_float_t Dotc = (*(complexe_float_t *)dotc);
  Dotc.real = 0.0;
  Dotc.imaginary = 0.0;
  dotc_r =  _mm_set1_ps(Dotc.real);
  dotc_i =  _mm_set1_ps(Dotc.imaginary);
  float tmp;
  for (i = 0; i < N; i += 4)
  {
    tmp_r = _mm_set1_ps(x[i].real);
    tmp_i = _mm_set1_ps(x[i].imaginary);
    conjugue_float_v(&tmp_r, &tmp_i, &v1_r, &v1_i);
    v2_r = _mm_set1_ps (y[i].real) ;
    v2_i = _mm_set1_ps (y[i].imaginary) ;
    mult_complexe_float_v(&v1_r, &v1_i, &v2_r, &v2_i, &res_r, &res_i);

    dotc_r = _mm_add_ps(dotc_r, res_r);
    dotc_i = _mm_add_ps(dotc_i, res_i);
  }
  _mm_store_ps(&tmp, dotc_r);
  Dotc.real = tmp;
  _mm_store_ps(&tmp, dotc_i);
  Dotc.imaginary = tmp;
}

//Parallélisé + Vectorisé
void mncblas_cdotc_sub_vp(const int N, const void *X, const int incX, const void *Y, const int incY, void *dotc)
{
__m128 v1_r, v1_i, v2_r, v2_i, res_r, res_i, dotc_r, dotc_i, tmp_r, tmp_i;
  register unsigned int i ;
  complexe_float_t *x = (complexe_float_t *)X;
  complexe_float_t *y = (complexe_float_t *)Y;
  complexe_float_t Dotc = (*(complexe_float_t *)dotc);
  Dotc.real = 0.0;
  Dotc.imaginary = 0.0;
  dotc_r =  _mm_set1_ps(Dotc.real);
  dotc_i =  _mm_set1_ps(Dotc.imaginary);
  float tmp;
  #pragma omp parallel for schedule(static)  private(v1_r, v1_i, v2_r, v2_i) private(tmp)
  for (i = 0; i < N; i = i + 4)
  {
    tmp_r = _mm_set1_ps(x[i].real);
    tmp_i = _mm_set1_ps(x[i].imaginary);
    conjugue_float_v(&tmp_r, &tmp_i, &v1_r, &v1_i);
    v2_r = _mm_set1_ps (y[i].real) ;
    v2_i = _mm_set1_ps (y[i].imaginary) ;
    mult_complexe_float_v(&v1_r, &v1_i, &v2_r, &v2_i, &res_r, &res_i);

    dotc_r = _mm_add_ps(dotc_r, res_r);
    dotc_i = _mm_add_ps(dotc_i, res_i);
  }
  _mm_store_ps(&tmp, dotc_r);
  Dotc.real = tmp;
  _mm_store_ps(&tmp, dotc_i);
  Dotc.imaginary = tmp;
}

/*----------------------------------zdotu_sub----------------------------*/

//Normal
void   mncblas_zdotu_sub(const int N, const void *X, const int incX, const void *Y, const int incY, void *dotu)
{
  register unsigned int i = 0 ;
  register unsigned int j = 0 ;
  register complexe_double_t tmp;

  for (; ((i < N) && (j < N)) ; i += incX, j+=incY)
  {
    tmp = mult_complexe_double (((complexe_double_t*)X)[i],((complexe_double_t*)Y)[j]);
    *(complexe_double_t *)dotu = add_complexe_double(*(complexe_double_t *)dotu, tmp) ;
  }
}

//Parallélisé
void mncblas_zdotu_sub_p(const int N, const void *X, const int incX, const void *Y, const int incY, void *dotu)
{
  register unsigned int i = 0;
  register complexe_double_t tmp;
  register double im = 0.0, re = 0.0;
  #pragma omp parallel for schedule(static) private(tmp) reduction(+:re) reduction(+:im)
  for (i = 0; i < N; i += incX)
  {
    tmp = mult_complexe_double(((complexe_double_t *)X)[i], ((complexe_double_t *)Y)[i]);
    re += tmp.real;
    im += tmp.imaginary;
  }
  (*(complexe_float_t*)dotu).real = re;
  (*(complexe_float_t*)dotu).imaginary = im;
}

//Vectorisé
void mncblas_zdotu_sub_v(const int N, const void *X, const int incX, const void *Y, const int incY, void *dotu)
{
__m128d v1_r, v1_i, v2_r, v2_i, res_r, res_i, dotu_r, dotu_i;
  register unsigned int i ;
  complexe_double_t *x = (complexe_double_t *)X;
  complexe_double_t *y = (complexe_double_t *)Y;
  complexe_double_t Dotu = (*(complexe_double_t *)dotu);
  Dotu.real = 0.0;
  Dotu.imaginary = 0.0;
  dotu_r =  _mm_set1_pd(Dotu.real);
  dotu_i =  _mm_set1_pd(Dotu.imaginary);
  double tmp;
  for (i = 0; i < N; i = i + 4)
  {
    v1_r = _mm_set1_pd (x[i].real);
    v1_i = _mm_set1_pd (x[i].imaginary);
    v2_r = _mm_set1_pd (y[i].real);
    v2_i = _mm_set1_pd (y[i].imaginary) ;
    mult_complexe_double_v(&v1_r, &v1_i, &v2_r, &v2_i, &res_r, &res_i);

    dotu_r = _mm_add_pd(dotu_r, res_r);
    dotu_i = _mm_add_pd(dotu_i, res_i);
  }
  _mm_store_pd(&tmp, dotu_r);
  Dotu.real = tmp;
  _mm_store_pd(&tmp, dotu_i);
  Dotu.imaginary = tmp;
}
//Parallélisé + Vectorisé
void mncblas_zdotu_sub_vp(const int N, const void *X, const int incX, const void *Y, const int incY, void *dotu)
{
__m128d v1_r, v1_i, v2_r, v2_i, res_r, res_i, dotu_r, dotu_i ;
  register unsigned int i ;
  complexe_double_t *x = (complexe_double_t *)X;
  complexe_double_t *y = (complexe_double_t *)Y;
  complexe_double_t Dotu = (*(complexe_double_t *)dotu);
  Dotu.real = 0.0;
  Dotu.imaginary = 0.0;
  dotu_r =  _mm_set1_pd(Dotu.real);
  dotu_i =  _mm_set1_pd(Dotu.imaginary);
  double tmp;

  #pragma omp parallel for schedule(static)  private(v1_r, v1_i, v2_r, v2_i,tmp)
  for (i = 0; i < N; i += 4)
  {
    v1_r = _mm_set1_pd (x[i].real) ;
    v1_i = _mm_set1_pd (x[i].imaginary) ;
    v2_r = _mm_set1_pd (y[i].real) ;
    v2_i = _mm_set1_pd (y[i].imaginary) ;
    mult_complexe_double_v(&v1_r, &v1_i, &v2_r, &v2_i, &res_r, &res_i);

    dotu_r = _mm_add_pd(dotu_r, res_r);
    dotu_i = _mm_add_pd(dotu_i, res_i);
  }
  _mm_store_pd(&tmp, dotu_r);
  Dotu.real = tmp;
  _mm_store_pd(&tmp, dotu_i);
  Dotu.imaginary = tmp;
  }

/*----------------------------------zdotc_sub----------------------------*/

//Normal
void   mncblas_zdotc_sub(const int N, const void *X, const int incX, const void *Y, const int incY, void *dotc)
{
  register unsigned int i = 0 ;
  register unsigned int j = 0 ;
  register complexe_double_t tmp;
  register complexe_double_t conjX;

  for (; ((i < N) && (j < N)) ; i += incX, j+=incY)
  {
    conjX = conjugue_double(((complexe_double_t*)X)[i]);
    tmp = mult_complexe_double (conjX,((complexe_double_t*)Y)[j]);
    *(complexe_double_t *)dotc = add_complexe_double(*(complexe_double_t *)dotc, tmp) ;
  }

}

//Parallélisé
void mncblas_zdotc_sub_p(const int N, const void *X, const int incX, const void *Y, const int incY, void *dotc)
{
  register unsigned int i = 0;
  register complexe_double_t tmp;
  register complexe_double_t conjX;
  register double im = 0.0, re = 0.0;

  #pragma omp parallel for schedule(static) private(conjX,tmp) reduction(+:re) reduction(+:im)
  for (i = 0; i < N; i += incX)
  {
    conjX = conjugue_double(((complexe_double_t *)X)[i]);
    tmp = mult_complexe_double(conjX, ((complexe_double_t *)Y)[i]);
    re += tmp.real;
    im += tmp.imaginary;
  }
  (*(complexe_float_t*)dotc).real = re;
  (*(complexe_float_t*)dotc).imaginary = im;
}

//Vectorisé
void mncblas_zdotc_sub_v(const int N, const void *X, const int incX, const void *Y, const int incY, void *dotc)
{
__m128d v1_r, v1_i, v2_r, v2_i, res_r, res_i, dotc_r, dotc_i, tmp_r, tmp_i ;
  register unsigned int i ;
  complexe_double_t *x = (complexe_double_t *)X;
  complexe_double_t *y = (complexe_double_t *)Y;
  complexe_double_t Dotc = (*(complexe_double_t *)dotc);
  Dotc.real = 0.0;
  Dotc.imaginary = 0.0;
  dotc_r =  _mm_set1_pd(Dotc.real);
  dotc_i =  _mm_set1_pd(Dotc.imaginary);
  double tmp;
  for (i = 0; i < N; i = i + 4)
  {
    tmp_r = _mm_set1_pd(x[i].real);
    tmp_i = _mm_set1_pd(x[i].imaginary);
    conjugue_double_v(&tmp_r, &tmp_i, &v1_r, &v1_i);
    v2_r = _mm_set1_pd (y[i].real) ;
    v2_i = _mm_set1_pd (y[i].imaginary) ;
    mult_complexe_double_v(&v1_r, &v1_i, &v2_r, &v2_i, &res_r, &res_i);

    dotc_r = _mm_add_pd(dotc_r, res_r);
    dotc_i = _mm_add_pd(dotc_i, res_i);
  }
  _mm_store_pd(&tmp, dotc_r);
  Dotc.real = tmp;
  _mm_store_pd(&tmp, dotc_i);
  Dotc.imaginary = tmp;
}

//Parallélisé + Vectorisé
void mncblas_zdotc_sub_vp(const int N, const void *X, const int incX, const void *Y, const int incY, void *dotc)
{
__m128d v1_r, v1_i, v2_r, v2_i, res_r, res_i, dotc_r, dotc_i, tmp_r, tmp_i ;
  register unsigned int i ;
  complexe_double_t *x = (complexe_double_t *)X;
  complexe_double_t *y = (complexe_double_t *)Y;
  complexe_double_t Dotc = (*(complexe_double_t *)dotc);
  Dotc.real = 0.0;
  Dotc.imaginary = 0.0;
  dotc_r =  _mm_set1_pd(Dotc.real);
  dotc_i =  _mm_set1_pd(Dotc.imaginary);
  double tmp;
  #pragma omp parallel for schedule(static)  private(v1_r, v1_i, v2_r, v2_i) private (tmp)
  for (i = 0; i < N; i = i + 4)
  {
    tmp_r = _mm_set1_pd(x[i].real);
    tmp_i = _mm_set1_pd(x[i].imaginary);
    conjugue_double_v(&tmp_r, &tmp_i, &v1_r, &v1_i);
    v2_r = _mm_set1_pd (y[i].real) ;
    v2_i = _mm_set1_pd (y[i].imaginary) ;
    mult_complexe_double_v(&v1_r, &v1_i, &v2_r, &v2_i, &res_r, &res_i);

    dotc_r = _mm_add_pd(dotc_r, res_r);
    dotc_i = _mm_add_pd(dotc_i, res_i);
  }
   _mm_store_pd(&tmp, dotc_r);
  Dotc.real = tmp;
  _mm_store_pd(&tmp, dotc_i);
  Dotc.imaginary = tmp;
}









