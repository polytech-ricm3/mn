#include "../include/complexe.h"
#include "../include/mnblas.h"

void mncblas_sgemm(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const float alpha, const float *A,
                 const int lda, const float *B, const int ldb,
                 const float beta, float *C, const int ldc)
{
    register unsigned int i;
    register unsigned int j;
    register unsigned int k;
    float f;
    for (i = 0; i < M; i++) {
        for (j = 0; j < M; j++) {
            C[i * M + j] = beta * C[i * M + j];
            f = 0;
            for (k = 0; k < M; k++) 
                f += A[i * M + k] * B[k * M + j];
            C[i * M + j] += f * alpha;
        }
    }
}

void mncblas_sgemm_p(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const float alpha, const float *A,
                 const int lda, const float *B, const int ldb,
                 const float beta, float *C, const int ldc)
{
    register unsigned int i;
    register unsigned int j;
    register unsigned int k;
    float f;
    #pragma omp parallel for schedule(static) collapse(2)
    for (i = 0; i < M; i++) {
        for (j = 0; j < M; j++) {
            C[i * M + j] = beta * C[i * M + j];
            f = 0;
            #pragma omp parallel for schedule(static) reduction(+:f)
            for (k = 0; k < M; k++) 
                f += A[i * M + k] * B[k * M + j];
            C[i * M + j] += f * alpha;
        }
    }
}

void mncblas_sgemm_v(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const float alpha, const float *A,
                 const int lda, const float *B, const int ldb,
                 const float beta, float *C, const int ldc)
{
    register unsigned int i;
    register unsigned int j;
    register unsigned int k;
    float tmp_f;
    __m128 f, beta_m = _mm_set1_ps(beta), alpha_m = _mm_set1_ps(alpha);
    for (i = 0; i < M; i++) {
        for (j = 0; j < M; j++) {
            _mm_store1_ps(&tmp_f,
                        _mm_mul_ps(beta_m,_mm_set1_ps(C[i * M + j])));
            C[i * M + j] = tmp_f;
            f = _mm_set1_ps(0);
            for (k = 0; k < M; k+=4)
            {
                f = _mm_add_ps(f, _mm_mul_ps(
                                        _mm_set1_ps(A[i * M + k]),
                                        _mm_set1_ps(B[k * M + j])
                                    )
                              );
            }
            _mm_store1_ps(&tmp_f, _mm_add_ps(_mm_set1_ps(C[i * M + j]),
                                                     _mm_mul_ps(f,alpha_m)));
            C[i * M + j] = tmp_f;
        }
    }
}

void mncblas_sgemm_vp(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const float alpha, const float *A,
                 const int lda, const float *B, const int ldb,
                 const float beta, float *C, const int ldc)
{
    register unsigned int i;
    register unsigned int j;
    register unsigned int k;
    float tmp_f;
    __m128 f, beta_m = _mm_set1_ps(beta), alpha_m = _mm_set1_ps(alpha);
    #pragma omp parallel for schedule(static) collapse(2) private(tmp_f)
    for (i = 0; i < M; i++) {
        for (j = 0; j < M; j++) {
            _mm_store1_ps(&tmp_f,
                        _mm_mul_ps(beta_m,_mm_set1_ps(C[i * M + j])));
            C[i * M + j] = tmp_f;
            f = _mm_set1_ps(0);
            #pragma omp parallel for schedule(static)
            for (k = 0; k < M; k+=4)
            {
                f = _mm_add_ps(f, _mm_mul_ps(
                                        _mm_set1_ps(A[i * M + k]),
                                        _mm_set1_ps(B[k * M + j])
                                    )
                              );
            }
            _mm_store1_ps(&tmp_f, _mm_add_ps(_mm_set1_ps(C[i * M + j]),
                                                     _mm_mul_ps(f,alpha_m)));
            C[i * M + j] = tmp_f;
        }
    }
}

void mncblas_dgemm(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const double alpha, const double *A,
                 const int lda, const double *B, const int ldb,
                 const double beta, double *C, const int ldc)
{
    register unsigned int i;
    register unsigned int j;
    register unsigned int k;
    double d;
    for (i = 0; i < M; i++) {
        for (j = 0; j < M; j++) {
            C[i * M + j] = beta * C[i * M + j];
            d = 0;
            for (k = 0; k < M; k++) 
                d += A[i * M + k] * B[k * M + j];
            C[i * M + j] += d * alpha;
        }
    }
}

void mncblas_dgemm_p(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const double alpha, const double *A,
                 const int lda, const double *B, const int ldb,
                 const double beta, double *C, const int ldc)
{
    register unsigned int i;
    register unsigned int j;
    register unsigned int k;
    double d;
    
    #pragma omp parallel for schedule(static) collapse(2)
    for (i = 0; i < M; i++) {
        for (j = 0; j < M; j++) {
            C[i * M + j] = beta * C[i * M + j];
            d = 0;
            #pragma omp parallel for schedule(static) reduction(+:d)
            for (k = 0; k < M; k++) 
                d += A[i * M + k] * B[k * M + j];
            C[i * M + j] += d * alpha;
        }
    }
}

void mncblas_dgemm_v(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const double alpha, const double *A,
                 const int lda, const double *B, const int ldb,
                 const double beta, double *C, const int ldc)
{
    register unsigned int i;
    register unsigned int j;
    register unsigned int k;

   __m128d f, beta_m = _mm_set1_pd(beta), alpha_m = _mm_set1_pd(alpha);
   double tmp_d;
    for (i = 0; i < M; i++) {
        for (j = 0; j < M; j++) {
            _mm_store1_pd(&tmp_d,
                        _mm_mul_pd(beta_m,_mm_set1_pd(C[i * M + j])));
            f = _mm_set1_pd(0);
            for (k = 0; k < M; k+=2)
            {
                f = _mm_add_pd(f, _mm_mul_pd(
                                        _mm_set1_pd(A[i * M + k]),
                                        _mm_set1_pd(B[k * M + j])
                                    )
                              );
            }
            _mm_store1_pd(&tmp_d, _mm_add_pd(_mm_set1_pd(C[i * M + j]),
                                                     _mm_mul_pd(f,alpha_m)));
            C[i * M + j] = tmp_d;
        }
    }
}

void mncblas_dgemm_vp(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const double alpha, const double *A,
                 const int lda, const double *B, const int ldb,
                 const double beta, double *C, const int ldc)
{
    register unsigned int i;
    register unsigned int j;
    register unsigned int k;

   __m128d d, beta_m = _mm_set1_pd(beta), alpha_m = _mm_set1_pd(alpha);
   double tmp_d;
   #pragma omp parallel for schedule(static) collapse(2) private(tmp_d)
    for (i = 0; i < M; i++) {
        for (j = 0; j < M; j++) {
            _mm_store1_pd(&tmp_d,
                        _mm_mul_pd(beta_m,_mm_set1_pd(C[i * M + j])));
            d = _mm_set1_pd(0);
            #pragma omp parallel for schedule(static)
            for (k = 0; k < M; k+=2)
            {
                d = _mm_add_pd(d, _mm_mul_pd(
                                        _mm_set1_pd(A[i * M + k]),
                                        _mm_set1_pd(B[k * M + j])
                                    )
                              );
            }
            _mm_store1_pd(&tmp_d, _mm_add_pd(_mm_set1_pd(C[i * M + j]),
                                                     _mm_mul_pd(d,alpha_m)));
            C[i * M + j] = tmp_d;
        }
    }
}

void mncblas_cgemm(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const void *alpha, const void *A,
                 const int lda, const void *B, const int ldb,
                 const void *beta, void *C, const int ldc)
{
    register unsigned int i;
    register unsigned int j;
    register unsigned int k;
    complexe_float_t *Ac = ((complexe_float_t*)A);
    complexe_float_t *Bc = ((complexe_float_t*)B);
    complexe_float_t *Cc = ((complexe_float_t*)C);
    complexe_float_t alphac = (*(complexe_float_t*)alpha);
    complexe_float_t betac = (*(complexe_float_t*)beta);
    
    complexe_float_t tmp = {0,0};

    for (i = 0; i < M; i++) {
        for (j = 0; j < M; j++) {
            Cc[i * M +j] = mult_complexe_float (Cc[i * M +j], betac);
            tmp.imaginary = 0;
            tmp.real = 0;
            for (k = 0; k < M; k++) { 
                tmp = add_complexe_float(tmp,mult_complexe_float(Ac[i * M + k], Bc[k * M + j]));
            }
            Cc[i * M + j] = add_complexe_float(Cc[i * M + j],mult_complexe_float (alphac, tmp));
        }
    }
}

void mncblas_cgemm_p(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const void *alpha, const void *A,
                 const int lda, const void *B, const int ldb,
                 const void *beta, void *C, const int ldc)
{
    register unsigned int i;
    register unsigned int j;
    register unsigned int k;
    complexe_float_t *Ac = ((complexe_float_t*)A);
    complexe_float_t *Bc = ((complexe_float_t*)B);
    complexe_float_t *Cc = ((complexe_float_t*)C);
    complexe_float_t alphac = (*(complexe_float_t*)alpha);
    complexe_float_t betac = (*(complexe_float_t*)beta);
    
    register float re = 0.0, im = 0.0;
    register complexe_float_t tmp;

    #pragma omp parallel for schedule(static) collapse(2)
    for (i = 0; i < M; i++) {
        for (j = 0; j < M; j++) {
            Cc[i * M +j] = mult_complexe_float (Cc[i * M +j], betac);
            im = 0.0;
            re = 0.0;
            #pragma omp parallel for schedule(static) private(tmp) reduction(+:im) reduction(+:re)
            for (k = 0; k < M; k++) { 
                tmp = mult_complexe_float(Ac[i * M + k], Bc[k * M + j]);
                re += tmp.real;
                im += tmp.imaginary;
            }
            tmp.real = re;
            tmp.imaginary = im;
            Cc[i * M + j] = add_complexe_float(Cc[i * M + j],mult_complexe_float (alphac, tmp));
        }
    }
}

void mncblas_cgemm_v(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const void *alpha, const void *A,
                 const int lda, const void *B, const int ldb,
                 const void *beta, void *C, const int ldc)
{
    register unsigned int i;
    register unsigned int j;
    register unsigned int k;
    complexe_float_t *Ac = ((complexe_float_t*)A);
    complexe_float_t *Bc = ((complexe_float_t*)B);
    complexe_float_t *Cc = ((complexe_float_t*)C);
    complexe_float_t alphac = (*(complexe_float_t*)alpha);
    complexe_float_t betac = (*(complexe_float_t*)beta);
    
    //register float re = 0.0, im = 0.0;
    __m128 re = _mm_set1_ps(0.0), img = _mm_set1_ps(0.0), alpha_r = _mm_set1_ps(alphac.real), alpha_i = _mm_set1_ps(alphac.imaginary), 
    beta_r = _mm_set1_ps(betac.real), beta_i = _mm_set1_ps(betac.imaginary), tmp_r, tmp_i, tmp2_r, tmp2_i, tmp3_r, tmp3_i;
    float tmp_f;

    for (i = 0; i < M; i++) {
        for (j = 0; j < M; j++) {
            tmp_r = _mm_set1_ps(Cc[i * M +j].real);
            tmp_i = _mm_set1_ps(Cc[i * M +j].imaginary);
            mult_complexe_float_v (&tmp_r, &tmp_i, &beta_r, &beta_i, &tmp2_r, &tmp2_i);
            _mm_store1_ps(&tmp_f,tmp2_r);
            Cc[i * M +j].real = tmp_f;
            _mm_store1_ps(&tmp_f,tmp2_i);
            Cc[i * M +j].imaginary = tmp_f;
            for (k = 0; k < M; k+=4) { 
                tmp2_r = _mm_set1_ps(Ac[i * M + k].real);
                tmp2_i = _mm_set1_ps(Ac[i * M + k].imaginary);
                tmp3_r = _mm_set1_ps(Bc[k * M + j].real);
                tmp3_i = _mm_set1_ps(Bc[k * M + j].imaginary);
                mult_complexe_float_v(&tmp2_r, &tmp2_i, &tmp3_r, &tmp2_i, &tmp_r, &tmp_i);
                
                re = _mm_add_ps(re,tmp_r);
                img = _mm_add_ps(img,tmp_i);
            }
            
            mult_complexe_float_v(&alpha_r, &alpha_i,&re,&img,&tmp_r,&tmp_i);
            tmp2_r = _mm_set1_ps(Cc[i * M +j].real);
            tmp2_i = _mm_set1_ps(Cc[i * M +j].imaginary);
            add_complexe_float_v(&tmp2_r, &tmp2_i, &tmp_r, &tmp_i, &tmp3_r, &tmp3_i);
            _mm_store1_ps(&tmp_f,tmp3_r);
            Cc[i * M +j].real = tmp_f;
            _mm_store1_ps(&tmp_f,tmp3_i);
            Cc[i * M +j].imaginary = tmp_f;
        }
    }
}

void mncblas_cgemm_vp(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const void *alpha, const void *A,
                 const int lda, const void *B, const int ldb,
                 const void *beta, void *C, const int ldc)
{
    register unsigned int i;
    register unsigned int j;
    register unsigned int k;
    complexe_float_t *Ac = ((complexe_float_t*)A);
    complexe_float_t *Bc = ((complexe_float_t*)B);
    complexe_float_t *Cc = ((complexe_float_t*)C);
    complexe_float_t alphac = (*(complexe_float_t*)alpha);
    complexe_float_t betac = (*(complexe_float_t*)beta);
    
    //register float re = 0.0, im = 0.0;
    __m128 re = _mm_set1_ps(0.0), img = _mm_set1_ps(0.0), alpha_r = _mm_set1_ps(alphac.real), alpha_i = _mm_set1_ps(alphac.imaginary), 
    beta_r = _mm_set1_ps(betac.real), beta_i = _mm_set1_ps(betac.imaginary), tmp_r, tmp_i, tmp2_r, tmp2_i, tmp3_r, tmp3_i;
    float tmp_f;


    #pragma omp parallel for schedule(static) private(tmp_f) collapse(2)
    for (i = 0; i < M; i++) {
        for (j = 0; j < M; j++) {
            tmp_r = _mm_set1_ps(Cc[i * M +j].real);
            tmp_i = _mm_set1_ps(Cc[i * M +j].imaginary);
            mult_complexe_float_v (&tmp_r, &tmp_i, &beta_r, &beta_i, &tmp2_r, &tmp2_i);
            _mm_store1_ps(&tmp_f,tmp2_r);
            Cc[i * M +j].real = tmp_f;
            _mm_store1_ps(&tmp_f,tmp2_i);
            Cc[i * M +j].imaginary = tmp_f;
            #pragma omp parallel for schedule(static)
            for (k = 0; k < M; k+=4) { 
                tmp2_r = _mm_set1_ps(Ac[i * M + k].real);
                tmp2_i = _mm_set1_ps(Ac[i * M + k].imaginary);
                tmp3_r = _mm_set1_ps(Bc[k * M + j].real);
                tmp3_i = _mm_set1_ps(Bc[k * M + j].imaginary);
                mult_complexe_float_v(&tmp2_r, &tmp2_i, &tmp3_r, &tmp2_i, &tmp_r, &tmp_i);
                
                re = _mm_add_ps(re,tmp_r);
                img = _mm_add_ps(img,tmp_i);
            }
            mult_complexe_float_v(&alpha_r, &alpha_i,&re,&img,&tmp_r,&tmp_i);
            tmp2_r = _mm_set1_ps(Cc[i * M +j].real);
            tmp2_i = _mm_set1_ps(Cc[i * M +j].imaginary);
            add_complexe_float_v(&tmp2_r, &tmp2_i, &tmp_r, &tmp_i, &tmp3_r, &tmp3_i);
            _mm_store1_ps(&tmp_f,tmp3_r);
            Cc[i * M +j].real = tmp_f;
            _mm_store1_ps(&tmp_f,tmp3_i);
            Cc[i * M +j].imaginary = tmp_f;
        }
    }
}

void mncblas_zgemm(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const void *alpha, const void *A,
                 const int lda, const void *B, const int ldb,
                 const void *beta, void *C, const int ldc)
{
    register unsigned int i;
    register unsigned int j;
    register unsigned int k;
    complexe_double_t *Ac = ((complexe_double_t*)A);
    complexe_double_t *Bc = ((complexe_double_t*)B);
    complexe_double_t *Cc = ((complexe_double_t*)C);
    complexe_double_t alphac = (*(complexe_double_t*)alpha);
    complexe_double_t betac = (*(complexe_double_t*)beta);
    
    complexe_double_t tmp = {0,0};

    for (i = 0; i < M; i++) {
        for (j = 0; j < M; j++) {
            Cc[i * M +j] = mult_complexe_double (Cc[i * M +j], betac);
            tmp.imaginary = 0;
            tmp.real = 0;
            for (k = 0; k < M; k++) {
                tmp = add_complexe_double(tmp,mult_complexe_double(Ac[i * M + k], Bc[k * M + j]));
            }
            Cc[i * M + j] = add_complexe_double(Cc[i * M + j],mult_complexe_double (alphac, tmp));
        }
    }
}

void mncblas_zgemm_p(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const void *alpha, const void *A,
                 const int lda, const void *B, const int ldb,
                 const void *beta, void *C, const int ldc)
{
    register unsigned int i;
    register unsigned int j;
    register unsigned int k;
    complexe_double_t *Ac = ((complexe_double_t*)A);
    complexe_double_t *Bc = ((complexe_double_t*)B);
    complexe_double_t *Cc = ((complexe_double_t*)C);
    complexe_double_t alphac = (*(complexe_double_t*)alpha);
    complexe_double_t betac = (*(complexe_double_t*)beta);
    
    register double re = 0.0, im = 0.0;
    register complexe_double_t tmp;

    #pragma omp parallel for schedule(static) collapse(2)
    for (i = 0; i < M; i++) {
        for (j = 0; j < M; j++) {
            Cc[i * M +j] = mult_complexe_double (Cc[i * M +j], betac);
            im = 0.0;
            re = 0.0;
            #pragma omp parallel for schedule(static) private(tmp) reduction(+:im) reduction(+:re)
            for (k = 0; k < M; k++) {
                tmp = mult_complexe_double(Ac[i * M + k], Bc[k * M + j]);
                re += tmp.real;
                im += tmp.imaginary;
            }
            tmp.real = re;
            tmp.imaginary = im;
            Cc[i * M + j] = add_complexe_double(Cc[i * M + j],mult_complexe_double (alphac, tmp));
        }
    }
}

void mncblas_zgemm_v(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const void *alpha, const void *A,
                 const int lda, const void *B, const int ldb,
                 const void *beta, void *C, const int ldc)
{
    register unsigned int i;
    register unsigned int j;
    register unsigned int k;
    complexe_double_t *Ac = ((complexe_double_t*)A);
    complexe_double_t *Bc = ((complexe_double_t*)B);
    complexe_double_t *Cc = ((complexe_double_t*)C);
    complexe_double_t alphac = (*(complexe_double_t*)alpha);
    complexe_double_t betac = (*(complexe_double_t*)beta);
    
    __m128d re = _mm_set1_pd(0.0), img = _mm_set1_pd(0.0), alpha_r = _mm_set1_pd(alphac.real), alpha_i = _mm_set1_pd(alphac.imaginary), 
    beta_r = _mm_set1_pd(betac.real), beta_i = _mm_set1_pd(betac.imaginary), tmp_r, tmp_i, tmp2_r, tmp2_i, tmp3_r, tmp3_i;
    double tmp_d;

    for (i = 0; i < M; i++) {
        for (j = 0; j < M; j++) {
            tmp_r = _mm_set1_pd(Cc[i * M +j].real);
            tmp_i = _mm_set1_pd(Cc[i * M +j].imaginary);
            mult_complexe_double_v (&tmp_r, &tmp_i, &beta_r, &beta_i, &tmp2_r, &tmp2_i);
            _mm_store1_pd(&tmp_d,tmp2_r);
            Cc[i * M +j].real = tmp_d;
            _mm_store1_pd(&tmp_d,tmp2_i);
            Cc[i * M +j].imaginary = tmp_d;
            for (k = 0; k < M; k+=2) {
                tmp2_r = _mm_set1_pd(Ac[i * M + k].real);
                tmp2_i = _mm_set1_pd(Ac[i * M + k].imaginary);
                tmp3_r = _mm_set1_pd(Bc[k * M + j].real);
                tmp3_i = _mm_set1_pd(Bc[k * M + j].imaginary);
                mult_complexe_double_v(&tmp2_r, &tmp2_i, &tmp3_r, &tmp2_i, &tmp_r, &tmp_i);
                
                re = _mm_add_pd(re,tmp_r);
                img = _mm_add_pd(img,tmp_i);
            }
            mult_complexe_double_v(&alpha_r, &alpha_i,&re,&img,&tmp_r,&tmp_i);
            tmp2_r = _mm_set1_pd(Cc[i * M +j].real);
            tmp2_i = _mm_set1_pd(Cc[i * M +j].imaginary);
            add_complexe_double_v(&tmp2_r, &tmp2_i, &tmp_r, &tmp_i, &tmp3_r, &tmp3_i);
            _mm_store1_pd(&tmp_d,tmp3_r);
            Cc[i * M +j].real = tmp_d;
            _mm_store1_pd(&tmp_d,tmp3_i);
            Cc[i * M +j].imaginary = tmp_d;
        }
    }
}

void mncblas_zgemm_vp(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const void *alpha, const void *A,
                 const int lda, const void *B, const int ldb,
                 const void *beta, void *C, const int ldc)
{
    register unsigned int i;
    register unsigned int j;
    register unsigned int k;
    complexe_double_t *Ac = ((complexe_double_t*)A);
    complexe_double_t *Bc = ((complexe_double_t*)B);
    complexe_double_t *Cc = ((complexe_double_t*)C);
    complexe_double_t alphac = (*(complexe_double_t*)alpha);
    complexe_double_t betac = (*(complexe_double_t*)beta);
    double tmp_d;
    
    __m128d re = _mm_set1_pd(0.0), img = _mm_set1_pd(0.0), alpha_r = _mm_set1_pd(alphac.real), alpha_i = _mm_set1_pd(alphac.imaginary), 
    beta_r = _mm_set1_pd(betac.real), beta_i = _mm_set1_pd(betac.imaginary), tmp_r, tmp_i, tmp2_r, tmp2_i, tmp3_r, tmp3_i;
    #pragma omp parallel for schedule(static) collapse(2) private(tmp_d)
    for (i = 0; i < M; i++) {
        for (j = 0; j < M; j++) {
            tmp_r = _mm_set1_pd(Cc[i * M +j].real);
            tmp_i = _mm_set1_pd(Cc[i * M +j].imaginary);
            mult_complexe_double_v (&tmp_r, &tmp_i, &beta_r, &beta_i, &tmp2_r, &tmp2_i);
            _mm_store1_pd(&tmp_d,tmp2_r);
            Cc[i * M +j].real = tmp_d;
            _mm_store1_pd(&tmp_d,tmp2_i);
            Cc[i * M +j].imaginary = tmp_d;
            #pragma omp parallel for schedule(static)
            for (k = 0; k < M; k += 2) {
                tmp2_r = _mm_set1_pd(Ac[i * M + k].real);
                tmp2_i = _mm_set1_pd(Ac[i * M + k].imaginary);
                tmp3_r = _mm_set1_pd(Bc[k * M + j].real);
                tmp3_i = _mm_set1_pd(Bc[k * M + j].imaginary);
                mult_complexe_double_v(&tmp2_r, &tmp2_i, &tmp3_r, &tmp2_i, &tmp_r, &tmp_i);
                
                re = _mm_add_pd(re,tmp_r);
                img = _mm_add_pd(img,tmp_i);
            }
            mult_complexe_double_v(&alpha_r, &alpha_i,&re,&img,&tmp_r,&tmp_i);
            tmp2_r = _mm_set1_pd(Cc[i * M +j].real);
            tmp2_i = _mm_set1_pd(Cc[i * M +j].imaginary);
            add_complexe_double_v(&tmp2_r, &tmp2_i, &tmp_r, &tmp_i, &tmp3_r, &tmp3_i);
            _mm_store1_pd(&tmp_d,tmp3_r);
            Cc[i * M +j].real = tmp_d;
            _mm_store1_pd(&tmp_d,tmp3_i);
            Cc[i * M +j].imaginary = tmp_d;
        }
    }
}