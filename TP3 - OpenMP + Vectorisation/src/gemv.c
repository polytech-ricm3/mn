#include "../include/complexe.h"
#include "../include/mnblas.h"


/* -------------------  sgemv -----------------------*/

//Normal
void mncblas_sgemv(const MNCBLAS_LAYOUT layout,
  const MNCBLAS_TRANSPOSE TransA, const int M, const int N,
  const float alpha, const float *A, const int lda,
  const float *X, const int incX, const float beta,
  float *Y, const int incY)
  {
    register unsigned int i, j;
    float tmp;
    for (i = 0; i < N; i+=incX)
    {
      tmp = 0;
      for (j = 0; j < M; j += incY)
      {
        tmp += A[i * lda + j] * X[j];
      }
      Y[i] = alpha * tmp + beta * Y[i];
    }

  }

//Parallélisé
void mncblas_sgemv_p(const MNCBLAS_LAYOUT layout,
  const MNCBLAS_TRANSPOSE TransA, const int M, const int N,
  const float alpha, const float *A, const int lda,
  const float *X, const int incX, const float beta,
  float *Y, const int incY)
  {
    register unsigned int i, j;
    float tmp;
    #pragma omp parallel for schedule(static)
    for (i = 0; i < N; i+=incX)
    {
      tmp = 0;
      #pragma omp parallel for schedule(static) reduction(+:tmp)
      for (j = 0; j < M; j += incY)
      {
        tmp += A[i * lda + j] * X[j];
      }
      Y[i] = alpha * tmp + beta * Y[i];
    }

  }

  //Vectorisé
  void mncblas_sgemv_v(const MNCBLAS_LAYOUT layout,
  const MNCBLAS_TRANSPOSE TransA, const int M, const int N,
  const float alpha, const float *A, const int lda,
  const float *X, const int incX, const float beta,
  float *Y, const int incY)
  {
    register unsigned int i, j;
    __m128 tmp = _mm_setr_ps(0.0,0.0,0.0,0.0);
    __m128 alpha_m = _mm_set1_ps(alpha), beta_m = _mm_set1_ps(beta);
    for (i = 0; i < N; i+=4)
    {
      
      for (j = 0; j < M; j += 4)
      {
        tmp = _mm_add_ps(tmp,_mm_mul_ps(_mm_load_ps(A + (i * lda + j)),_mm_load_ps(X + j)));
      }
      _mm_store_ps(Y+i,_mm_add_ps(_mm_mul_ps(alpha_m,tmp),_mm_mul_ps(beta_m,_mm_load_ps(Y+i))));
    }

  }

  //Vectorisé + Parallélisé

  void mncblas_sgemv_vp(const MNCBLAS_LAYOUT layout,
  const MNCBLAS_TRANSPOSE TransA, const int M, const int N,
  const float alpha, const float *A, const int lda,
  const float *X, const int incX, const float beta,
  float *Y, const int incY)
  {
    register unsigned int i, j;
    __m128 tmp = _mm_setr_ps(0.0,0.0,0.0,0.0);
    __m128 alpha_m = _mm_set1_ps(alpha), beta_m = _mm_set1_ps(beta);

    #pragma omp parallel for schedule(static)
    for (i = 0; i < N; i+=4)
    {
      
      #pragma omp parallel for schedule(static)
      for (j = 0; j < M; j += 4)
      {
        tmp = _mm_add_ps(tmp,_mm_mul_ps(_mm_load_ps(A + (i * lda + j)),_mm_load_ps(X + j)));
      }
      _mm_store_ps(Y+i,_mm_add_ps(_mm_mul_ps(alpha_m,tmp),_mm_mul_ps(beta_m,_mm_load_ps(Y+i))));
      
    }

  }

  /* -------------------  dgemv -----------------------*/


  //Normal
  void mncblas_dgemv(MNCBLAS_LAYOUT layout,
  MNCBLAS_TRANSPOSE TransA, const int M, const int N,
  const double alpha, const double *A, const int lda,
  const double *X, const int incX, const double beta,
  double *Y, const int incY)
  {
    register unsigned int i, j;
    double tmp;
    for (i = 0; i < N; i+=incX)
    {
      tmp = 0;
      for (j = 0; j < M; j += incY)
      {
        tmp += A[i  * lda + j] * X[j];
      }
      Y[i] = alpha * tmp + beta * Y[i];
    }

  }

  //Parallélisé
  void mncblas_dgemv_p(MNCBLAS_LAYOUT layout,
    MNCBLAS_TRANSPOSE TransA, const int M, const int N,
    const double alpha, const double *A, const int lda,
    const double *X, const int incX, const double beta,
    double *Y, const int incY)
    {
      register unsigned int i,j;
      double tmp;
      #pragma omp parallel for schedule(static)
      for (i = 0; i < N; i+=incX)
      {
        tmp = 0;
        #pragma omp parallel for schedule(static) reduction(+:tmp)
        for (j = 0; j < M; j += incY)
        {
          tmp += A[i  * lda + j] * X[j];
        }
        Y[i] = alpha * tmp + beta * Y[i];
      }

    }

  //Vectorisé
  void mncblas_dgemv_v(MNCBLAS_LAYOUT layout,
    MNCBLAS_TRANSPOSE TransA, const int M, const int N,
    const double alpha, const double *A, const int lda,
    const double *X, const int incX, const double beta,
    double *Y, const int incY)
    {
      register unsigned int i,j;
      __m128d tmp;
      __m128d alpha_m = _mm_set1_pd(alpha), beta_m = _mm_set1_pd(beta);

      for (i = 0; i < N; i+=2)
      {
        tmp = _mm_setr_pd(0.0,0.0);
        for (j = 0; j < M; j += 2)
        {
          tmp = _mm_add_pd(tmp,_mm_mul_pd(_mm_load_pd(A + (i * lda + j)),_mm_load_pd(X + j)));
        }
        _mm_store_pd(Y+i,_mm_add_pd(_mm_mul_pd(alpha_m,tmp),_mm_mul_pd(beta_m,_mm_load_pd(Y+i))));
      }

    }

    //Vectorisé + Parallélisé
    void mncblas_dgemv_vp(MNCBLAS_LAYOUT layout,
    MNCBLAS_TRANSPOSE TransA, const int M, const int N,
    const double alpha, const double *A, const int lda,
    const double *X, const int incX, const double beta,
    double *Y, const int incY)
    {
      register unsigned int i,j;
      __m128d tmp;
      __m128d alpha_m = _mm_set1_pd(alpha), beta_m = _mm_set1_pd(beta);

      #pragma omp parallel for schedule(static)
      for (i = 0; i < N; i+=2)
      {
        tmp = _mm_setr_pd(0.0,0.0);
        #pragma omp parallel for schedule(static)
        for (j = 0; j < M; j += 2)
        {
          tmp = _mm_add_pd(tmp,_mm_mul_pd(_mm_load_pd(A + (i * lda + j)),_mm_load_pd(X + j)));
        }
        _mm_store_pd(Y+i,_mm_add_pd(_mm_mul_pd(alpha_m,tmp),_mm_mul_pd(beta_m,_mm_load_pd(Y+i))));
      }

    }
    
    /* -------------------  cgemv -----------------------*/

    //Normal
    void mncblas_cgemv(MNCBLAS_LAYOUT layout,
  MNCBLAS_TRANSPOSE TransA, const int M, const int N,
  const void *alpha, const void *A, const int lda,
  const void *X, const int incX, const void *beta,
  void *Y, const int incY)
  {
    register unsigned int i, j;
    complexe_float_t *Xc = ((complexe_float_t *)X);
    complexe_float_t *Ac = ((complexe_float_t *)A);
    complexe_float_t *Yc = ((complexe_float_t *)Y);

    register complexe_float_t tmp = {0,0};
    complexe_float_t betac = (*(complexe_float_t*)beta);
    complexe_float_t alphac = (*(complexe_float_t*)alpha);
    for (i = 0; i < N; i+=incX)
    {
      tmp.imaginary = 0;
      tmp.real = 0;
      for (j = 0; j < M; j += incY)
      {
        tmp = add_complexe_float(tmp,mult_complexe_float (Ac[i* lda + j], Xc[j]));
      }
      Yc[i] = add_complexe_float(mult_complexe_float (alphac, tmp), mult_complexe_float (betac, Yc[i]));
    }
  }


    //Parallélisé
    void mncblas_cgemv_p(MNCBLAS_LAYOUT layout,
      MNCBLAS_TRANSPOSE TransA, const int M, const int N,
      const void *alpha, const void *A, const int lda,
      const void *X, const int incX, const void *beta,
      void *Y, const int incY)
      {
        register unsigned int i, j;
        complexe_float_t *Xc = ((complexe_float_t *)X);
        complexe_float_t *Ac = ((complexe_float_t *)A);
        complexe_float_t *Yc = ((complexe_float_t *)Y);
        register float re = 0.0, im = 0.0;

        register complexe_float_t tmp;
        complexe_float_t betac = (*(complexe_float_t*)beta);
        complexe_float_t alphac = (*(complexe_float_t*)alpha);
        #pragma omp parallel for schedule(static)
        for (i = 0; i < N; i+=incX)
        {
          im = 0.0;
          re = 0.0;
          #pragma omp parallel for schedule(static) private(tmp) reduction(+:re) reduction(+:im)
          for (j = 0; j < M; j += incY)
          {
            tmp = mult_complexe_float (Ac[i* lda + j], Xc[j]);
            re += tmp.real;
            im += tmp.imaginary;
          }
          tmp.real = re;
          tmp.imaginary = im;
          Yc[i] = add_complexe_float(mult_complexe_float (alphac, tmp), mult_complexe_float (betac, Yc[i]));
        }
      }

      //Vectorisé
    void mncblas_cgemv_v(MNCBLAS_LAYOUT layout,
      MNCBLAS_TRANSPOSE TransA, const int M, const int N,
      const void *alpha, const void *A, const int lda,
      const void *X, const int incX, const void *beta,
      void *Y, const int incY)
      {
        register unsigned int i, j;
        complexe_float_t *Xc = ((complexe_float_t *)X);
        complexe_float_t *Ac = ((complexe_float_t *)A);
        complexe_float_t *Yc = ((complexe_float_t *)Y);
        __m128 re = _mm_setr_ps(0.0,0.0,0.0,0.0);
        __m128 im = _mm_setr_ps(0.0,0.0,0.0,0.0);
        __m128 tmp_r, tmp_i, tmp2_r, tmp2_i, tmp3_r, tmp3_i, A_r, A_i, X_r, X_i, Y_r, Y_i, alpha_r, alpha_i, beta_r, beta_i;

        complexe_float_t betac = (*(complexe_float_t*)beta);
        complexe_float_t alphac = (*(complexe_float_t*)alpha);

        float tmp_d;

        
        for (i = 0; i < N; i+=4)
        {
          re = _mm_setr_ps(0.0,0.0,0.0,0.0);
          im = _mm_setr_ps(0.0,0.0,0.0,0.0);
       
          for (j = 0; j < M; j += 4)
          {
            A_r = _mm_set1_ps(Ac[i* lda + j].real);
            A_i = _mm_set1_ps(Ac[i* lda + j].imaginary);
            X_r = _mm_set1_ps(Xc[j].real);
            X_i = _mm_set1_ps(Xc[j].imaginary);
            
            mult_complexe_float_v(&A_r, &A_i, &X_r, &X_i, &tmp_r, &tmp_i);
            re = _mm_add_ps(re, tmp_r);
            im = _mm_add_ps(im, tmp_i);
          }
          alpha_r = _mm_set1_ps(alphac.real);
          alpha_i = _mm_set1_ps(alphac.imaginary);

          beta_r = _mm_set1_ps(betac.real);
          beta_i = _mm_set1_ps(betac.imaginary);

          mult_complexe_float_v(&alpha_r,&alpha_i,&tmp_r, &tmp_i, &tmp2_r, &tmp2_i);
          mult_complexe_float_v(&beta_r, &beta_i, &Y_r, &Y_i, &tmp3_r, &tmp3_i);
          add_complexe_float_v(&tmp2_r, &tmp2_i, &tmp3_r, &tmp3_i, &tmp_r, &tmp_i);

          _mm_store1_ps(&tmp_d,tmp_r);
          Yc[i].real = tmp_d;
          _mm_store1_ps(&tmp_d,tmp_i);
          Yc[i].imaginary = tmp_d;
        }
      }      

      //Vectorisation + Parallélisation
          void mncblas_cgemv_vp(MNCBLAS_LAYOUT layout,
      MNCBLAS_TRANSPOSE TransA, const int M, const int N,
      const void *alpha, const void *A, const int lda,
      const void *X, const int incX, const void *beta,
      void *Y, const int incY)
      {
        register unsigned int i, j;
        complexe_float_t *Xc = ((complexe_float_t *)X);
        complexe_float_t *Ac = ((complexe_float_t *)A);
        complexe_float_t *Yc = ((complexe_float_t *)Y);
        __m128 re = _mm_setr_ps(0.0,0.0,0.0,0.0);
        __m128 im = _mm_setr_ps(0.0,0.0,0.0,0.0);
        __m128 tmp_r, tmp_i, tmp2_r, tmp2_i, tmp3_r, tmp3_i, A_r, A_i, X_r, X_i, Y_r, Y_i, alpha_r, alpha_i, beta_r, beta_i;

        complexe_float_t betac = (*(complexe_float_t*)beta);
        complexe_float_t alphac = (*(complexe_float_t*)alpha);
        float tmp;

        #pragma omp parallel for schedule(static) private(tmp)
        for (i = 0; i < N; i+=4)
        {
          re = _mm_setr_ps(0.0,0.0,0.0,0.0);
          im = _mm_setr_ps(0.0,0.0,0.0,0.0);
       
          #pragma omp parallel for schedule(static) private (re,im)
          for (j = 0; j < M; j += 4)
          {
            A_r = _mm_set1_ps(Ac[i* lda + j].real);
            A_i = _mm_set1_ps(Ac[i* lda + j].imaginary);
            X_r = _mm_set1_ps(Xc[j].real);
            X_i = _mm_set1_ps(Xc[j].imaginary);
            
            mult_complexe_float_v(&A_r, &A_i, &X_r, &X_i, &tmp_r, &tmp_i);
            re = _mm_add_ps(re, tmp_r);
            im = _mm_add_ps(im, tmp_i);
          }
          alpha_r = _mm_set1_ps(alphac.real);
          alpha_i = _mm_set1_ps(alphac.imaginary);

          beta_r = _mm_set1_ps(betac.real);
          beta_i = _mm_set1_ps(betac.imaginary);

          mult_complexe_float_v(&alpha_r,&alpha_i,&tmp_r, &tmp_i, &tmp2_r, &tmp2_i);
          mult_complexe_float_v(&beta_r, &beta_i, &Y_r, &Y_i, &tmp3_r, &tmp3_i);
          add_complexe_float_v(&tmp2_r, &tmp2_i, &tmp3_r, &tmp3_i, &tmp_r, &tmp_i);

          _mm_store1_ps(&tmp,tmp_r);
          Yc[i].real = tmp;
          _mm_store1_ps(&tmp,tmp_i);
          Yc[i].imaginary = tmp;
        }
      }    

      /* -------------------  zgemv -----------------------*/

      //Normal
      void mncblas_zgemv(MNCBLAS_LAYOUT layout,
  MNCBLAS_TRANSPOSE TransA, const int M, const int N,
  const void *alpha, const void *A, const int lda,
  const void *X, const int incX, const void *beta,
  void *Y, const int incY)
  {
    register unsigned int i, j;
    complexe_double_t *Xc = ((complexe_double_t *)X);
    complexe_double_t *Ac = ((complexe_double_t *)A);
    complexe_double_t *Yc = ((complexe_double_t *)Y);

    register complexe_double_t tmp = {0,0};
    complexe_double_t betac = (*(complexe_double_t*)beta);
    complexe_double_t alphac = (*(complexe_double_t*)alpha);
    for (i = 0; i < N; i+=incX)
    {
      tmp.imaginary = 0;
      tmp.real = 0;
      for (j = 0; j < M; j += incY)
      {
        tmp = add_complexe_double(tmp,mult_complexe_double (Ac[i* lda + j], Xc[j]));
      }
      Yc[i] = add_complexe_double(mult_complexe_double (alphac, tmp), mult_complexe_double (betac, Yc[i]));
    }
  }
      //Parallélisé
      void mncblas_zgemv_p(MNCBLAS_LAYOUT layout,
        MNCBLAS_TRANSPOSE TransA, const int M, const int N,
        const void *alpha, const void *A, const int lda,
        const void *X, const int incX, const void *beta,
        void *Y, const int incY)
        {
          register unsigned int i, j;
          complexe_double_t *Xc = ((complexe_double_t *)X);
          complexe_double_t *Ac = ((complexe_double_t *)A);
          complexe_double_t *Yc = ((complexe_double_t *)Y);

          register double re = 0.0, im = 0.0;
          register complexe_double_t tmp;
          complexe_double_t betac = (*(complexe_double_t*)beta);
          complexe_double_t alphac = (*(complexe_double_t*)alpha);
          #pragma omp parallel for schedule(static)
          for (i = 0; i < N; i+=incX)
          {
            im = 0.0;
            re = 0.0;
            #pragma omp parallel for schedule(static) private(tmp) reduction(+:re) reduction(+:im)
            for (j = 0; j < M; j += incY)
            {
              tmp = mult_complexe_double (Ac[i* lda + j], Xc[j]);
              re += tmp.real;
              im += tmp.imaginary;
            }
            tmp.real = re;
            tmp.imaginary = im;
            Yc[i] = add_complexe_double(mult_complexe_double (alphac, tmp), mult_complexe_double (betac, Yc[i]));
          }
        }

      // Vectorisé
      void mncblas_zgemv_v(MNCBLAS_LAYOUT layout,
      MNCBLAS_TRANSPOSE TransA, const int M, const int N,
      const void *alpha, const void *A, const int lda,
      const void *X, const int incX, const void *beta,
      void *Y, const int incY)
      {
        register unsigned int i, j;
        complexe_double_t *Xc = ((complexe_double_t *)X);
        complexe_double_t *Ac = ((complexe_double_t *)A);
        complexe_double_t *Yc = ((complexe_double_t *)Y);
        __m128d re = _mm_setr_pd(0.0,0.0);
        __m128d im = _mm_setr_pd(0.0,0.0);
        __m128d tmp_r, tmp_i, tmp2_r, tmp2_i, tmp3_r, tmp3_i, A_r, A_i, X_r, X_i, Y_r, Y_i, alpha_r, alpha_i, beta_r, beta_i;

        complexe_double_t betac = (*(complexe_double_t*)beta);
        complexe_double_t alphac = (*(complexe_double_t*)alpha);
         double tmp_d;

        
        for (i = 0; i < N; i+=4)
        {
          re = _mm_setr_pd(0.0,0.0);
          im = _mm_setr_pd(0.0,0.0);
       
          for (j = 0; j < M; j += 4)
          {
            A_r = _mm_set1_pd(Ac[i* lda + j].real);
            A_i = _mm_set1_pd(Ac[i* lda + j].imaginary);
            X_r = _mm_set1_pd(Xc[j].real);
            X_i = _mm_set1_pd(Xc[j].imaginary);
            
            mult_complexe_double_v(&A_r, &A_i, &X_r, &X_i, &tmp_r, &tmp_i);
            re = _mm_add_pd(re, tmp_r);
            im = _mm_add_pd(im, tmp_i);
          }
          alpha_r = _mm_set1_pd(alphac.real);
          alpha_i = _mm_set1_pd(alphac.imaginary);

          beta_r = _mm_set1_pd(betac.real);
          beta_i = _mm_set1_pd(betac.imaginary);

          mult_complexe_double_v(&alpha_r,&alpha_i,&tmp_r, &tmp_i, &tmp2_r, &tmp2_i);
          mult_complexe_double_v(&beta_r, &beta_i, &Y_r, &Y_i, &tmp3_r, &tmp3_i);
          add_complexe_double_v(&tmp2_r, &tmp2_i, &tmp3_r, &tmp3_i, &tmp_r, &tmp_i);

      
          _mm_store_pd(&tmp_d,tmp_r);
          Yc[i].real = tmp_d;
          _mm_store_pd(&tmp_d,tmp_i);
          Yc[i].imaginary = tmp_d; 
        }
      }      

      // Vectorisé + Parallélisé
      void mncblas_zgemv_vp(MNCBLAS_LAYOUT layout,
      MNCBLAS_TRANSPOSE TransA, const int M, const int N,
      const void *alpha, const void *A, const int lda,
      const void *X, const int incX, const void *beta,
      void *Y, const int incY)
      {
        register unsigned int i, j;
        complexe_double_t *Xc = ((complexe_double_t *)X);
        complexe_double_t *Ac = ((complexe_double_t *)A);
        complexe_double_t *Yc = ((complexe_double_t *)Y);
        __m128d re = _mm_setr_pd(0.0,0.0);
        __m128d im = _mm_setr_pd(0.0,0.0);
        __m128d tmp_r, tmp_i, tmp2_r, tmp2_i, tmp3_r, tmp3_i, A_r, A_i, X_r, X_i, Y_r, Y_i, alpha_r, alpha_i, beta_r, beta_i;

        complexe_double_t betac = (*(complexe_double_t*)beta);
        complexe_double_t alphac = (*(complexe_double_t*)alpha);
        double tmp_d;

        #pragma omp parallel for schedule(static) private(tmp_d)
        for (i = 0; i < N; i+=4)
        {
          re = _mm_setr_pd(0.0,0.0);
          im = _mm_setr_pd(0.0,0.0);
       
          #pragma omp parallel for schedule(static) private (re,im)
          for (j = 0; j < M; j += 4)
          {
            A_r = _mm_set1_pd(Ac[i* lda + j].real);
            A_i = _mm_set1_pd(Ac[i* lda + j].imaginary);
            X_r = _mm_set1_pd(Xc[j].real);
            X_i = _mm_set1_pd(Xc[j].imaginary);
            
            mult_complexe_double_v(&A_r, &A_i, &X_r, &X_i, &tmp_r, &tmp_i);
            re = _mm_add_pd(re, tmp_r);
            im = _mm_add_pd(im, tmp_i);
          }
          alpha_r = _mm_set1_pd(alphac.real);
          alpha_i = _mm_set1_pd(alphac.imaginary);

          beta_r = _mm_set1_pd(betac.real);
          beta_i = _mm_set1_pd(betac.imaginary);

          mult_complexe_double_v(&alpha_r,&alpha_i,&tmp_r, &tmp_i, &tmp2_r, &tmp2_i);
          mult_complexe_double_v(&beta_r, &beta_i, &Y_r, &Y_i, &tmp3_r, &tmp3_i);
          add_complexe_double_v(&tmp2_r, &tmp2_i, &tmp3_r, &tmp3_i, &tmp_r, &tmp_i);
          _mm_store_pd(&tmp_d,tmp_r);
          Yc[i].real = tmp_d;
          _mm_store_pd(&tmp_d,tmp_i);
          Yc[i].imaginary = tmp_d; 
        }
      }